// Survey: https://app.surveystack.io/surveys/6346c1e467f839000121503e/edit
// Section: 34, NPK fertilizer
const builder = require(`../src/convention_builder`);
const { randomUUID } = require('crypto');
const fs = require('fs');

//LOOK can we just use this for nonNPK as well? if we make quantities not required I think it would be the same

// ids for the examples
let npk_fertilizer_convention_uuid = randomUUID();
let quantity__standard__moisture_percentage_uuid = randomUUID();
let quantity__standard__nitrogen_percentage_uuid = randomUUID();
let quantity__standard__phosphorus_percentage_uuid = randomUUID();
let quantity__standard__potassium_percentage_uuid = randomUUID();
let quantity__standard__area_percentage_uuid = randomUUID();
let taxonomy_term__unit__percentage_uuid = randomUUID();
let log__input__npk_fertilizer_uuid = randomUUID();
let taxonomy_term__unit__rate_uuid = randomUUID();

//examples
let log__input__npk_fertilizer_example = {
    id:log__input__npk_fertilizer_uuid,
    attributes: {
        name: "example NPK fertilizer log",
        status:"done",
    }
};
let log__input__npk_fertilizer_error = {
    id:log__input__npk_fertilizer_uuid,
    attributes: {
        name: "example NPK fertilizer log",
        status:"true",
    }
};

//Main entity
let log__input__npk_fertilizer = new builder.SchemaOverlay({
    typeAndBundle: 'log--input',
    name: 'npk_fertilizer',
    validExamples: [log__input__npk_fertilizer_example],
    erroredExamples: [log__input__npk_fertilizer_error]    
});

//LOOK add description
log__input__npk_fertilizer.setMainDescription("This log records nitrogen, phosphorus and potassium fertilizers. It allows the user to create new and/or select multiple fertilizer names and brands. There is another survey question for non-NPK fertilizers. This log has the option to record frequency of application, brand, application method, rate, units, %N, %P, %K and other minerals.");

log__input__npk_fertilizer.setConstant({
    attribute:"status",
    value:"done"
});

//Fertilizer quantity units - moved to global
////Examples
//let taxonomy_term__unit__fertilizer_example = {
//    id:taxonomy_term__unit__rate_uuid,
//    attributes: {
//        name: "lbs_acres",
//    }
//};
//let taxonomy_term__unit__fertilizer_error = {
//    id:taxonomy_term__unit__rate_uuid,
//    attributes: {
//        name: "ft",
//    }
//};
////Overlays and constants
//let taxonomy_term__unit__fertilizer = new builder.SchemaOverlay({
//    typeAndBundle: "taxonomy_term--unit",
//    name: "fertilizer",
//    validExamples: [ taxonomy_term__unit__fertilizer_example ],
//    erroredExamples: [ taxonomy_term__unit__fertilizer_error ]
//});
//taxonomy_term__unit__fertilizer.setEnum({
//    attribute: "name",
//    valuesArray: [
//    ],
//    description: "List of fertilizer units for both liquid and solid amendments."
//});
//taxonomy_term__unit__fertilizer.setMainDescription("Fertilizer Units")

//convention
let NPKfertilizerConvention = new builder.ConventionSchema({
    title: "NPK Fertilizer",
    version: "0.0.1",
    schemaName:"log--input--npk_fertilizer",
    repoURL:"www.gitlabrepo.com/version/farmos_conventions",
    description:`## Purpose\n
The NPK fertilizer log records nitrogen, phosphorus and potassium fertilizer applications.\n
## Specification\n
text\n`,
    validExamples: [],
    erroredExamples: []
});

//LOOK do we need method or brand (don't think so because there's nothing additional that we need beyond what's already specified in shcema). Debugger saying schema doesn't exist for quantity--standard--moisture_percentage
//add local attributes
NPKfertilizerConvention.addAttribute( { schemaOverlayObject:log__input__npk_fertilizer, attributeName: "log--input--npk_fertilizer", required: true } );

//add global attributes
NPKfertilizerConvention.addAttribute( { schemaOverlayObject:"asset--plant--planting", attributeName: "asset--plant--planting", required: true } );
NPKfertilizerConvention.addAttribute( { schemaOverlayObject:"quantity--standard--area_percentage", attributeName: "quantity--standard--area_percentage", required: false});
NPKfertilizerConvention.addAttribute( { schemaOverlayObject:"taxonomy_term--unit--%", attributeName: "taxonomy_term--unit--%", required: false});
NPKfertilizerConvention.addAttribute( { schemaOverlayObject:"quantity--standard--moisture_percentage", attributeName: "quantity--standard--moisture_percentage", required: false } );
NPKfertilizerConvention.addAttribute( { schemaOverlayObject:"quantity--standard--nitrogen_percentage", attributeName: "quantity--standard--nitrogen_percentage", required: false } );
NPKfertilizerConvention.addAttribute( { schemaOverlayObject:"quantity--standard--phosphorus_percentage", attributeName: "quantity--standard--phosphorus_percentage", required: false } );
NPKfertilizerConvention.addAttribute( { schemaOverlayObject:"quantity--standard--potassium_percentage", attributeName: "quantity--standard--potassium_percentage", required: false } );
NPKfertilizerConvention.addAttribute( { schemaOverlayObject:"quantity--material--rate", attributeName: "quantity--material--rate", required: false } );
NPKfertilizerConvention.addAttribute( { schemaOverlayObject:"taxonomy_term--log_category--amendment", attributeName: "taxonomy_term--log_category--amendment", required: false } );
NPKfertilizerConvention.addAttribute( { schemaOverlayObject:"taxonomy_term--unit--rate", attributeName: "taxonomy_term--unit--rate", required: false } );

//add relationships
NPKfertilizerConvention.addRelationship( { containerEntity:"log--input--npk_fertilizer", relationName:"asset", mentionedEntity:"asset--plant--planting", required: true } );
NPKfertilizerConvention.addRelationship( { containerEntity:"log--input--npk_fertilizer" , relationName:"quantity" , mentionedEntity:"quantity--standard--area_percentage" , required: false } );
NPKfertilizerConvention.addRelationship( { containerEntity:"quantity--standard--area_percentage" , relationName:"units" , mentionedEntity:"taxonomy_term--unit--%" , required: false } );
NPKfertilizerConvention.addRelationship( { containerEntity:"log--input--npk_fertilizer" , relationName:"quantity" , mentionedEntity:"quantity--standard--moisture_percentage" , required: false } );
NPKfertilizerConvention.addRelationship( { containerEntity:"quantity--standard--moisture_percentage" , relationName:"units" , mentionedEntity:"taxonomy_term--unit--%" , required: false } );
NPKfertilizerConvention.addRelationship( { containerEntity:"log--input--npk_fertilizer" , relationName:"quantity" , mentionedEntity:"quantity--standard--nitrogen_percentage" , required: false } );
NPKfertilizerConvention.addRelationship( { containerEntity:"quantity--standard--nitrogen_percentage" , relationName:"units" , mentionedEntity:"taxonomy_term--unit--%" , required: false } );
NPKfertilizerConvention.addRelationship( { containerEntity:"log--input--npk_fertilizer" , relationName:"quantity" , mentionedEntity:"quantity--standard--phosphorus_percentage" , required: false } );
NPKfertilizerConvention.addRelationship( { containerEntity:"quantity--standard--phosphorus_percentage" , relationName:"units" , mentionedEntity:"taxonomy_term--unit--%" , required: false } );
NPKfertilizerConvention.addRelationship( { containerEntity:"log--input--npk_fertilizer" , relationName:"quantity" , mentionedEntity:"quantity--standard--potassium_percentage" , required: false } );
NPKfertilizerConvention.addRelationship( { containerEntity:"quantity--standard--potassium_percentage" , relationName:"units" , mentionedEntity:"taxonomy_term--unit--%" , required: false } );
NPKfertilizerConvention.addRelationship( { containerEntity:"log--input--npk_fertilizer" , relationName:"quantity" , mentionedEntity:"quantity--material--rate" , required: false } );
NPKfertilizerConvention.addRelationship( { containerEntity:"log--input--npk_fertilizer" , relationName:"category" , mentionedEntity:"taxonomy_term--log_category--amendment" , required: true } );
NPKfertilizerConvention.addRelationship( { containerEntity:"quantity--material--rate" , relationName:"units" , mentionedEntity:"taxonomy_term--unit--rate" , required: false } );

//examples
let asset__plant__planting_attributes = NPKfertilizerConvention.overlays['asset--plant--planting'].validExamples?.[0]?.attributes;
let asset__plant__planting_uuid = NPKfertilizerConvention.overlays['asset--plant--planting'].validExamples?.[0]?.id;

let quantity__material__rate_attributes = NPKfertilizerConvention.overlays['quantity--material--rate'].validExamples?.[0]?.attributes;
let quantity__material__rate_uuid = NPKfertilizerConvention.overlays['quantity--material--rate'].validExamples?.[0]?.id;

//let taxonomy_term__log_category__amendment_attributes = NPKfertilizerConvention.overlays.log_category.validExamples?.[0]?.attributes;
let taxonomy_term__log_category__amendment_uuid = NPKfertilizerConvention.overlays['taxonomy_term--log_category--amendment'].validExamples?.[0]?.id;

var NPKfertilizerConventionExample = {
    id: npk_fertilizer_convention_uuid,
    type: "Object",
    "asset--plant--planting": {
        id: asset__plant__planting_uuid,
        attributes: asset__plant__planting_attributes
    },
    "log--input--npk_fertilizer": {
        id: log__input__npk_fertilizer_uuid,
        attributes: {
            name: "example NPK fertilizer log",
            status: "done",
            timestamp: ( new Date() ).toISOString(),
        },
        relationships: {
            asset: { data: [
                {
                    type: "asset--plant",
                    id: asset__plant__planting_uuid
                }
            ]},
            category: { data: [
                {
                    type: "taxonomy_term--log_category",
                    id: taxonomy_term__log_category__amendment_uuid
                }
            ]},
            quantity: { data: [
                {
                    type: "quantity--standard",
                    id: quantity__standard__area_percentage_uuid
                },
                {
                    type: "quantity--standard",
                    id: quantity__standard__moisture_percentage_uuid
                },
                {
                    type: "quantity--standard",
                    id: quantity__standard__nitrogen_percentage_uuid
                },
                {
                    type: "quantity--standard",
                    id: quantity__standard__phosphorus_percentage_uuid
                },
                {
                    type: "quantity--standard",
                    id: quantity__standard__potassium_percentage_uuid
                },
                {
                    type: "quantity--material",
                    id: quantity__material__rate_uuid
                }
                ]} 
        },
    },
    "taxonomy_term--log_category--amendment": {
        id: taxonomy_term__log_category__amendment_uuid,
        attributes: {
            name: "amendment"
        }
    },
    "quantity--material--rate": {
        id: quantity__material__rate_uuid,
        attributes: quantity__material__rate_attributes,
        relationships: {
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: taxonomy_term__unit__rate_uuid
                }
            ] }
        }
    },
    "taxonomy_term--unit--rate":{
        id: taxonomy_term__unit__rate_uuid,
        attributes: {
            name: "g_acre"
        }
    },
    "quantity--standard--area_percentage": {
        id: quantity__standard__area_percentage_uuid,
        attributes: {
            label: "area"
        },
        relationships: {
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: taxonomy_term__unit__percentage_uuid
                }
            ] }
        }
    },
    "taxonomy_term--unit--%":  {
        id: taxonomy_term__unit__percentage_uuid,
        attributes: {
            name: "%"
        },   
    },
    "quantity--standard--moisture_percentage": {
        id: quantity__standard__moisture_percentage_uuid,
        attributes: {
            label: "moisture",
            measure: "water_content"
        },
        relationships: {
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: taxonomy_term__unit__percentage_uuid
                }
            ] }
        }
    },
    "quantity--standard--nitrogen_percentage": {
        id: quantity__standard__nitrogen_percentage_uuid,
        attributes: {
            label: "n",
            measure: "ratio"
        },
        relationships: {
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: taxonomy_term__unit__percentage_uuid
                }
            ] }
        }
    },
    "quantity--standard--phosphorus_percentage": {
        id: quantity__standard__phosphorus_percentage_uuid,
        attributes: {
            label: "p",
            measure: "ratio"
        },
        relationships: {
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: taxonomy_term__unit__percentage_uuid
                }
            ] }
        }
    },
    "quantity--standard--potassium_percentage": {
        id: quantity__standard__potassium_percentage_uuid,
        attributes: {
            label: "k",
            measure: "ratio"
        },
        relationships: {
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: taxonomy_term__unit__percentage_uuid
                }
            ] }
        }
    }
};

var NPKfertilizerConventionError = {
    id: npk_fertilizer_convention_uuid,
    type: "Object",
    "asset--plant--planting": {
        id: asset__plant__planting_uuid,
        attributes: asset__plant__planting_attributes
    },
    "log--input--npk_fertilizer": {
        id: log__input__npk_fertilizer_uuid,
        attributes: {
            name: "example NPK fertilizer log",
            status: "pending",
            timestamp: ( new Date() ).toISOString(),
        },
        relationships: {
            asset: { data: [
                {
                    type: "asset--plant",
                    id: asset__plant__planting_uuid
                }
            ]},
            quantity: { data: [
                {
                    type: "quantity--standard",
                    id: quantity__standard__area_percentage_uuid
                },
                {
                    type: "quantity--standard",
                    id: quantity__standard__moisture_percentage_uuid
                },
                {
                    type: "quantity--standard",
                    id: quantity__standard__nitrogen_percentage_uuid
                },
                {
                    type: "quantity--standard",
                    id: quantity__standard__phosphorus_percentage_uuid
                },
                {
                    type: "quantity--standard",
                    id: quantity__standard__potassium_percentage_uuid
                },
                {
                    type: "quantity--standard",
                    id: quantity__material__rate_uuid
                }
                ]} 
        },
    },
    "quantity--material--rate": {
        id: quantity__material__rate_uuid,
        attributes: quantity__material__rate_attributes
    },
    "quantity--standard--area_percentage": {
        id: quantity__standard__area_percentage_uuid,
        attributes: {
            label: "area"
        },
        relationships: {
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: taxonomy_term__unit__percentage_uuid
                }
            ] }
        }
    },
    "taxonomy_term--unit--%":  {
        id: taxonomy_term__unit__percentage_uuid,
        attributes: {
            name: "%"
        },   
    },
    "quantity--standard--nitrogen_percentage": {
        id: quantity__standard__nitrogen_percentage_uuid,
        attributes: {
            label: "nitrogen",
            measure: "ratio"
        },
        relationships: {
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: taxonomy_term__unit__percentage_uuid
                }
            ] }
        }
    },
    "quantity--standard--phosphorus_percentage": {
        id: quantity__standard__phosphorus_percentage_uuid,
        attributes: {
            label: "phosphorus",
            measure: "ratio"
        },
        relationships: {
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: taxonomy_term__unit__percentage_uuid
                }
            ] }
        }
    },
    "quantity--standard--potassium_percentage": {
        id: quantity__standard__potassium_percentage_uuid,
        attributes: {
            label: "potassium",
            measure: "ratio"
        },
        relationships: {
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: taxonomy_term__unit__percentage_uuid
                }
            ] }
        }
    }
};

NPKfertilizerConvention.validExamples = [NPKfertilizerConventionExample];
NPKfertilizerConvention.erroredExamples = [NPKfertilizerConventionError];

let test = NPKfertilizerConvention.testExamples();
let storageOperation = NPKfertilizerConvention.store();
