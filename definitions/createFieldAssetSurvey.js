// Survey: https://app.surveystack.io/surveys/6346c1e467f839000121503e/edit
// Section: 5, create field

const builder = require(`../src/convention_builder`);
const { randomUUID } = require('crypto');
const fs = require('fs');

// ids for the examples
let asset__land__create_field_uuid = randomUUID();
let create_field_convention_uuid = randomUUID();

// // Main entity: Land Asset
let asset__land__create_field_example = {
    id:asset__land__create_field_uuid,
    attributes: {
        name: "example create field",
        status:"active",
        is_location:"true",
        is_fixed:"true",
        land_type:"field"
    }
};

let asset__land__create_field_error = {
    id:asset__land__create_field_uuid,
    attributes: {
        name: "example create field",
        status:"true",
        is_location:"false",
        is_fixed: "true",
        land_type: "area"
    }
};

let asset__land__create_field = new builder.SchemaOverlay({
    typeAndBundle: 'asset--land',
    name: 'create_field',
    validExamples: [asset__land__create_field_example],
    erroredExamples: [asset__land__create_field_error]    
});

asset__land__create_field.setMainDescription("Creating a field in surveystack creates a land asset in farmOS.");
asset__land__create_field.setConstant({
    attribute:"status",
    value:"active"
});
asset__land__create_field.setConstant({
    attribute:"is_location",
    value:"true"
});
asset__land__create_field.setConstant({
    attribute:"is_fixed",
    value:"true"
});
asset__land__create_field.setConstant({
    attribute:"land_type",
    value:"field"
});


// // // Convention
// object
let createFieldConvention = new builder.ConventionSchema({
    title: "Field",
    version: "0.0.1",
    schemaName:"asset--land--field",
    repoURL:"www.gitlabrepo.com/version/farmos_conventions",
    description:`## Purpose\n
text\n
## Specification\n
text\n`,
    validExamples: [],
    erroredExamples: []
});

createFieldConvention.addAttribute( { schemaOverlayObject:asset__land__create_field, attributeName: "asset--land--create_field", required: true } );

let createFieldConventionExample = {
    id: create_field_convention_uuid,
    "asset--land--create_field": {
        id:asset__land__create_field_uuid,
        attributes: {
            name: "example create field",
            status:"active",
            is_location:"true",
            is_fixed:"true",
            land_type:"field"
        }
    }
};

let createFieldConventionError = {
    id: create_field_convention_uuid,
    "asset--land--create_field": {
        id:asset__land__create_field_uuid,
        attributes: {
            name: "example create field",
            status:"true",
            is_location:"false",
            is_fixed: "true",
            land_type: "area"
        }
    },
};

createFieldConvention.validExamples = [createFieldConventionExample];
createFieldConvention.erroredExamples = [createFieldConventionError];

let test = createFieldConvention.testExamples();
let storageOperation = createFieldConvention.store();
