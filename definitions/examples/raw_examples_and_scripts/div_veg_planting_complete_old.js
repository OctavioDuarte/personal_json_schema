const builder = require(`../../../src/convention_builder`);
const { randomUUID } = require('crypto');
const { exampleImporter } = require("../../../src/schema_utilities.js");
const fs = require('fs');

let seedingConventionSrc = JSON.parse( fs.readFileSync( `./../../../collection/conventions/log--seeding--div_veg_planting/object.json` ) );
let seedingConvention = new builder.ConventionSchema( seedingConventionSrc );

let seedingExample = exampleImporter(filename = "div_veg_planting_complete.json");

Object.keys(seedingConvention.schema.properties);

let formattedExample = {
    seeding_log: {},
    log_category: {},
    area_quantity: {},
    area_unit: {},
    seeding_rate_quantity: {},
    rate_unit: {},
    bed_width_quantity: {},
    width_unit: {},
    plant_asset:{}
};

//1st entity is the plant asset
formattedExample.plant_asset = seedingExample[0];

//2nd entity is the taxonomy_term--plant_type, this is missing

//3rd entity is also the taxonomy_term--plant_type, this is missing

//4th entity is also the taxonomy_term--season, this is missing

//5th entity is log--seeding
formattedExample.seeding_log = seedingExample[4];

//6th entity is taxonomy_term--log_category
formattedExample.log_category = seedingExample[5];

//7th entity is quantity--standard, area
formattedExample.area_quantity = seedingExample[6];

//8th entity is taxonomy_term--unit, %
formattedExample.area_quantity = seedingExample[7];

//9th entity is quantity--standard, bed_width
formattedExample.bed_width_quantity = seedingExample[8];

//10th entity is taxonomy_term--unit, meters
formattedExample.width_unit = seedingExample[9];

//11th entity is quantity--standard, seeding_rate
formattedExample.seeding_rate_quantity = seedingExample[10];

//12th entity is taxonomy_term--unit, kg_per_hec
formattedExample.rate_unit = seedingExample[11];

//13th entity is log--transplanting, this is missing

//14th entity is taxonomy_term--log_category, transplanting

//15th entity is quantity--material