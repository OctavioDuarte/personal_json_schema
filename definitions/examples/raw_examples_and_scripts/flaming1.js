// Shows how to import an example with the specialized tool we've built and test on it in the `validExamples` field of a ConventionSchema.
// Example relies on a previously written ConventionSchema, the one for Seeding Log, from the Management Survey.
const builder = require(`../../../src/convention_builder`);
const { randomUUID } = require('crypto');
const { exampleImporter, organizeEntitiesArrayIntoConvention } = require("../../../src/schema_utilities.js");
const fs = require('fs');

// Obtain the preexisting convention
let flamingConventionSrc = JSON.parse( fs.readFileSync( `./../../../output/collection/conventions/log--activity--flaming/object.json` ) );
let flamingConvention = new builder.ConventionSchema( flamingConventionSrc );


// read in surveystack example
let flamingExample = exampleImporter(filename = "flaming1.json");
// print the result in the terminal
flamingExample;

let search = organizeEntitiesArrayIntoConvention( { entitiesArray:flamingExample, conventionObject:flamingConvention } );

/*Object.keys(flamingConvention.schema.properties);

// Let's create an object with this attribute names

let formattedExample = {
    flaming_log: {},
    log_category: {},
    area_quantity: {},
    area_unit: {},
    plant_asset:{}
};

//1st entity is log--activity
formattedExample.flaming_log = flamingExample[0];

//2nd entity is taxonomy_term--log_category--weed_control
formattedExample.log_category = flamingExample[1];

//3rd entity is quantity--standard--area
formattedExample.area_quantity = flamingExample[2];

//4th entity is taxonomy_term--unit--%
formattedExample.area_unit = flamingExample[3];

// Let's insert this example between the valid examples in our schema convention and see if it passes the tests.
flamingConvention.validExamples = [ formattedExample ];

let testResults = flamingConvention.testExamples();

if (testResults.success) {
    // finished example goes in "processed"
    // the name is obtained from the convention name
    fs.writeFileSync( `./../processed/${flamingConvention.schemaName}.json`, JSON.stringify(formattedExample), console.error );
    console.log("Example stored.");
};*/
