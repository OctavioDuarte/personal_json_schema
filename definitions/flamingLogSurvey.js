// Survey: https://app.surveystack.io/surveys/6346c1e467f839000121503e/edit
// Section: 28, flaming
const builder = require(`../src/convention_builder`);
const { randomUUID } = require('crypto');
const fs = require('fs');

// ids for the examples
let flaming_convention_uuid = randomUUID();
let log__activity__flaming_uuid = randomUUID();
let taxonomy_term__log_category__weed_control_uuid = randomUUID();
let quantity__standard__area_percentage_uuid = randomUUID();
let taxonomy_term__unit__percentage_uuid = randomUUID();

//Main entity
////examples
let log__activity__flaming_example = {
    id:log__activity__flaming_uuid,
    attributes: {
        name: "flaming log",
        status:"done",
    }
};
let log__activity__flaming_error = {
    id:log__activity__flaming_uuid,
    attributes: {
        name: "testing log",
        status:"done",
    }
};

////overlay
let log__activity__flaming = new builder.SchemaOverlay({
    typeAndBundle: 'log--activity',
    name: 'flaming',
    validExamples: [log__activity__flaming_example],
    erroredExamples: [log__activity__flaming_error]    
});
//LOOK review description
log__activity__flaming.setMainDescription("Flaming logs are used for termination or weed control and allow for a single date or start date, end date and frequency.");

log__activity__flaming.setConstant({
    attribute:"status",
    value:"done"
});

log__activity__flaming.setPattern({
    attribute:"name",
    pattern:"(flaming)",
    description:"Flaming must be included in the name."
});

let test2 = log__activity__flaming.testExamples();

//moved to global
//log category 
////examples
//let logCategoryExample = {
//    id: logCategoryUUID,
//    attributes: {
//        name: "weed_control"
//    }
//};
//let logCategoryError = {
//    id: logCategoryUUID,
//    attributes: {
//        label: "seeding"
//    }
//};

////overlays
//let logCategory = new builder.SchemaOverlay({
//    typeAndBundle: "taxonomy_term--log_category",
//    name: "weed_control",
//    validExamples: [logCategoryExample],
//    erroredExamples: [logCategoryError],
//});
//logCategory.setConstant({
//    attribute:"name",
//    value:"weed_control"
//});
//logCategory.setMainDescription("Log category is set to weed_control.");

//convention
let flamingConvention = new builder.ConventionSchema({
    title: "Flaming",
    version: "0.0.1",
    schemaName:"log--activity--flaming",
    repoURL:"www.gitlabrepo.com/version/farmos_conventions",
    description:`## Purpose\n
Flaming logs can be created for termination or weed control.\n
## Specification\n
text\n`,
    // we still have no examples, we will add them later
    validExamples: [],
    erroredExamples: []
});

//add local attributes
flamingConvention.addAttribute( { schemaOverlayObject:log__activity__flaming, attributeName: "log--activity--flaming", required: true } );

//add global attributes
flamingConvention.addAttribute( { schemaOverlayObject:"taxonomy_term--log_category--weed_control", attributeName: "taxonomy_term--log_category--weed_control", required: true } );
flamingConvention.addAttribute({ schemaOverlayObject:"quantity--standard--area_percentage", attributeName: "quantity--standard--area_percentage", required: false});
flamingConvention.addAttribute({ schemaOverlayObject:"taxonomy_term--unit--%", attributeName: "taxonomy_term--unit--%", required: false});
flamingConvention.addAttribute( { schemaOverlayObject:"asset--plant--planting", attributeName: "asset--plant--planting", required: true } );

//relationships
flamingConvention.addRelationship( { containerEntity:"log--activity--flaming" , relationName:"category" , mentionedEntity:"taxonomy_term--log_category--weed_control" , required: true } );
flamingConvention.addRelationship( { containerEntity:"log--activity--flaming" , relationName:"quantity" , mentionedEntity:"quantity--standard--area_percentage" , required: false } );
flamingConvention.addRelationship( { containerEntity:"quantity--standard--area_percentage" , relationName:"units" , mentionedEntity:"taxonomy_term--unit--%" , required: false } );
flamingConvention.addRelationship( { containerEntity:"log--activity--flaming", relationName:"asset", mentionedEntity:"asset--plant--planting", required: true } );

let asset__plant__planting_attributes = flamingConvention.overlays['asset--plant--planting'].validExamples?.[0].attributes;
let asset__plant__planting_uuid = flamingConvention.overlays['asset--plant--planting'].validExamples?.[0]?.id;

//Convention examples
let flamingConventionExample = {
    id: flaming_convention_uuid,
    type: "Object",
    "asset--plant--planting": {
        id: asset__plant__planting_uuid,
        attributes: asset__plant__planting_attributes
    },
    "log--activity--flaming": {
        id: log__activity__flaming_uuid,
        attributes: {
            name: "example flaming log",
            status: "done",
            timestamp: ( new Date() ).toISOString(),
        },
        relationships: {
            asset: { data: [
                {
                    type: "asset--plant",
                    id: asset__plant__planting_uuid
                }
            ]},
            category: { data: [
                {
                    type: "taxonomy_term--log_category",
                    id:taxonomy_term__log_category__weed_control_uuid
                }
            ] },
            quantity: { data: [
                {
                    type: "quantity--standard",
                    id: quantity__standard__area_percentage_uuid
                }
            ] }
        },
    },
    "taxonomy_term--log_category--weed_control": {
        id: taxonomy_term__log_category__weed_control_uuid,
        attributes: {
            name: "weed_control"
        },   
    },
    "quantity--standard--area_percentage": {
        id: quantity__standard__area_percentage_uuid,
        attributes: {
            label: "area"
        },
        relationships: {
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: taxonomy_term__unit__percentage_uuid
                }
            ] }
        }
    },
    "taxonomy_term--unit--%":  {
        id: taxonomy_term__unit__percentage_uuid,
        attributes: {
            name: "%"
        },   
    }
};

// Rewrite the example
let flamingConventionError = {
    id: flaming_convention_uuid,
    type: "object",
    "asset--plant--planting": {
        id: asset__plant__planting_uuid,
        attributes: asset__plant__planting_attributes
    },
    "log--activity--flaming": {
        id: log__activity__flaming_uuid,
        attributes: {
            name: "example flaming log",
            status: "pending",
        },
        relationships: {
            category: { data: [
                {
                    type: "taxonomy_term--log_category",
                    id:taxonomy_term__log_category__weed_control_uuid
                }
            ] },
            quantity: { data: [
                {
                    type: "quantity--standard",
                    id: quantity__standard__area_percentage_uuid
                }
            ] }
        },
    },
    "taxonomy_term--log_category--weed_control": {
        id: taxonomy_term__log_category__weed_control_uuid,
        attributes: {
            name: "blah"
        },   
    },
    /*"quantity--standard--area_percentage": {
        id: areaPercentageUUID,
        attributes: {
            label: "area"
        },
        relationships: {
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: percentageUnitUUID
                }
            ] }
        }
    },*/
};


// Now that we have proper examples, let's feed them into our convention object.
flamingConvention.validExamples = [flamingConventionExample];
flamingConvention.erroredExamples = [flamingConventionError];

let secondTest = flamingConvention.testExamples();
let storageOperation = flamingConvention.store();
