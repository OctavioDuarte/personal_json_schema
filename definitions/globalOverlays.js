const builder = require(`../src/convention_builder`);
const { randomUUID } = require('crypto');
const fs = require('fs');


//taxonomy_term--log_category--grazing
let taxonomy_term__log_category__grazing_uuid = randomUUID();
let taxonomy_term__log_category__grazing_example = {
    id: taxonomy_term__log_category__grazing_uuid,
    attributes: {
        name: "grazing"
    }
};
let taxonomy_term__log_category__grazing_error = {
    id: taxonomy_term__log_category__grazing_uuid,
    attributes: {
        label: "seeding"
    }
};
let taxonomy_term__log_category__grazing = new builder.SchemaOverlay({
    typeAndBundle: "taxonomy_term--log_category",
    name: "grazing",
    validExamples: [taxonomy_term__log_category__grazing_example],
    erroredExamples: [taxonomy_term__log_category__grazing_error],
});
taxonomy_term__log_category__grazing.setConstant({
    attribute:"name",
    value:"grazing"
});
taxonomy_term__log_category__grazing.setMainDescription("Log category is set to grazing.");

taxonomy_term__log_category__grazing.store();


//taxonomy_term--log_category--weed_control
let taxonomy_term__log_category__weed_control_uuid = randomUUID();
let taxonomy_term__log_category__weed_control_example = {
    id: taxonomy_term__log_category__weed_control_uuid,
    attributes: {
            name: "weed_control"
    }
};
let taxonomy_term__log_category__weed_control_error = {
    id: taxonomy_term__log_category__weed_control_uuid,
    attributes: {
        label: "seeding"
    }
};
let taxonomy_term__log_category__weed_control = new builder.SchemaOverlay({
    typeAndBundle: "taxonomy_term--log_category",
    name: "weed_control",
    validExamples: [taxonomy_term__log_category__weed_control_example],
    erroredExamples: [taxonomy_term__log_category__weed_control_error],
});
taxonomy_term__log_category__weed_control.setConstant({
    attribute:"name",
    value:"weed_control"
});
taxonomy_term__log_category__weed_control.setMainDescription("Log category is set to weed_control.");
    
taxonomy_term__log_category__weed_control.store();


//taxonomy_term--log_category--irrigation
let taxonomy_term__log_category__irrigation_uuid = randomUUID();
let taxonomy_term__log_category__irrigation_example = {
   id: taxonomy_term__log_category__irrigation_uuid,
    attributes: {
        name: "irrigation"
    }
};
let taxonomy_term__log_category__irrigation_error = {
    id: taxonomy_term__log_category__irrigation_uuid,
    attributes: {
        name: "activity"
    }
};
let taxonomy_term__log_category__irrigation = new builder.SchemaOverlay({
    typeAndBundle: "taxonomy_term--log_category",
    name: "irrigation",
    validExamples: [taxonomy_term__log_category__irrigation_example],
    erroredExamples: [taxonomy_term__log_category__irrigation_error],
});
taxonomy_term__log_category__irrigation.setConstant({
    attribute:"name",
    value:"irrigation"
});
taxonomy_term__log_category__irrigation.setMainDescription("Log category is set to irrigation.");

taxonomy_term__log_category__irrigation.store();

//log_category--amendment
let taxonomy_term__log_category__amendment_uuid = randomUUID();
let taxonomy_term__log_category__amendment_example = {
    id: taxonomy_term__log_category__amendment_uuid,
    attributes: {
        name: "amendment"
    }
};
let taxonomy_term__log_category__amendment_error = {
    id: taxonomy_term__log_category__amendment_uuid,
    attributes: {
        name: "seeding"
    }
};
let taxonomy_term__log_category__amendment = new builder.SchemaOverlay({
    typeAndBundle: "taxonomy_term--log_category",
    name: "amendment",
    validExamples: [taxonomy_term__log_category__amendment_example],
    erroredExamples: [taxonomy_term__log_category__amendment_error],
});
taxonomy_term__log_category__amendment.setConstant({
    attribute:"name",
    value:"amendment"
});
taxonomy_term__log_category__amendment.setMainDescription("Log category is set to amendment.")

taxonomy_term__log_category__amendment.store();


//taxonomy_term--log_category--pest_disease_control
let taxonomy_term__log_category__pest_disease_control_uuid = randomUUID();
let taxonomy_term__log_category__pest_disease_control_example = {
    id: taxonomy_term__log_category__pest_disease_control_uuid,
    attributes: {
        name: "pest_disease_control"
    }
};
let taxonomy_term__log_category__pest_disease_control_error = {
    id: taxonomy_term__log_category__pest_disease_control_uuid,
    attributes: {
        name: "activity"
    }
};
let taxonomy_term__log_category__pest_disease_control = new builder.SchemaOverlay({
    typeAndBundle: "taxonomy_term--log_category",
    name: "pest_disease_control",
    validExamples: [taxonomy_term__log_category__pest_disease_control_example],
    erroredExamples: [taxonomy_term__log_category__pest_disease_control_error],
});
taxonomy_term__log_category__pest_disease_control.setConstant({
    attribute:"name",
    value:"pest_disease_control"
});
taxonomy_term__log_category__pest_disease_control.setMainDescription("The log category is set to pest disease control and/or weed control.");
taxonomy_term__log_category__pest_disease_control.store();

//taxonomy term - log_category--seeding
let taxonomy_term__log_category__seeding_uuid = randomUUID();
let taxonomy_term__log_category__seeding_example = {
    id: taxonomy_term__log_category__seeding_uuid,
    attributes: {
        name: "seeding"
    }
};
let taxonomy_term__log_category__seeding_error = {
    id: taxonomy_term__log_category__seeding_uuid,
    attributes: {
        name: "activity"
    }
};
let taxonomy_term__log_category__seeding = new builder.SchemaOverlay({
    typeAndBundle: "taxonomy_term--log_category",
    name: "seeding",
    validExamples: [taxonomy_term__log_category__seeding_example],
    erroredExamples: [taxonomy_term__log_category__seeding_error],
});
taxonomy_term__log_category__seeding.setConstant({
    attribute:"name",
    value:"seeding"
});
taxonomy_term__log_category__seeding.setMainDescription("The log category is set to seeding.");

taxonomy_term__log_category__seeding.store();


//taxonomy_term--log_category--transplanting
let taxonomy_term__log_category__transplanting_uuid = randomUUID();
let taxonomy_term__log_category__transplanting_example = {
    id: taxonomy_term__log_category__transplanting_uuid,
    attributes: {
        name: "transplanting"
    }
};
let taxonomy_term__log_category__transplanting_error = {
    id: taxonomy_term__log_category__transplanting_uuid,
    attributes: {
        label: "activity"
    }
};
let taxonomy_term__log_category__transplanting = new builder.SchemaOverlay({
    typeAndBundle: "taxonomy_term--log_category",
    name: "transplanting",
    validExamples: [taxonomy_term__log_category__transplanting_example],
    erroredExamples: [taxonomy_term__log_category__transplanting_error],
});
taxonomy_term__log_category__transplanting.setConstant({
    attribute: "name",
    value: "transplanting"
});
taxonomy_term__log_category__transplanting.setMainDescription("Log category is set to transplanting.")

taxonomy_term__log_category__transplanting.store();

//taxonomy_term--log_category--termination
let taxonomy_term__log_category__termination_uuid = randomUUID();
let taxonomy_term__log_category__termination_example = {
    id: taxonomy_term__log_category__termination_uuid,
    attributes: {
        name: "termination",
    }
};
let taxonomy_term__log_category__termination_error = {
    id: taxonomy_term__log_category__termination_uuid,
    attributes: {
        name: "done",
    }
};
let taxonomy_term__log_category__termination = new builder.SchemaOverlay({
    typeAndBundle: "taxonomy_term--log_category",
    name: "termination",
    validExamples: [taxonomy_term__log_category__termination_example],
    erroredExamples: [taxonomy_term__log_category__termination_error]
});
taxonomy_term__log_category__termination.setMainDescription("Log category is set to termination.") 
taxonomy_term__log_category__termination.setConstant( {
    attribute:"name",
    value:"termination"
} );
taxonomy_term__log_category__termination.store();

//quantity--material--rate
let quantity__material__rate_uuid = randomUUID();
let quantity__material__rate_example = {
    id: quantity__material__rate_uuid,
    attributes: {
        label: "application_rate",
        measure: "rate"
    }
};
let quantity__material__rate_error = {
    id: quantity__material__rate_uuid,
    attributes: {
        label: "application_rate",
        measure: "area"
    }
};
////overlays and constants
let quantity__material__rate = new builder.SchemaOverlay({
    typeAndBundle: "quantity--material",
    name: "rate",
    validExamples: [ quantity__material__rate_example ],
    erroredExamples: [quantity__material__rate_error]
});
quantity__material__rate.setMainDescription("The herbicide rate indicates the amount of product used for every surface unit.");
quantity__material__rate.setConstant({
    attribute: "measure",
    value:"rate"
});
quantity__material__rate.store();

//moisture percentage
let quantity__standard__moisture_percentage_uuid = randomUUID();
let quantity__standard__moisture_percentage_example = {
    id: quantity__standard__moisture_percentage_uuid,
    attributes: {
        label: "moisture",
        measure: "water_content"
    }
};
let quantity__standard__moisture_percentage_error = {
    id: quantity__standard__moisture_percentage_uuid,
    attributes: {
        label: "moisture",
        measure: "area"
    }
};
let quantity__standard__moisture_percentage = new builder.SchemaOverlay({
    typeAndBundle: "quantity--standard",
    name: "moisture_percentage",
    validExamples: [quantity__standard__moisture_percentage_example],
    erroredExamples: [ quantity__standard__moisture_percentage_error ]
});
quantity__standard__moisture_percentage.setMainDescription("The percent moisture present in the organic matter material.");
quantity__standard__moisture_percentage.setConstant({
    attribute: "measure",
    //LOOK check that this is how value should be entered
    value:"water_content"
});
quantity__standard__moisture_percentage.setConstant({
    attribute: "label",
    value:"moisture"
});
quantity__standard__moisture_percentage.store();

//nitrogen percentage
let quantity__standard__nitrogen_percentage_uuid = randomUUID();
let quantity__standard__nitrogen_percentage_example = {
    id: quantity__standard__nitrogen_percentage_uuid,
    attributes: {
        label: "n",
        measure: "ratio"
    }
};
let quantity__standard__nitrogen_percentage_error = {
    id: quantity__standard__nitrogen_percentage_uuid,
    attributes: {
        label: "p",
        measure: "area"
    }
};
let quantity__standard__nitrogen_percentage = new builder.SchemaOverlay({
    typeAndBundle: "quantity--standard",
    name: "nitrogen_percentage",
    validExamples: [quantity__standard__nitrogen_percentage_example],
    erroredExamples: [quantity__standard__nitrogen_percentage_error]
});
quantity__standard__nitrogen_percentage.setMainDescription("The percent of nitrogen present in the organic matter material.");
quantity__standard__nitrogen_percentage.setConstant({
    attribute: "measure",
    //LOOK check that this is how value should be entered
    value:"ratio"
});
quantity__standard__nitrogen_percentage.setConstant({
    attribute: "label",
    value:"n"
});
quantity__standard__nitrogen_percentage.store();

//phosphorus percentage
let quantity__standard__phosphorus_percentage_uuid = randomUUID();
let quantity__standard__phosphorus_percentage_example = {
    id: quantity__standard__phosphorus_percentage_uuid,
    attributes: {
        label: "p",
        measure: "ratio"
    }
};
let quantity__standard__phosphorus_percentage_error = {
    id: quantity__standard__phosphorus_percentage_uuid,
    attributes: {
        label: "n",
        measure: "area"
    }
};
let quantity__standard__phosphorus_percentage = new builder.SchemaOverlay({
    typeAndBundle: "quantity--standard",
    name: "phosphorus_percentage",
    validExamples: [quantity__standard__phosphorus_percentage_example],
    erroredExamples: [quantity__standard__phosphorus_percentage_error]
});
quantity__standard__phosphorus_percentage.setMainDescription("The percent of phosphorus present in the organic matter material.");
quantity__standard__phosphorus_percentage.setConstant({
    attribute: "measure",
    //LOOK check that this is how value should be entered
    value:"ratio"
});
quantity__standard__phosphorus_percentage.setConstant({
    attribute: "label",
    value:"p"
});
quantity__standard__phosphorus_percentage.store();

//potassium percentage
let quantity__standard__potassium_percentage_uuid = randomUUID();
let quantity__standard__potassium_percentage_example = {
    id: quantity__standard__potassium_percentage_uuid,
    attributes: {
        label: "k",
        measure: "ratio"
    }
};
let quantity__standard__potassium_percentage_error = {
    id: quantity__standard__potassium_percentage_uuid,
    attributes: {
        label: "n",
        measure: "moisture"
    }
};
let quantity__standard__potassium_percentage = new builder.SchemaOverlay({
    typeAndBundle: "quantity--standard",
    name: "potassium_percentage",
    validExamples: [quantity__standard__potassium_percentage_example],
    erroredExamples: [quantity__standard__potassium_percentage_error]
});
quantity__standard__potassium_percentage.setMainDescription("The percent of potassium present in the organic matter material.");
quantity__standard__potassium_percentage.setConstant({
    attribute: "measure",
    //LOOK check that this is how value should be entered
    value:"ratio"
});
quantity__standard__potassium_percentage.setConstant({
    attribute: "label",
    value:"k"
});
quantity__standard__potassium_percentage.store();

//plant asset
let asset__plant__planting_uuid = randomUUID();
let asset__plant__planting_example = {
    id:asset__plant__planting_uuid,
    attributes: {
        name: "example div veg plant asset",
        geometry: null,
        status:"active"
    }
};
let asset__plant__planting_error = {
    id:asset__plant__planting_uuid,
    attributes: {
        name: "example div veg plant asset",
        geometry: "[91.93934434,-40.345345]",
        status:"true"
    }
};
let asset__plant__planting = new builder.SchemaOverlay({
    typeAndBundle: 'asset--plant',
    name: 'planting',
    validExamples: [asset__plant__planting_example],
    erroredExamples: [asset__plant__planting_error]    
});
asset__plant__planting.setMainDescription("The planting is the main asset that all management logs will reference.");
asset__plant__planting.setConstant({
    attribute:"status",
    value:"active"
});
asset__plant__planting.setConstant({
    attribute:"geometry",
    value:null
});
asset__plant__planting.store();

//area percentage
let quantity__standard__area_percentage_uuid = randomUUID();
let quantity__standard__area_percentage_example = {
    id: quantity__standard__area_percentage_uuid,
    attributes: {
        label: "area",
        measure: "area"
    }
};
let quantity__standard__area_percentage_error = {
    id: quantity__standard__area_percentage_uuid,
    attributes: {
        label: "percent",
        measure: "moisture"
    }
};
let quantity__standard__area_percentage = new builder.SchemaOverlay({
    typeAndBundle: "quantity--standard",
    name: "area_percentage",
    validExamples: [quantity__standard__area_percentage_example],
    erroredExamples: [quantity__standard__area_percentage_error]
});
quantity__standard__area_percentage.setMainDescription("Area is the percentage of the field that the seeding applies to.");
quantity__standard__area_percentage.setConstant({
    attribute: "measure",
    value:"area"
});
quantity__standard__area_percentage.setConstant({
    attribute: "label",
    value:"area"
});
quantity__standard__area_percentage.store();

//percentage unit
let taxonomy_term__unit__percentage_uuid = randomUUID();
let taxonomy_term__unit__percentage_example = {
    id: taxonomy_term__unit__percentage_uuid,
    attributes: {
        name: "%",
    }
};
let taxonomy_term__unit__percentage_error = {
    id: taxonomy_term__unit__percentage_uuid,
    attributes: {
        name: "acres",
    }
};
let taxonomy_term__unit__percentage = new builder.SchemaOverlay({
    typeAndBundle: "taxonomy_term--unit",
    name: "%",
    validExamples: [taxonomy_term__unit__percentage_example],
    erroredExamples: [taxonomy_term__unit__percentage_error]
});
taxonomy_term__unit__percentage.setMainDescription("Units for area are: % of acres.");
taxonomy_term__unit__percentage.setConstant({
    attribute: "name",
    value: "%"
});
taxonomy_term__unit__percentage.store();

//taxonomy_term--unit--rate
let taxonomy_term__unit__rate_uuid = randomUUID();
let taxonomy_term__unit__rate_example = {
    id: taxonomy_term__unit__rate_uuid,
    attributes: {
        name: "oz_acre",
    }
};
let taxonomy_term__unit__rate_error = {
    id: taxonomy_term__unit__rate_uuid,
    attributes: {
        name: "blah",
    }
};
////overlays and constants
let taxonomy_term__unit__rate = new builder.SchemaOverlay({
    typeAndBundle: "taxonomy_term--unit",
    name: "rate",
    validExamples: [ taxonomy_term__unit__rate_example ],
    erroredExamples: [ taxonomy_term__unit__rate_error ]
});
taxonomy_term__unit__rate.setMainDescription("The units refer to the application amount in a mass to area ratio.")

taxonomy_term__unit__rate.setEnum({
    attribute: "name",
    valuesArray: [
        "cubic_ft",
        "cubic_yard",
        "gallons_acre",
        "g_acre",
        "g_bedft",
        "g_rowft",
        "g_sqft",
        "in",
        "kg_acre",
        "kg_ha",
        "kg_rowmeter",
        "kg_sqm",
        "lbs_acre",
        "lbs_bedft",
        "lbs_rowft",
        "lbs_sqft",
        "liters_acre",
        "metric_tons_acre",
        "metric_tons_ha",
        "oz_acre",
        "oz_bedft",
        "oz_rowft",
        "oz_sqft",
        "gallons",
        "g",
        "kg",
        "lbs",
        "liters",
        "metric_tons",
        "oz",
        "us_tons",
        "us_tons_acre"
    ],
    description: "Several units are available, both imperial and metric. They are all compatible mass to area ratios."
});
taxonomy_term__unit__rate.store();