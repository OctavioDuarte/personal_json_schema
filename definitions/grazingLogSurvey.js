// Survey: https://app.surveystack.io/surveys/6346c1e467f839000121503e/edit
// Section: 41, grazing
const builder = require(`../src/convention_builder`);
const { randomUUID } = require('crypto');
const fs = require('fs');

//UUIDs
let taxonomy_term__log_category__grazing_uuid = randomUUID();
let log__activity__grazing_uuid = randomUUID();
let quantity__standard__animal_count_uuid = randomUUID();
let quantity__standard__animal_weight_uuid = randomUUID();
let quantity__standard__subpaddocks_uuid = randomUUID();
let grazing_convention_uuid = randomUUID();
let taxonomy_term__unit__weight_uuid = randomUUID();
let quantity__standard__area_percentage_uuid = randomUUID();
let taxonomy_term__unit__percentage_uuid = randomUUID();
let taxonomy_term__log_category_uuid = randomUUID();

//Main entity
////Examples
let log__activity__grazing_example = {
    id:log__activity__grazing_uuid,
    attributes: {
        name: "grazing log",
        status:"done",
    }
};
let log__activity__grazing_error = {
    id:log__activity__grazing_uuid,
    attributes: {
        name: "grazing log",
        status:"pending",
    }
};
////overlays and constants
let log__activity__grazing = new builder.SchemaOverlay({
    typeAndBundle: 'log--activity',
    name: 'grazing',
    validExamples: [log__activity__grazing_example],
    erroredExamples: [log__activity__grazing_error]    
});
//LOOK review description
log__activity__grazing.setMainDescription("Grazing logs are used to track when a herd was moved in and/or out of an area. This log records field, % of area covered, type of livestock, grazing type, animal count, average weight, and grazing purpose");
log__activity__grazing.setConstant({
    attribute:"status",
    value:"done"
});

//Log Category
////Examples
//let logCategoryExample = {
//    id: logCategoryUUID,
//    attributes: {
//        name: "grazing"
//    }
//};
//let logCategoryError = {
//    id: logCategoryUUID,
//    attributes: {
//        label: "blah"
//    }
//};
////overlays and constants
//let logCategory = new builder.SchemaOverlay({
//    typeAndBundle: "taxonomy_term--log_category",
//    name: "grazing",
//    validExamples: [logCategoryExample],
//    erroredExamples: [logCategoryError],
//});
//logCategory.setConstant({
//    attribute:"name",
//    value:"grazing"
//});
//logCategory.setMainDescription("The log category is set to grazing.");

//quantity - weight
////Examples
let quantity__standard__animal_weight_example = {
    id: quantity__standard__animal_weight_uuid,
    attributes: {
        measure: "weight"
    }
};
let quantity__standard__animal_weight_error = {
    id: quantity__standard__animal_weight_uuid,
    attributes: {
        measure: "count"
    }
};
////overlays and constants
let quantity__standard__animal_weight = new builder.SchemaOverlay({
    typeAndBundle: "quantity--standard",
    name: "animal_weight",
    validExamples: [ quantity__standard__animal_weight_example ],
    erroredExamples: [ quantity__standard__animal_weight_error ]
});
quantity__standard__animal_weight.setMainDescription("Animal weight is an average weight of the herd involved in the grazing event. The measure attribute is set to weight.");
quantity__standard__animal_weight.setConstant({
    attribute: "measure",
    value:"weight"
});

//Weight unit
////Examples
let taxonomy_term__unit__weight_example = {
    id: taxonomy_term__unit__weight_uuid,
    attributes: {
        name: "lbs"
    }
};
////overlays and constants
let taxonomy_term__unit__weight = new builder.SchemaOverlay({
    typeAndBundle: "taxonomy_term--unit",
    name: "weight",
    validExamples: [ taxonomy_term__unit__weight_example ],
    //erroredExamples: [ taxonomy_term__unit__weight_error ]
});
//LOOK from taxonomy list in surveystack beta management question 22, list needs to be updated
taxonomy_term__unit__weight.setEnum({
    attribute: "name",
    valuesArray: [
        "lbs"
    ],
    description: "Units used to measure the average weight of the grazing herd."
});
taxonomy_term__unit__weight.setMainDescription("Units in kg or lb.");

//quantity - count LOOK do we need units for these quantities? they are just numbers so I would assume not
//Animal Count
////Examples
let quantity__standard__animal_count_example = {
    id: quantity__standard__animal_count_uuid,
    attributes: {
        measure: "count"
    }
};
let quantity__standard__animal_count_error = {
    id: quantity__standard__animal_count_uuid,
    attributes: {
        measure: "weight"
    }
};
////overlays and constants
let quantity__standard__animal_count = new builder.SchemaOverlay({
    typeAndBundle: "quantity--standard",
    name: "animal_count",
    validExamples: [ quantity__standard__animal_count_example ],
    erroredExamples: [quantity__standard__animal_count_error]
});
quantity__standard__animal_count.setMainDescription("Animal count refers to the number of animals involved in this grazing event.");
quantity__standard__animal_count.setConstant({
    attribute: "measure",
    value:"count"
});

//quantity - sub-paddocks
////examples
let quantity__standard__subpaddocks_example = {
    id: quantity__standard__subpaddocks_uuid,
    attributes: {
        measure: "count"
    }
};
let quantity__standard__subpaddocks_error = {
    id: quantity__standard__subpaddocks_uuid,
    attributes: {
        measure: "weight"
    }
};
let quantity__standard__subpaddocks = new builder.SchemaOverlay({
    typeAndBundle: "quantity--standard",
    name: "subpaddocks",
    validExamples: [ quantity__standard__subpaddocks_example ],
    erroredExamples: [ quantity__standard__subpaddocks_error ]
});
quantity__standard__subpaddocks.setMainDescription("The number of subpaddocks or sections the grazed field is split into.");
quantity__standard__subpaddocks.setConstant({
    attribute: "measure",
    value:"count"
});

//convention
let grazingConvention = new builder.ConventionSchema({
    title: "Grazing log",
    version: "0.0.1",
    schemaName:"log--activity--grazing",
    repoURL:"www.gitlabrepo.com/version/farmos_conventions",
    //LOOK set description
    description:`## Purpose\n
Grazing logs can be created for planting termination or for tracking general herd managment activities.\n
## Specification\n
text\n`,
    validExamples: [],
    erroredExamples: []
});

//add local attributes
grazingConvention.addAttribute( { schemaOverlayObject:log__activity__grazing, attributeName: "log--activity--grazing", required: true } );
grazingConvention.addAttribute( { schemaOverlayObject:quantity__standard__animal_count, attributeName: "quantity--standard--animal_count", required: false } );
grazingConvention.addAttribute( { schemaOverlayObject:quantity__standard__animal_weight, attributeName: "quantity--standard--animal_weight", required: false } );
grazingConvention.addAttribute( { schemaOverlayObject:taxonomy_term__unit__weight, attributeName: "taxonomy_term--unit--weight", required: false } );
grazingConvention.addAttribute( { schemaOverlayObject:quantity__standard__subpaddocks, attributeName: "quantity--standard--subpaddocks", required: false } );

//add global attributes
grazingConvention.addAttribute( { schemaOverlayObject:"taxonomy_term--log_category", attributeName: "taxonomy_term--log_category", required: false } );
grazingConvention.addAttribute({ schemaOverlayObject:"taxonomy_term--log_category--grazing", attributeName: "taxonomy_term--log_category--grazing", required: true  });
grazingConvention.addAttribute({ schemaOverlayObject:"quantity--standard--area_percentage", attributeName: "quantity--standard--area_percentage", required: false});
grazingConvention.addAttribute({ schemaOverlayObject:"taxonomy_term--unit--%", attributeName: "taxonomy_term--unit--%", required: false});
grazingConvention.addAttribute( { schemaOverlayObject:"asset--plant--planting", attributeName: "asset--plant--planting", required: true } );

//relationships
grazingConvention.addRelationship( { containerEntity:"log--activity--grazing" , relationName:"category" , mentionedEntity:"taxonomy_term--log_category" , required: false } );
grazingConvention.addRelationship( { containerEntity:"log--activity--grazing" , relationName:"category" , mentionedEntity:"taxonomy_term--log_category--grazing" , required: true } );
grazingConvention.addRelationship( { containerEntity:"log--activity--grazing" , relationName:"quantity" , mentionedEntity:"quantity--standard--area_percentage" , required: false } );
grazingConvention.addRelationship( { containerEntity:"quantity--standard--area_percentage" , relationName:"units" , mentionedEntity:"taxonomy_term--unit--%" , required: false } );
grazingConvention.addRelationship( { containerEntity:"log--activity--grazing" , relationName:"quantity" , mentionedEntity:"quantity--standard--animal_count" , required: false } );
grazingConvention.addRelationship( { containerEntity:"log--activity--grazing" , relationName:"quantity" , mentionedEntity:"quantity--standard--animal_weight" , required: false } );
grazingConvention.addRelationship( { containerEntity:"log--activity--grazing" , relationName:"quantity" , mentionedEntity:"quantity--standard--subpaddocks" , required: false } );
grazingConvention.addRelationship( { containerEntity:"log--activity--grazing", relationName:"asset", mentionedEntity:"asset--plant--planting", required: true } );
grazingConvention.addRelationship( { containerEntity:"quantity--standard--animal_weight" , relationName:"units" , mentionedEntity:"taxonomy_term--unit--weight" , required: false } );

let asset__plant__planting_attributes = grazingConvention.overlays['asset--plant--planting'].validExamples?.[0]?.attributes;
let asset__plant__planting_uuid = grazingConvention.overlays['asset--plant--planting'].validExamples?.[0]?.id;

// Rewrite the example
let grazingConventionExample = {
    id: grazing_convention_uuid,
    type: "Object",
    "asset--plant--planting": {
        id: asset__plant__planting_uuid,
        attributes: asset__plant__planting_attributes
    },
    "log--activity--grazing": {
        id: log__activity__grazing_uuid,
        attributes: {
            name: "example grazing log",
            status: "done",
            timestamp: ( new Date() ).toISOString(),
        },
        relationships: {
            asset: { data: [
                {
                    type: "asset--plant",
                    id: asset__plant__planting_uuid,
                }
            ] },
            category: { data: [
                {
                    type: "taxonomy_term--log_category",
                    id:taxonomy_term__log_category__grazing_uuid
                },
                {
                    type: "taxonomy_term--log_category",
                    id:taxonomy_term__log_category_uuid
                }
            ] },
            quantity: { data: [
                {
                    type: "quantity--standard",
                    id: quantity__standard__area_percentage_uuid
                },
                {
                    type: "quantity--standard",
                    id: quantity__standard__animal_count_uuid
                },
                {
                    type: "quantity--standard",
                    id: quantity__standard__animal_weight_uuid
                },
                {
                    type: "quantity--standard",
                    id: quantity__standard__subpaddocks_uuid
                }
            ] }
        },
    },
    "taxonomy_term--log_category--grazing": {
        id: taxonomy_term__log_category__grazing_uuid,
        attributes: {
            name: "grazing"
        }
    },
    "taxonomy_term--log_category": {
        id: taxonomy_term__log_category_uuid,
        attributes: {
            name: "weed_control"
        }
    },
    "quantity--standard--area_percentage": {
        id: quantity__standard__area_percentage_uuid,
        attributes: {
            label: "area"
        },
        relationships: {
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: taxonomy_term__unit__percentage_uuid
                }
            ] }
        }
    },
    "taxonomy_term--unit--%":  {
        id: taxonomy_term__unit__percentage_uuid,
        attributes: {
            name: "%"
        },   
    },
    "quantity--standard--animal_count": {
        id: quantity__standard__animal_count_uuid,
        attributes: {
            label: "head"
        },
        relationships: { }
    },
    "quantity--standard--subpaddocks": {
        id: quantity__standard__subpaddocks_uuid,
        attributes: {
            label: "subsections"
        },
        relationships: { }
    },
    "quantity--standard--animal_weight": {
        id: quantity__standard__animal_weight_uuid,
        attributes: {
            label: "weight"
        },
        relationships: { 
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: taxonomy_term__unit__weight_uuid
                }
            ] }
        }
    },
    "taxonomy_term--unit--weight":  {
        id: taxonomy_term__unit__weight_uuid,
        attributes: {
            name: "lbs"
        },   
    },
};

// Errored convention example
let grazingConventionError = {
    id: grazing_convention_uuid,
    type: "Object",
    "asset--plant--planting": {
        id: asset__plant__planting_uuid,
        attributes: asset__plant__planting_attributes
    },
    "log--activity--grazing": {
        id: log__activity__grazing_uuid,
        attributes: {
            name: "example grazing log",
            status: "done",
            timestamp: ( new Date() ).toISOString(),
        },
        relationships: {
/*            category: { data: [
                {
                    type: "taxonomy_term--log_category",
                    id:logCategoryUUID
                }
            ] }, */
            quantity: { data: [
                {
                    type: "quantity--standard",
                    id: quantity__standard__area_percentage_uuid
                },
                {
                    type: "quantity--standard",
                    id: quantity__standard__area_percentage_uuid
                },
                {
                    type: "quantity--standard",
                    id: quantity__standard__animal_weight_uuid
                },
                {
                    type: "quantity--standard",
                    id: quantity__standard__subpaddocks_uuid
                }
            ] }
        },
    },
    //log_category: logCategoryExample,
    "quantity--standard--area_percentage": {
        id: quantity__standard__area_percentage_uuid,
        attributes: {
            label: "area"
        },
        relationships: {
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: taxonomy_term__unit__percentage_uuid
                }
            ] }
        }
    },
    "taxonomy_term--unit--%":  {
        id: taxonomy_term__unit__percentage_uuid,
        attributes: {
            name: "%"
        },   
    },
    "quantity--standard--animal_count": {
        id: quantity__standard__animal_count_uuid,
        attributes: {
            label: "head"
        },
        relationships: { }
    },
    "quantity--standard--subpaddocks": {
        id: quantity__standard__subpaddocks_uuid,
        attributes: {
            label: "subsections"
        },
        relationships: { }
    },
    "quantity--standard--animal_weight": {
        id: quantity__standard__animal_weight_uuid,
        attributes: {
            label: "weight"
        },
        relationships: { 
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: taxonomy_term__unit__weight_uuid
                }
            ] }
        }
    },
    "taxonomy_term--unit--weight":  {
        id: taxonomy_term__unit__weight_uuid,
        attributes: {
            type: "taxonomy_term--unit",
            name: "lbs"
        },   
    },
};

grazingConvention.validExamples = [grazingConventionExample];
grazingConvention.erroredExamples = [grazingConventionError];

let test = grazingConvention.testExamples();
let storageOperation = grazingConvention.store();
