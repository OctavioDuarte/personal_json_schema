// Survey: https://app.surveystack.io/surveys/6346c1e467f839000121503e/edit
// Section: 22, pesticide, herbicide, fungicide
const builder = require(`../src/convention_builder`);
const { randomUUID } = require('crypto');
const fs = require('fs');

//UUIDs
let log__input__herbicide_uuid = randomUUID();
let quantity__material__rate_uuid = randomUUID();
let taxonomy_term__unit__rate_uuid = randomUUID();
let herbicide_convention_uuid = randomUUID();
let quantity__standard__area_percentage_uuid = randomUUID();
let taxonomy_term__unit__percentage_uuid = randomUUID();
let taxonomy_term__log_category__pest_disease_control_uuid = randomUUID();
let taxonomy_term__material_type__herbicide_uuid = randomUUID();
let quantity__standard__active_ingredient_percent_uuid = randomUUID();

//Main entity
////Examples
let log__input__herbicide_example = {
    id:log__input__herbicide_uuid,
    attributes: {
        name: "example herbicide log",
        status:"done",
    }
};
let log__input__herbicide_error = {
    id:log__input__herbicide_uuid,
    attributes: {
        name: "example herbicide log",
        status:"pending",
    }
};
////overlays and constants
let log__input__herbicide = new builder.SchemaOverlay({
    typeAndBundle: 'log--input',
    name: 'herbicide',
    validExamples: [log__input__herbicide_example],
    erroredExamples: [log__input__herbicide_error]    
});
log__input__herbicide.setMainDescription("This log records herbicide, pesticide and fungicide inputs.");

log__input__herbicide.setConstant({
    attribute:"status",
    value:"done",
    description: "The status should always be set to done to inherit the area."
});

//let logCategoryExample = {
//    id: taxonomy_term__log_category__pest_disease_control_uuid,
//    attributes: {
//        name: "weed_control"
//    }
//};
//let logCategoryError = {
//    id: taxonomy_term__log_category__pest_disease_control_uuid,
//    attributes: {
//        label: "blah"
//    }
//};

////overlays and constants
//added to global
//let logCategory = new builder.SchemaOverlay({
//    typeAndBundle: "taxonomy_term--log_category",
//    name: "pest_disease_control",
//    validExamples: [logCategoryExample],
//    erroredExamples: [logCategoryError],
//});

//logCategory.setMainDescription("The log category is set to pest disease control and/or weed control.");


//quantity - rate - moved to global
////Examples
//let herbicideRateExample = {
//    id: herbicideRateUUID,
//    attributes: {
//        label: "application_rate",
//        measure: "rate"
//    }
//};
////overlays and constants
//let herbicideRate = new builder.SchemaOverlay({
//    typeAndBundle: "quantity--material",
//    name: "rate",
//    validExamples: [ herbicideRateExample ]
//});
//herbicideRate.setMainDescription("The herbicide rate indicates the amount of product used for every surface unit.");
//herbicideRate.setConstant({
//    attribute: "measure",
//    value:"rate"
//});

//taxonomy term - units
////Examples
//let herbicideRateUnitExample = {
//    id: taxonomy_term__unit__rate_uuid,
//    name: "oz_acre"
//};
//let herbicideRateUnitError = {
//    id: taxonomy_term__unit__rate_uuid,
//    name: "ounces_per_acre"
//};
////overlays and constants
//let herbicideRateUnit = new builder.SchemaOverlay({
//    typeAndBundle: "taxonomy_term--unit",
//    name: "rate_unit",
//    validExamples: [ herbicideRateUnitExample ],
//    erroredExamples: [ herbicideRateUnitError ]
//});
//herbicideRateUnit.setMainDescription("The units refer to the application amount in a mass to area ratio.")

//rateUnit.setEnum({
//    attribute: "name",
//    valuesArray: [
//        "cubic_ft",
//        "cubic_yard",
//        "gallons_acre",
//        "g_acre",
//        "g_bedft",
//        "g_rowft",
//        "g_sqft",
//        "in",
//        "kg_acre",
//        "kg_ha",
//        "kg_rowmeter",
//        "kg_sqm",
//        "lbs_acre",
//        "lbs_bedft",
//        "lbs_rowft",
//        "lbs_sqft",
//        "liters_acre",
//        "metric_tons_acre",
//        "metric_tons_ha",
//        "oz_acre",
//        "oz_bedft",
//        "oz_rowft",
//        "oz_sqft",
//        "gallons",
//        "g",
//        "kg",
//        "lbs",
//        "liters",
//        "metric_tons",
//        "oz",
//        "us_tons",
//        "us_tons_acre"
//    ],
//    description: "Several units are available, both imperial and metric. They are all compatible mass to area ratios."
//});

let taxonomy_term__material_type__herbicide = new builder.SchemaOverlay({
    typeAndBundle: "taxonomy_term--material_type",
    name: "herbicide",
    validExamples: [],
    erroredExamples: []
});
taxonomy_term__material_type__herbicide.setMainDescription("The name of the herbicide is stored here.");

//LOOK from taxonomy list in surveystack beta management question 22
taxonomy_term__material_type__herbicide.setEnum({
    attribute: "name",
    valuesArray: [
        "2_4_d",
        "achieve",
        "aero",
        "aim",
        "assert_300sc",
        "assure",
        "basagran_forte",
        "broadleaf",
        "brotex_240",
        "caramba",
        "centurion",
        "citric_acid",
        "clethodim",
        "coragen",
        "cornerstone_plus",
        "delaro",
        "everest",
        "express_pro",
        "fluroxypyr",
        "fulvic_acid",
        "glyphosate",
        "grow_ttf",
        "liberty",
        "lorsban",
        "matador_120ec",
        "mcpa_amine_600",
        "prosaro",
        "puma_advance",
        "quilt",
        "roundup",
        "silencer",
        "spartan_charge",
        "startup_540",
        "stellar",
        "toledo_3.6f",
        "toledo_45wp",
        "victor",
        "widematch",
        "zidua"        
    ],
    description: "This product name information is stored in a SurveyStack ontology list of herbicides, insecticides and fungicides."
});

//taxonomy term - active ingredient percent
let quantity__standard__active_ingredient_percent = new builder.SchemaOverlay({
    typeAndBundle: "quantity--standard",
    name: "active ingredient percent",
    //LOOK do we need new examples here, could we use the examples from herbicide rate?
    //validExamples: [ activeIngredientExample ],
    //erroredExamples: [ activeIngredientError ]
});
//LOOK - what should the value be here??
quantity__standard__active_ingredient_percent.setConstant({
    attribute: "measure",
    value:"rate"
});
quantity__standard__active_ingredient_percent.setMainDescription("The % of the active ingredient in the herbicide or pesticide product.");

//Convention
// Object
let herbicideConvention = new builder.ConventionSchema({
    title: "Herbicide/pesticide",
    version: "0.0.1",
    schemaName:"log--input--herbicide",
    repoURL:"www.gitlabrepo.com/version/farmos_conventions",
    description:`## Purpose\n
Herbicide input log to be applied to a plant asset only if the farmer indicated so in their planting records. In other words, this log needs to be assigned to a planting.\n
## Specification\n
text\n`,
    validExamples: [],
    erroredExamples: []
});

////add local attributes
herbicideConvention.addAttribute( { schemaOverlayObject: log__input__herbicide, attributeName: "log--input--herbicide", required: true } );
herbicideConvention.addAttribute( { schemaOverlayObject: taxonomy_term__material_type__herbicide, attributeName: "taxonomy_term--material_type--herbicide", required: true});
herbicideConvention.addAttribute( { schemaOverlayObject: quantity__standard__active_ingredient_percent, attributeName: "quantity--standard--active_ingredient_percent", required: true});

//add global attributes
herbicideConvention.addAttribute( { schemaOverlayObject:"asset--plant--planting", attributeName: "asset--plant--planting", required: true } );
herbicideConvention.addAttribute( { schemaOverlayObject: "quantity--standard--area_percentage", attributeName: "quantity--standard--area_percentage", required: false});
herbicideConvention.addAttribute( { schemaOverlayObject: "taxonomy_term--unit--%", attributeName: "taxonomy_term--unit--%", required: false});
//LOOK we always push quantity here even if there is no quantity specified, to ensure that the type or name information is stored in the material reference of the quantity
herbicideConvention.addAttribute( { schemaOverlayObject: "quantity--material--rate", attributeName: "quantity--material--rate", required: true});
herbicideConvention.addAttribute( { schemaOverlayObject: "taxonomy_term--unit--rate", attributeName: "taxonomy_term--unit--rate", required: true});
herbicideConvention.addAttribute( { schemaOverlayObject:"taxonomy_term--log_category--pest_disease_control", attributeName: "taxonomy_term--log_category--pest_disease_control", required: true } );

////add relationships
herbicideConvention.addRelationship( { containerEntity:"log--input--herbicide", relationName:"asset", mentionedEntity:"asset--plant--planting", required: true } );
herbicideConvention.addRelationship( { containerEntity:"log--input--herbicide" , relationName:"quantity" , mentionedEntity:"quantity--standard--area_percentage" , required: false } );
herbicideConvention.addRelationship( { containerEntity:"quantity--standard--area_percentage" , relationName:"units" , mentionedEntity:"taxonomy_term--unit--%" , required: false } );
herbicideConvention.addRelationship( { containerEntity:"log--input--herbicide" , relationName:"quantity" , mentionedEntity:"quantity--material--rate" , required: false } );
herbicideConvention.addRelationship( { containerEntity:"quantity--material--rate" , relationName:"units" , mentionedEntity:"taxonomy_term--unit--rate" , required: false } );
herbicideConvention.addRelationship( { containerEntity:"log--input--herbicide" , relationName:"category" , mentionedEntity:"taxonomy_term--log_category--pest_disease_control" , required: true } );
herbicideConvention.addRelationship( { containerEntity:"quantity--material--rate", relationName:"material_type" , mentionedEntity:"taxonomy_term--material_type--herbicide" , required: true } );
herbicideConvention.addRelationship( { containerEntity:"log--input--herbicide" , relationName:"quantity" , mentionedEntity:"quantity--standard--active_ingredient_percent" , required: false } );
herbicideConvention.addRelationship( { containerEntity:"quantity--standard--active_ingredient_percent" , relationName:"units" , mentionedEntity:"taxonomy_term--unit--%" , required: false } );


let asset__plant__planting_attributes = herbicideConvention.overlays['asset--plant--planting'].validExamples?.[0]?.attributes;
let asset__plant__planting_uuid = herbicideConvention.overlays['asset--plant--planting'].validExamples?.[0]?.id;

let herbicideConventionExample = {
    id: herbicide_convention_uuid,
    type: "Object",
    "asset--plant--planting": {
        id: asset__plant__planting_uuid,
        attributes: asset__plant__planting_attributes
    },
    "log--input--herbicide": {
        id: log__input__herbicide_uuid,
        attributes: {
            name: "example herbicide log",
            status: "done",
            timestamp: ( new Date() ).toISOString(),
        },
        relationships: {
            category: { data: [
                {
                    type: "taxonomy_term--log_category",
                    id:taxonomy_term__log_category__pest_disease_control_uuid
                }
            ] },
            quantity: { data: [
                {
                    type: "quantity--standard",
                    id: quantity__standard__area_percentage_uuid
                },
                {
                    type: "quantity--material",
                    id: quantity__material__rate_uuid
                },
                {
                    type: "quantity--standard",
                    id: quantity__standard__active_ingredient_percent_uuid
                }
            ] },
            asset: { data: [
                {
                    type: "asset--plant",
                    id: asset__plant__planting_uuid
                }
            ]}
        },
    },
    "taxonomy_term--log_category--pest_disease_control": {
        id: taxonomy_term__log_category__pest_disease_control_uuid,
        attributes: {
            name: "pest_disease_control"
        },   
    },
    "quantity--standard--area_percentage": {
        id: quantity__standard__area_percentage_uuid,
        attributes: {
            label: "area"
        },
        relationships: {
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: taxonomy_term__unit__percentage_uuid
                }
            ] }
        }
    },
    "quantity--standard--active_ingredient_percent": {
        id: quantity__standard__active_ingredient_percent_uuid,
        attributes: {
            label: "active ingredient percent"
        },
        relationships: {
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: taxonomy_term__unit__percentage_uuid
                }
            ] }
        }
    },
    "taxonomy_term--unit--%":  {
        id: taxonomy_term__unit__percentage_uuid,
        attributes: {
            name: "%"
        },   
    },
    "quantity--material--rate": {
        id: quantity__material__rate_uuid,
        attributes: {
            label: "quantity",
            measure: "rate"
        },
        relationships: { 
            units: 
            { data: [
                {
                    type:"taxonomy_term--unit",
                    id: taxonomy_term__unit__rate_uuid
                }
            ] },
            material_type:
            { data: [
                {
                    type:"taxonomy_term--material_type",
                    id: taxonomy_term__material_type__herbicide_uuid
                }
            ] },
        }
    },
    "taxonomy_term--unit--rate":  {
        id: taxonomy_term__unit__rate_uuid,
        attributes: {
            //type: "taxonomy_term--unit",
            name: "cubic_ft"
        },   
    },
    "taxonomy_term--material_type--herbicide": {
        id: taxonomy_term__material_type__herbicide_uuid,
        attributes: {
            name: "aero"
        }
    }
};
let herbicideConventionError = {
    id: herbicide_convention_uuid,
    type: "Object",
    "asset--plant--planting": {
        id: asset__plant__planting_uuid,
        attributes: asset__plant__planting_attributes
    },
    "log--input--herbicide": {
        id: log__input__herbicide_uuid,
        attributes: {
            name: "example herbicide log",
            status: "done",
            timestamp: ( new Date() ).toISOString(),
        },
        relationships: {
            /*category: { data: [
                {
                    type: "taxonomy_term--log_category",
                    id:taxonomy_term__log_category__pest_disease_control_uuid
                }
            ] },*/
            quantity: { data: [
                {
                    type: "quantity--standard",
                    id: quantity__standard__area_percentage_uuid
                }
                /*{
                    type: "quantity--standard",
                    id: herbicideRateUUID
                }*/
            ] }
        },
    },
    //log_category: logCategoryExample,
    "quantity--standard--area_percentage": {
        id: quantity__standard__area_percentage_uuid,
        attributes: {
            label: "area"
        },
        relationships: {
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: taxonomy_term__unit__percentage_uuid
                }
            ] }
        }
    },
    "taxonomy_term--unit--%":  {
        id: taxonomy_term__unit__percentage_uuid,
        attributes: {
            name: "%"
        },   
    },
    "quantity--material--rate": {
        id: quantity__material__rate_uuid,
        attributes: {
            label: "quantity"
        },
        relationships: { 
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: taxonomy_term__unit__rate_uuid
                }
            ] }
        }
    },
    "taxonomy_term--unit--rate":  {
        id: taxonomy_term__unit__rate_uuid,
        attributes: {
            name: "cubic_ft"
        },   
    },
};

herbicideConvention.validExamples = [herbicideConventionExample];
herbicideConvention.erroredExamples = [herbicideConventionError];

let test = herbicideConvention.testExamples();
let storageOperation = herbicideConvention.store();
