// Survey: https://app.surveystack.io/surveys/6346c1e467f839000121503e/edit
// Section: 40, irrigation
const builder = require(`../src/convention_builder`);
const { randomUUID } = require('crypto');
const fs = require('fs');

//UUIDS
let log__input__irrigation_uuid = randomUUID();
let taxonomy_term__log_category__irrigation_uuid = randomUUID();
let irrigation_convention_uuid = randomUUID();
let quantity__material__total_water_uuid = randomUUID();
let taxonomy_term__unit__total_water_uuid = randomUUID();
let taxonomy_term__unit__effectiveness_rating_uuid = randomUUID();
let quantity__standard__effectiveness_uuid = randomUUID();
let quantity__standard__area_percentage_uuid = randomUUID();
let taxonomy_term__unit__percentage_uuid = randomUUID();

//Main entity
////Examples
let log__input__irrigation_example = {
    id:log__input__irrigation_uuid,
    attributes: {
        name: "irrigation log",
        status:"done",
    }
};
let log__input__irrigation_error = {
    id:log__input__irrigation_uuid,
    attributes: {
        name: "irrigation log",
        status:"pending",
    }
};
////overlays and constants
let log__input__irrigation = new builder.SchemaOverlay({
    typeAndBundle: 'log--input',
    name: 'irrigation',
    validExamples: [log__input__irrigation_example],
    erroredExamples: [log__input__irrigation_error]    
});
//LOOK add description
log__input__irrigation.setMainDescription(
    "Irrigation logs are used to record watering events. It records % of field covered, water source, quantity, and general effectivness."
    );

log__input__irrigation.setConstant({
    attribute:"status",
    value:"done"
});

//moved to global
//taxonomy term - log_category
////Examples
//let logCategoryExample = {
//    id: taxonomy_term__log_category__irrigation_uuid,
//    attributes: {
//        name: "irrigation"
//    }
//};
//let logCategoryError = {
//    id: taxonomy_term__log_category__irrigation_uuid,
//    attributes: {
//        name: "activity"
//    }
//};
////overlays and constants
//let logCategory = new builder.SchemaOverlay({
//    typeAndBundle: "taxonomy_term--log_category",
//    name: "irrigation",
//    validExamples: [logCategoryExample],
//    erroredExamples: [logCategoryError],
//});
//logCategory.setConstant({
//    attribute:"name",
//    value:"irrigation"
//});
//logCategory.setMainDescription("Log category is set to irrigation.")

//quantities - total water
////Examples
let quantity__material__total_water_example = {
    id: quantity__material__total_water_uuid,
    attributes: {
        name: "total water",
        measure:"value",
    }
};
let quantity__material__total_water_error = {
    id: quantity__material__total_water_uuid,
    attributes: {
        name: "total water",
        measure:"volume",
    }
};
////Overlays and constants
let quantity__material__total_water = new builder.SchemaOverlay({
    typeAndBundle: "quantity--material",
    name: "total_water",
    validExamples: [ quantity__material__total_water_example ],
    erroredExamples: [ quantity__material__total_water_error ]
});
//LOOK set description
quantity__material__total_water.setMainDescription("This represents the total quantity of water added.");
//LOOK what should the measure be here?
quantity__material__total_water.setConstant({
    attribute: "measure",
    value:"value"
});
//totalWater.setConstant({
//    attribute: "label",
//    value:"total_water"
//});

//taxonomy term - units
let taxonomy_term__unit__total_water = new builder.SchemaOverlay({
    typeAndBundle: "taxonomy_term--unit",
    name: "total_water"
});
taxonomy_term__unit__total_water.setMainDescription("Units for total water added are recorded in inches.");
taxonomy_term__unit__total_water.setEnum({
    attribute: "name",
    valuesArray: [
        "inches",        
    ],
});

//quantity - effectiveness
////Examples
let quantity__standard__effectiveness_example = {
    id: quantity__standard__effectiveness_uuid,
    attributes: {
        name: "effectiveness"
    }
};
let quantity__standard__effectiveness_error = {
    id: quantity__standard__effectiveness_uuid,
    attributes: {
        name: "activity"
    }
};
////overlays and examples
let quantity__standard__effectiveness = new builder.SchemaOverlay({
    typeAndBundle: "quantity--standard",
    name: "effectiveness",
    validExamples: [ quantity__standard__effectiveness_example ],
    erroredExamples: [ quantity__standard__effectiveness_error ]
});
quantity__standard__effectiveness.setConstant({
    attribute: "label",
    value:"effectiveness"
});
quantity__standard__effectiveness.setMainDescription("Estimate if the field was under watered, mostly watered, or well watered.");

//Taxonomy term -- unit
////Examples
let taxonomy_term__unit__effectiveness_rating_example = {
    id: taxonomy_term__unit__effectiveness_rating_uuid,
    attributes: {
        name: "Under watered (0)"
    }
};
let taxonomy_term__unit__effectiveness_rating_error = {
    id: taxonomy_term__unit__effectiveness_rating_uuid,
    attributes: {
        name: "good amount"
    }
};
////overlays and constants
let taxonomy_term__unit__effectiveness_rating = new builder.SchemaOverlay({
    typeAndBundle: "taxonomy_term--unit",
    name: "effectiveness_rating",
    validExamples: [ taxonomy_term__unit__effectiveness_rating_example ],
    erroredExamples: [ taxonomy_term__unit__effectiveness_rating_error ]
});
taxonomy_term__unit__effectiveness_rating.setEnum({
    attribute: "name",
    valuesArray: [
        "Under watered (0)",
        "Mostly watered (1)",
        "Well watered (2)",
    ],
    description: "This is an effectiveness scale with three levels from under watered to well watered."
});
taxonomy_term__unit__effectiveness_rating.setMainDescription("This question does not use measured units, this uses a subjective scale where the user will have to use the knowledge of their operation to judge how well the field was watered.")


//convention
let irrigationConvention = new builder.ConventionSchema({
    title: "Irrigation",
    version: "0.0.1",
    schemaName:"log--input--irrigation",
    repoURL:"www.gitlabrepo.com/version/farmos_conventions",
    //LOOK add description
    description:`## Purpose\n
Irrigation logs are used to record watering events.\n
## Specification\n
text\n`,
    validExamples: [],
    erroredExamples: []
});

//add local attributes
irrigationConvention.addAttribute( { schemaOverlayObject:log__input__irrigation, attributeName: "log--input--irrigation", required: true } );
irrigationConvention.addAttribute({ schemaOverlayObject:quantity__material__total_water, attributeName: "quantity--material--total_water", required: false});
irrigationConvention.addAttribute({ schemaOverlayObject:taxonomy_term__unit__total_water, attributeName: "taxonomy_term--unit--total_water", required: false});
irrigationConvention.addAttribute({ schemaOverlayObject:quantity__standard__effectiveness, attributeName: "quantity--standard--effectiveness", required: false});
irrigationConvention.addAttribute({ schemaOverlayObject:taxonomy_term__unit__effectiveness_rating, attributeName: "taxonomy_term--unit--effectiveness_rating", required: false});

//add global attributes
irrigationConvention.addAttribute( { schemaOverlayObject:"asset--plant--planting", attributeName: "asset--plant--planting", required: true } );
irrigationConvention.addAttribute({ schemaOverlayObject:"quantity--standard--area_percentage", attributeName: "quantity--standard--area_percentage", required: false});
irrigationConvention.addAttribute({ schemaOverlayObject:"taxonomy_term--unit--%", attributeName: "taxonomy_term--unit--%", required: false});
irrigationConvention.addAttribute({ schemaOverlayObject:"taxonomy_term--log_category--irrigation", attributeName: "taxonomy_term--log_category--irrigation", required: false});

irrigationConvention.addRelationship( { containerEntity:"log--input--irrigation", relationName:"asset", mentionedEntity:"asset--plant--planting", required: true } );
irrigationConvention.addRelationship( { containerEntity:"log--input--irrigation", relationName:"category", mentionedEntity:"taxonomy_term--log_category--irrigation", required: true } );
irrigationConvention.addRelationship( { containerEntity:"log--input--irrigation" , relationName:"quantity" , mentionedEntity:"quantity--standard--area_percentage" , required: false } );
irrigationConvention.addRelationship( { containerEntity:"quantity--standard--area_percentage" , relationName:"units" , mentionedEntity:"taxonomy_term--unit--%" , required: false } );
irrigationConvention.addRelationship( { containerEntity:"log--input--irrigation" , relationName:"quantity" , mentionedEntity:"quantity--material--total_water" , required: false } );
irrigationConvention.addRelationship( { containerEntity:"quantity--material--total_water" , relationName:"units" , mentionedEntity:"taxonomy_term--unit--total_water" , required: false } );
irrigationConvention.addRelationship( { containerEntity:"log--input--irrigation" , relationName:"quantity" , mentionedEntity:"quantity--standard--effectiveness" , required: false } );
irrigationConvention.addRelationship( { containerEntity:"quantity--standard--effectiveness" , relationName:"units" , mentionedEntity:"taxonomy_term--unit--effectiveness_rating" , required: false } );

let asset__plant__planting_attributes = irrigationConvention.overlays['asset--plant--planting'].validExamples?.[0]?.attributes;
let asset__plant__planting_uuid = irrigationConvention.overlays['asset--plant--planting'].validExamples?.[0]?.id;

// Convention example
let irrigationConventionExample = {
    id: irrigation_convention_uuid,
    type: "Object",
    "asset--plant--planting": {
        id: asset__plant__planting_uuid,
        attributes: asset__plant__planting_attributes
    },
    "log--input--irrigation": {
        id: log__input__irrigation_uuid,
        attributes: {
            name: "example irrigation log",
            status: "done",
            timestamp: ( new Date() ).toISOString(),
        },
        relationships: {
            asset: { data: [
                {
                    type: "asset--plant",
                    id: asset__plant__planting_uuid
                }
            ] },
            category: { data: [
                {
                    type: "taxonomy_term--log_category",
                    id:taxonomy_term__log_category__irrigation_uuid
                }
            ] },
            quantity: { data: [
                {
                    type: "quantity--standard",
                    id: quantity__standard__area_percentage_uuid
                },
                {
                    type: "quantity--material",
                    id: quantity__material__total_water_uuid
                },
                {
                    type: "quantity--standard",
                    id: quantity__standard__effectiveness_uuid
                }
            ] }
        },
    },
    "taxonomy_term--log_category--irrigation": {
        id: taxonomy_term__log_category__irrigation_uuid,
        attributes: {
            name: "irrigation"
        },
    }, 
    "quantity--standard--area_percentage": {
        id: quantity__standard__area_percentage_uuid,
        attributes: {
            label: "area"
        },
        relationships: {
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: taxonomy_term__unit__percentage_uuid
                }
            ] }
        }
    },
    "taxonomy_term--unit--%":  {
        id: taxonomy_term__unit__percentage_uuid,
        attributes: {
            name: "%"
        },   
    },
    "quantity--material--total_water": {
        id: quantity__material__total_water_uuid,
        attributes: {
            label: "total_water"
        },
        relationships: {
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: taxonomy_term__unit__total_water_uuid
                }
            ] }
        }
    },
    "taxonomy_term--unit--total_water":  {
        id: taxonomy_term__unit__total_water_uuid,
        attributes: {
            name: "inches"
        },   
    },
    "quantity--standard--effectiveness": {
        id: quantity__standard__effectiveness_uuid,
        attributes: {
            label: "effectiveness"
        },
        relationships: {
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: taxonomy_term__unit__effectiveness_rating_uuid
                }
            ] }
        }
    },
    "taxonomy_term__unit__effectiveness_rating":  {
        id: taxonomy_term__unit__effectiveness_rating_uuid,
        attributes: {
            name: "Under watered (0)"
        },   
    }
};

let irrigationConventionError = {
    id: irrigation_convention_uuid,
    type: "Object",
    "asset--plant--planting": {
        id: asset__plant__planting_uuid,
        attributes: asset__plant__planting_attributes
    },
    "log--input--irrigation": {
        id: log__input__irrigation_uuid,
        attributes: {
            name: "example irrigation log",
            status: "done",
            timestamp: ( new Date() ).toISOString(),
        },
        relationships: {
            category: { data: [
                {
                    type: "taxonomy_term--log_category",
                    id:taxonomy_term__log_category__irrigation_uuid
                }
            ] },
            quantity: { data: [
                {
                    type: "quantity--standard",
                    id: quantity__standard__area_percentage_uuid
                },
                /*{
                    type: "quantity--material",
                    id: quantity__material__total_water_uuid
                },*/
                {
                    type: "quantity--standard",
                    id: quantity__standard__effectiveness_uuid
                }
            ] }
        },
    },
    "taxonomy_term--log_category--irrigation": {
        id: taxonomy_term__log_category__irrigation_uuid,
        attributes: {
            name: "irrigation"
        },
    },
    "quantity--standard--area_percentage": {
        id: quantity__standard__area_percentage_uuid,
        attributes: {
            label: "area"
        },
        relationships: {
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: taxonomy_term__unit__percentage_uuid
                }
            ] }
        }
    },
    "taxonomy_term--unit--%":  {
        id: taxonomy_term__unit__percentage_uuid,
        attributes: {
            name: "%"
        },   
    },
    "quantity--material--total_water": {
        id: quantity__material__total_water_uuid,
        attributes: {
            label: "total_water"
        },
        relationships: {
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: taxonomy_term__unit__total_water_uuid
                }
            ] }
        }
    },
    "taxonomy_term--unit--total_water":  {
        id: taxonomy_term__unit__total_water_uuid,
        attributes: {
            name: "inches"
        },   
    },
};

irrigationConvention.validExamples = [irrigationConventionExample];
irrigationConvention.erroredExamples = [irrigationConventionError];

let test = irrigationConvention.testExamples();
let storageOperation = irrigationConvention.store();
