// Survey: https://app.surveystack.io/surveys/6346c1e467f839000121503e/edit
// Section: 39, Lime
const builder = require(`../src/convention_builder`);
const { randomUUID } = require('crypto');
const fs = require('fs');

//UUIDS
let log__input__lime_uuid = randomUUID();
let quantity__material__lime_uuid = randomUUID();
let taxonomy_term__unit__lime_uuid = randomUUID();
let lime_convention_uuid = randomUUID();
let quantity__standard__area_percentage_uuid = randomUUID();
let taxonomy_term__unit__percentage_uuid = randomUUID();
let taxonomy_term__material_type__lime_uuid = randomUUID();
let taxonomy_term__log_category__amendment_uuid = randomUUID();

//Main entity
////Example
let log__input__lime_example = {
    id:log__input__lime_uuid,
    attributes: {
        name: "example lime log",
        status:"done",
    }
};
let log__input__lime_error = {
    id:log__input__lime_uuid,
    attributes: {
        name: "example lime log",
        status:"false",
    }
};
////Overlays and constants
let log__input__lime = new builder.SchemaOverlay({
    typeAndBundle: 'log--input',
    name: 'lime',
    validExamples: [log__input__lime_example],
    erroredExamples: [log__input__lime_error]
});

//LOOK add description
log__input__lime.setMainDescription("Lime logs are used to record a lime application.");

log__input__lime.setConstant({
    attribute:"status",
    value:"done"
});

//lime quantity
////Examples
let quantity__material__lime_example = {
    id:quantity__material__lime_uuid,
    attributes: {
        label: "lime",
        measure:"value"
    }
};
let quantity__material__lime_error = {
    id:quantity__material__lime_uuid,
    attributes: {
        label: "compost",
        measure:"area",
    }
};
////Overlays and constants
let quantity__material__lime = new builder.SchemaOverlay({
    typeAndBundle: "quantity--material",
    name: "lime",
    validExamples: [ quantity__material__lime_example ],
    erroredExamples: [ quantity__material__lime_error ]
});
//LOOK set description
quantity__material__lime.setMainDescription("This represents the total quantity of lime applied. This quantity is always pushed even if empty in the survey.");
//LOOK what should the measure be here?
quantity__material__lime.setConstant({
    attribute: "measure",
    value:"value"
});
quantity__material__lime.setConstant({
    attribute: "label",
    value:"lime"
});

//Lime quantity units
////Examples
let taxonomy_term__unit__lime_example = {
    id:taxonomy_term__unit__lime_uuid,
    attributes: {
        name: "lbs_acres",
    }
};
let taxonomy_term__unit__lime_error = {
    id:taxonomy_term__unit__lime_uuid,
    attributes: {
        name: "ft",
    }
};
////Overlays and constants
let taxonomy_term__unit__lime = new builder.SchemaOverlay({
    typeAndBundle: "taxonomy_term--unit",
    name: "lime",
    validExamples: [ taxonomy_term__unit__lime_example ],
    erroredExamples: [ taxonomy_term__unit__lime_error ]
});
taxonomy_term__unit__lime.setEnum({
    attribute: "name",
    valuesArray: [
        "lbs_acres",
        "tons_acre",
        "kg_ha",
        "tons_ha"
    ],
});
taxonomy_term__unit__lime.setMainDescription("Lime units are recorded in weight per area.")

let taxonomy_term__material_type__lime = new builder.SchemaOverlay({
    typeAndBundle: "taxonomy_term--material_type",
    name: "lime",
    validExamples: [],
    erroredExamples: []
});
taxonomy_term__material_type__lime.setMainDescription("The type or brand of lime applied is stored here.");

taxonomy_term__material_type__lime.setEnum({
    attribute: "name",
    valuesArray: [
        "calcitic_lime",
        "crushed_lime",
        "dolomitic_lime",
        "high_cal",
        "nutralime_op_hi_cal",
        "nutralime_op_hi_mag",
        "pelletized_lime",
        "supercal_98g"
    ],
    description: "The type or brand name of the lime applied. SurveyStack has an ontology list of these types."
});

//Convention
let limeConvention = new builder.ConventionSchema({
    title: "Lime",
    version: "0.0.1",
    schemaName:"log--input--lime",
    repoURL:"www.gitlabrepo.com/version/farmos_conventions",
    //LOOK add description
    description:`## Purpose\n
Lime logs are created to record a lime input to a field or planting.\n
## Specification\n
text\n`,
    validExamples: [],
    erroredExamples: []
});

//add local attributes
limeConvention.addAttribute( { schemaOverlayObject:log__input__lime, attributeName: "log--input--lime", required: true } );
limeConvention.addAttribute( { schemaOverlayObject:quantity__material__lime, attributeName: "quantity--material--lime", required: false } );
limeConvention.addAttribute( { schemaOverlayObject:taxonomy_term__unit__lime, attributeName: "taxonomy_term--unit--lime", required: false } );
limeConvention.addAttribute( { schemaOverlayObject: taxonomy_term__material_type__lime, attributeName: "taxonomy_term--material_type--lime", required: false});

//add global attributes
limeConvention.addAttribute( { schemaOverlayObject:"asset--plant--planting", attributeName: "asset--plant--planting", required: true } );
limeConvention.addAttribute({ schemaOverlayObject:"quantity--standard--area_percentage", attributeName: "quantity--standard--area_percentage", required: false});
limeConvention.addAttribute({ schemaOverlayObject:"taxonomy_term--unit--%", attributeName: "taxonomy_term--unit--%", required: false});
limeConvention.addAttribute( { schemaOverlayObject:"taxonomy_term--log_category--amendment", attributeName: "taxonomy_term--log_category--amendment", required: false } );

//add relationships
limeConvention.addRelationship( { containerEntity:"log--input--lime", relationName:"asset", mentionedEntity:"asset--plant--planting", required: true } );
limeConvention.addRelationship( { containerEntity:"log--input--lime" , relationName:"quantity" , mentionedEntity:"quantity--standard--area_percentage" , required: false } );
limeConvention.addRelationship( { containerEntity:"quantity--standard--area_percentage" , relationName:"units" , mentionedEntity:"taxonomy_term--unit--%" , required: false } );
limeConvention.addRelationship( { containerEntity:"log--input--lime" , relationName:"quantity" , mentionedEntity:"quantity--material--lime" , required: false } );
limeConvention.addRelationship( { containerEntity:"quantity--material--lime" , relationName:"units" , mentionedEntity:"taxonomy_term--unit--lime" , required: false } );
limeConvention.addRelationship( { containerEntity:"log--input--lime" , relationName:"category" , mentionedEntity:"taxonomy_term--log_category--amendment" , required: true } );
limeConvention.addRelationship( { containerEntity:"quantity--material--lime", relationName:"material_type" , mentionedEntity:"taxonomy_term--material_type--lime" , required: true } );


let asset__plant__planting_attributes = limeConvention.overlays['asset--plant--planting'].validExamples?.[0]?.attributes;
let asset__plant__planting_uuid = limeConvention.overlays['asset--plant--planting'].validExamples?.[0]?.id;

//let logCategoryExampleAttributes = limeConvention.overlays.log_category.validExamples?.[0]?.attributes;
//let taxonomy_term__log_category__amendment_uuid = limeConvention.overlays.log_category.validExamples?.[0].id;

let limeConventionExample = {
        id: lime_convention_uuid,
        type: "Object",
        "asset--plant--planting": {
            id: asset__plant__planting_uuid,
            attributes: asset__plant__planting_attributes
        },
        "log--input--lime": {
            id: log__input__lime_uuid,
            attributes: {
                name: "example lime log",
                status: "done",
                timestamp: ( new Date() ).toISOString(),
            },
            relationships: {
                asset: { data: [
                    {
                        type: "asset--plant",
                        id: asset__plant__planting_uuid
                    }
                ]},
                category: { data: [
                    {
                        type: "taxonomy_term--log_category",
                        id: taxonomy_term__log_category__amendment_uuid
                    }
                ]},
                quantity: { data: [
                    {
                        type: "quantity--standard",
                        id: quantity__standard__area_percentage_uuid
                    },
                    {
                        type: "quantity--material",
                        id: quantity__material__lime_uuid
                    }
                ] }
            },
        },
        "quantity--standard--area_percentage": {
            id: quantity__standard__area_percentage_uuid,
            attributes: {
                label: "area"
            },
            relationships: {
                units: { data: [
                    {
                        type:"taxonomy_term--unit",
                        id: taxonomy_term__unit__percentage_uuid
                    }
                ] }
            }
        },
        "taxonomy_term--unit--%":  {
            id: taxonomy_term__unit__percentage_uuid,
            attributes: {
                name: "%"
            },   
        },
        "quantity--material--lime": {
            id: quantity__material__lime_uuid,
            attributes: {
                label: "lime",
                measure:"value"
            },
            relationships: {
                units: { data: [
                    {
                        type:"taxonomy_term--unit",
                        id: taxonomy_term__unit__lime_uuid
                    }
                ] },
                material_type: { data: [
                    {
                        type:"taxonomy_term--material_type",
                        id: taxonomy_term__material_type__lime_uuid
                    }
                ] }
            }
        },
        "taxonomy_term--unit--lime":  {
            id: taxonomy_term__unit__lime_uuid,
            attributes: {
                name: "lbs_acres"
            },   
        },
        "taxonomy_term--material_type--lime":  {
            id: taxonomy_term__material_type__lime_uuid,
            attributes: {
                name: "calcitic_lime"
            },   
        },
        "taxonomy_term--log_category--amendment":
        {
            id: taxonomy_term__log_category__amendment_uuid,
            attributes: {
                name: "amendment"
            }
        }
};
let limeConventionError = {
    id: lime_convention_uuid,
    type: "Object",
    "asset--plant--planting": {
        id: asset__plant__planting_uuid,
        attributes: asset__plant__planting_attributes
    },
    "log--input--lime": {
        id: log__input__lime_uuid,
        attributes: {
            name: "example lime log",
            status: "done",
            timestamp: ( new Date() ).toISOString(),
        },
        relationships: {
            asset: { data: [
                {
                    type: "asset--plant",
                    id: asset__plant__planting_uuid
                }
            ]},
            quantity: { data: [
                // {
                //     type: "quantity--standard",
                //     id: quantity__standard__area_percentage_uuid
                // },
                // // intentional error: a relationship is missing.
                // {
                //     type: "quantity--material",
                //     another: 1,
                //     // id: quantity__material__lime_uuid
                //     id: "aaaaaa"
                // }
            ] }
        },
    },
    "quantity--standard--area_percentage": {
        id: quantity__standard__area_percentage_uuid,
        attributes: {
            label: "area"
        },
        relationships: {
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: taxonomy_term__unit__percentage_uuid
                }
            ] }
        }
    },
    "taxonomy_term--unit--%":  {
        id: taxonomy_term__unit__percentage_uuid,
        attributes: {
            name: "%"
        },
    }
};

limeConvention.validExamples = [limeConventionExample];
limeConvention.erroredExamples = [limeConventionError];

let test = limeConvention.testExamples();
let storageOperation = limeConvention.store();
