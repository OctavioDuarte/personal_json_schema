// Survey: https://app.surveystack.io/surveys/6346c1e467f839000121503e/edit
// Section: 25, mowing
const builder = require(`../src/convention_builder`);
const { randomUUID } = require('crypto');
const fs = require('fs');

// ids for the examples
let log__activity__mowing_uuid = randomUUID();
let mowing_convention_uuid = randomUUID();
let quantity__standard__area_percentage_uuid = randomUUID();
let taxonomy_term__unit__percentage_uuid = randomUUID();

//Main entity
////Examples
let log__activity__mowing_example = {
    id:log__activity__mowing_uuid,
    attributes: {
        name: "example mowing log",
        status:"done",
    }
};
//Adding a second example to check that pattern is working
let log__activity__mowing_example2 = {
    id:log__activity__mowing_uuid,
    attributes: {
        name: "example raking log",
        status:"done",
    }
}
let log__activity__mowing_error = {
    id:log__activity__mowing_uuid,
    attributes: {
        name: "example log",
        status:"done",
    }
};

////Overlays and constants
let log__activity__mowing = new builder.SchemaOverlay({
    typeAndBundle: 'log--activity',
    name: 'mowing',
    validExamples: [log__activity__mowing_example , log__activity__mowing_example2 ],
    erroredExamples: [log__activity__mowing_error]    
});
log__activity__mowing.setMainDescription("This log includes any type of mowing, including mowing grass (for example, between rows in an orchard), mowing hay (in preparation for bailing), mowing for termination, etc.")

log__activity__mowing.setConstant({
    attribute:"status",
    value:"done"
});

log__activity__mowing.setPattern({
    attribute:"name",
    pattern:"(mowing)|(raking)|(tedding)",
    description:"There are several synonymous words for mowing, use pattern to accept any of them."
});

//let mowingLogTest = mowingLog.testExamples();

//convention
let mowingConvention = new builder.ConventionSchema({
    title: "Mowing",
    version: "0.0.1",
    schemaName:"log--activity--mowing",
    repoURL:"www.gitlabrepo.com/version/farmos_conventions",
    description:`## Purpose\n
Mowing logs can be created for termination or weed control.\n
## Specification\n
text\n`,
    validExamples: [],
    erroredExamples: []
});

//add local attributes
mowingConvention.addAttribute( { schemaOverlayObject:log__activity__mowing, attributeName: "log--activity--mowing", required: true } );

//add global attributes
mowingConvention.addAttribute({ schemaOverlayObject:"quantity--standard--area_percentage", attributeName: "quantity--standard--area_percentage", required: false});
mowingConvention.addAttribute({ schemaOverlayObject:"taxonomy_term--unit--%", attributeName: "taxonomy_term--unit--%", required: false});
mowingConvention.addAttribute( { schemaOverlayObject:"asset--plant--planting", attributeName: "asset--plant--planting", required: true } );

//add relationships
mowingConvention.addRelationship( { containerEntity:"log--activity--mowing" , relationName:"quantity" , mentionedEntity:"quantity--standard--area_percentage" , required: false } );
mowingConvention.addRelationship( { containerEntity:"quantity--standard--area_percentage" , relationName:"units" , mentionedEntity:"taxonomy_term--unit--%" , required: false } );
mowingConvention.addRelationship( { containerEntity:"log--activity--mowing", relationName:"asset", mentionedEntity:"asset--plant--planting", required: true } );

let asset__plant__planting_attributes = mowingConvention.overlays['asset--plant--planting'].validExamples?.[0]?.attributes;
let asset__plant__planting_uuid = mowingConvention.overlays['asset--plant--planting'].validExamples?.[0]?.id;

//examples
let mowingConventionExample = {
    id: mowing_convention_uuid,
    type: "Object",
    "asset--plant--planting": {
        id: asset__plant__planting_uuid,
        attributes: asset__plant__planting_attributes
    },
    "log--activity--mowing": {
        id: log__activity__mowing_uuid,
        attributes: {
            name: "example mowing log",
            status: "done",
            timestamp: ( new Date() ).toISOString(),
        },
        relationships: {
            asset: { data: [
                {
                    type: "asset--plant",
                    id: asset__plant__planting_uuid
                }
            ] },
            quantity: { data: [
                {
                    type: "quantity--standard",
                    id: quantity__standard__area_percentage_uuid
                }
            ] }
        },
    },
    "quantity--standard--area_percentage": {
        id: quantity__standard__area_percentage_uuid,
        attributes: {
            label: "area"
        },
        relationships: {
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: taxonomy_term__unit__percentage_uuid
                }
            ] }
        }
    },
    "taxonomy_term--unit--%":  {
        id: taxonomy_term__unit__percentage_uuid,
        attributes: {
            name: "%"
        },   
    }
};
let mowingConventionError = {
    id: mowing_convention_uuid,
    type: "Object",
    "asset--plant--planting": {
        id: asset__plant__planting_uuid,
        attributes: asset__plant__planting_attributes
    },
    /*"log--activity--mowing": {
        id: log__activity__mowing_uuid,
        attributes: {
            name: "example mowing log",
            status: "done",
        },
        relationships: {
            quantity: { data: [
                {
                    type: "quantity--standard",
                    id: quantity__standard__area_percentage_uuid
                }
            ] }
        },
    },*/
    "quantity--standard--area_percentage": {
        id: quantity__standard__area_percentage_uuid,
        attributes: {
            label: "area"
        },
        relationships: {
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: taxonomy_term__unit__percentage_uuid
                }
            ] }
        }
    }
};

mowingConvention.validExamples = [mowingConventionExample];
mowingConvention.erroredExamples = [mowingConventionError];

let test = mowingConvention.testExamples();
let storageOperation = mowingConvention.store();
