// NRCS CEMA 216, Soil Health Testing

// Main source of schema convention  functionality
const builder = require(`../src/convention_builder`);
// this function is needed to generate the UUIDs for our examples
const { randomUUID } = require('crypto');
// to deal with the filesystem
const fs = require('fs');


// Plan
// Encode all entitiwise requirements as overlays
// Encode the structure our overlays form as a convention
// Write examples both of succesful convention cases and examples for important errors.

// Main entity: Modus lab log test

// The lab log test is related to test quantities.
// The test quantities are related to test_method taxonomy terms.


let cemaTestUUID = randomUUID();

let validCemaExample = {
    id: cemaTestUUID,
    attributes: {
        name: "cema_216",
        status:"done",
        lab_test_type: "soil"
    }
};

let wrongValidCemaExample = {
    id: cemaTestUUID,
    attributes: {
        name: "cema_216",
        status:"ready",
        lab_test_type: "soil"
    }
};

let failedCemaExample = {
    id: cemaTestUUID,
    attributes: {
        name: "cema_216",
        status:"done",
        lab_test_type: "animal"
    }
};

// Overlays
let cemaTest = new builder.SchemaOverlay({
    typeAndBundle: "log--lab_test",
    name: "cema_test",
    validExamples: [validCemaExample, wrongValidCemaExample],
    erroredExamples:  [failedCemaExample]
});

cemaTest.setMainDescription("To document a valid CEMA test, a geographic location and a field (represented as a plant asset) are required. A MODUS test is central to the meaning of the tesst, which would otherwise lack comparability.");
cemaTest.setConstant({
    attribute:"status",
    value: "done"
});
cemaTest.setConstant({
    attribute:"lab_test_type",
    value:"soil"
});

let testCemaOverlay = cemaTest.testExamples();
testCemaOverlay.validExamples[1].errors;
// the error correctly highlights what was wrong.
