// Survey: https://app.surveystack.io/surveys/6346c1e467f839000121503e/edit
// Section: 32, Organic Matter Inputs
const builder = require(`../src/convention_builder`);
const { randomUUID } = require('crypto');
const fs = require('fs');

// ids for the examples
let log__input_organic_matter_uuid = randomUUID();
let taxonomy_term__log_category__amendment_uuid = randomUUID();
let quantity__standard__moisture_percentage_uuid = randomUUID();
let quantity__standard__nitrogen_percentage_uuid = randomUUID();
let quantity__standard__phosphorus_percentage_uuid = randomUUID();
let quantity__standard__potassium_percentage_uuid = randomUUID();
let organic_matter_convention_uuid = randomUUID();
let quantity__standard__area_percentage_uuid = randomUUID();
let taxonomy_term__unit__percentage_uuid = randomUUID();
let taxonomy_term__material_type__organic_matter_uuid = randomUUID();
let taxonomy_term__unit__organic_matter_uuid = randomUUID();

//examples
let log__input_organic_matter_example = {
    id:log__input_organic_matter_uuid,
    attributes: {
        name: "example organic matter log",
        status:"done",
    }
};
let log__input_organic_matter_error = {
    id:log__input_organic_matter_uuid,
    attributes: {
        name: "example organic matter log",
        status:"false",
    }
};

//Main entity
let log__input_organic_matter = new builder.SchemaOverlay({
    typeAndBundle: 'log--input',
    name: 'organic_matter',
    validExamples: [log__input_organic_matter_example],
    erroredExamples: [log__input_organic_matter_error]    
});
log__input_organic_matter.setMainDescription("This log includes any organic matter inputs including mulch, compost, manure, etc.");

log__input_organic_matter.setConstant({
    attribute:"status",
    value:"done"
});

//let logCategoryExample = {
//    id: logCategoryUUID,
//    attributes: {
//        name: "amendment"
//    }
//};
//let logCategoryError = {
//    id: logCategoryUUID,
//    attributes: {
//        name: "blah"
//    }
//};
//let logCategory = new builder.SchemaOverlay({
//    typeAndBundle: "taxonomy_term--log_category",
//    name: "amendment",
//    validExamples: [logCategoryExample],
//    erroredExamples: [logCategoryError],
//});
//logCategory.setConstant({
//    attribute:"name",
//    value:"amendment"
//});
//logCategory.setMainDescription("Log category is set to amendment.")

//quanities, will pull in % units
//moisture - moved to global
//let moisturePercentageExample = {
//    id: moisturePercentageUUID,
//    attributes: {
//        label: "moisture",
//        measure: "water_content"
//    }
//};
//let moisturePercentage = new builder.SchemaOverlay({
//    typeAndBundle: "quantity--standard",
//    name: "moisture_percentage",
//    validExamples: [moisturePercentageExample]
    //erroredExamples: [ ]
//});
//moisturePercentage.setMainDescription("The percent moisture present in the organic matter material.");
//moisturePercentage.setConstant({
//    attribute: "measure",
//    //LOOK check that this is how value should be entered
//    value:"water_content"
//});
//moisturePercentage.setConstant({
//    attribute: "label",
//    value:"moisture"
//});

//n - moved to global
//let nitrogenPercentageExample = {
//    id: nitrogenPercentageUUID,
//    attributes: {
//        label: "n",
//        measure: "ratio"
//    }
//};
//let nitrogenPercentage = new builder.SchemaOverlay({
//    typeAndBundle: "quantity--standard",
//    name: "nitrogen_percentage",
//    validExamples: [nitrogenPercentageExample]
//    //erroredExamples: [areaPercentageError]
//});
//nitrogenPercentage.setMainDescription("The percent of nitrogen present in the organic matter material.");
//nitrogenPercentage.setConstant({
//    attribute: "measure",
//    //LOOK check that this is how value should be entered
//    value:"ratio"
//});
//nitrogenPercentage.setConstant({
//    attribute: "label",
//    value:"n"
//});

//p - moved to global
//let phosphorusPercentageExample = {
//    id: phosphorusPercentageUUID,
//    attributes: {
//        label: "p",
//        measure: "ratio"
//    }
//};
//let phosphorusPercentage = new builder.SchemaOverlay({
//    typeAndBundle: "quantity--standard",
//    name: "phosphorus_percentage",
//    validExamples: [phosphorusPercentageExample]
//    //erroredExamples: [areaPercentageError]
//});
//phosphorusPercentage.setMainDescription("The percent of phosphorus present in the organic matter material.");
//phosphorusPercentage.setConstant({
//    attribute: "measure",
//    //LOOK check that this is how value should be entered
//    value:"ratio"
//});
//phosphorusPercentage.setConstant({
//    attribute: "label",
//    value:"p"
//});

//k - moved to global
//let potassiumPercentageExample = {
//    id: potassiumPercentageUUID,
//    attributes: {
//        label: "k",
//        measure: "ratio"
//    }
//};
//let potassiumPercentage = new builder.SchemaOverlay({
//    typeAndBundle: "quantity--standard",
//    name: "potassium_percentage",
//    validExamples: [potassiumPercentageExample]
//    //erroredExamples: [areaPercentageError]
//});
//potassiumPercentage.setMainDescription("The percent of potassium present in the organic matter material.");
//potassiumPercentage.setConstant({
//    attribute: "measure",
//    //LOOK check that this is how value should be entered
//    value:"ratio"
//});
//potassiumPercentage.setConstant({
//    attribute: "label",
//    value:"k"
//});

//organic matter material type name
let taxonomy_term__material_type__organic_matter = new builder.SchemaOverlay({
    typeAndBundle: "taxonomy_term--material_type",
    name: "organic_matter",
    validExamples: [],
    erroredExamples: []
});
taxonomy_term__material_type__organic_matter.setMainDescription("The type of organic matter is stored here.");

taxonomy_term__material_type__organic_matter.setEnum({
    attribute: "name",
    valuesArray: [
        "biosolids",
        "farm_yard_waste_compost",
        "farm_waste_compost",
        "hay_mulch",
        "leaf_mulch",
        "manure_food_waste_compost",
        "manure_yard_waste_compost",
        "dairy_manure_compost",
        "dairy_manure_liquid",
        "manure_dairy",
        "manure_goat",
        "manure_hog_liquid",
        "manure_hog_solid",
        "manure_horse",
        "manure_horse_compost",
        "manure_mixed_compost",
        "manure_mixed_ruminant_compost",
        "manure_other",
        "manure_poultry",
        "manure_poultry_compost",
        "manure_sheep",
        "mushroom_compost",
        "straw_mulch",
        "wood_chips",
        "yard_waste_compost"
    ],
    description: "List of types of organic matter material."
});

//Fertilizer quantity units
////Examples
let taxonomy_term__unit__organic_matter_example = {
    id:taxonomy_term__unit__organic_matter_uuid,
    attributes: {
        name: "lbs",
    }
};
let taxonomy_term__unit__organic_matter_error = {
    id:taxonomy_term__unit__organic_matter_uuid,
    attributes: {
        name: "ft",
    }
};

let taxonomy_term__unit__organic_matter = new builder.SchemaOverlay({
    typeAndBundle: "taxonomy_term--unit",
    name: "organic_matter",
    validExamples: [ taxonomy_term__unit__organic_matter_example ],
    erroredExamples: [ taxonomy_term__unit__organic_matter_error ]
});
taxonomy_term__unit__organic_matter.setEnum({
    attribute: "name",
    valuesArray: [
        "lbs",
        "in"
    ],
});
taxonomy_term__unit__organic_matter.setMainDescription("Organic matter is quantified in pounds or inches of material added.");

//Convention
let organicMatterConvention = new builder.ConventionSchema({
    title: "Organic Matter",
    version: "0.0.1",
    schemaName:"log--input--organic_matter",
    repoURL:"www.gitlabrepo.com/version/farmos_conventions",
    description:`## Purpose\n
This log records any organic matter inputs to a field or planting such as mulch, compost, manure, etc.\n
## Specification\n
text\n`,
    validExamples: [organicMatterConventionExample],
    erroredExamples: [organicMatterConventionError]
});

//add local attributes
organicMatterConvention.addAttribute( { schemaOverlayObject:log__input_organic_matter, attributeName: "log--input--organic_matter", required: true } );
organicMatterConvention.addAttribute( { schemaOverlayObject:taxonomy_term__material_type__organic_matter, attributeName: "taxonomy_term--material_type--organic_matter", required: false});
organicMatterConvention.addAttribute( { schemaOverlayObject: taxonomy_term__unit__organic_matter, attributeName: "taxonomy_term--unit--organic_matter", required: false});

//add global attributes
organicMatterConvention.addAttribute( { schemaOverlayObject:"taxonomy_term--log_category--amendment", attributeName: "taxonomy_term--log_category--amendment", required: true } );
organicMatterConvention.addAttribute( { schemaOverlayObject:"asset--plant--planting", attributeName: "asset--plant--planting", required: true } );
organicMatterConvention.addAttribute({ schemaOverlayObject:"quantity--standard--area_percentage", attributeName: "quantity--standard--area_percentage", required: false});
organicMatterConvention.addAttribute({ schemaOverlayObject:"taxonomy_term--unit--%", attributeName: "taxonomy_term--unit--%", required: false});
organicMatterConvention.addAttribute( { schemaOverlayObject:"quantity--standard--moisture_percentage", attributeName: "quantity--standard--moisture_percentage", required: false } );
organicMatterConvention.addAttribute( { schemaOverlayObject:"quantity--standard--nitrogen_percentage", attributeName: "quantity--standard--nitrogen_percentage", required: false } );
organicMatterConvention.addAttribute( { schemaOverlayObject:"quantity--standard--phosphorus_percentage", attributeName: "quantity--standard--phosphorus_percentage", required: false } );
organicMatterConvention.addAttribute( { schemaOverlayObject:"quantity--standard--potassium_percentage", attributeName: "quantity--standard--potassium_percentage", required: false } );
organicMatterConvention.addAttribute( { schemaOverlayObject:"quantity--material--rate", attributeName: "quantity--material--rate", required: false } );

//add relationships
organicMatterConvention.addRelationship( { containerEntity:"log--input--organic_matter" , relationName:"category" , mentionedEntity:"taxonomy_term--log_category--amendment" , required: true } );
organicMatterConvention.addRelationship( { containerEntity:"log--input--organic_matter", relationName:"asset", mentionedEntity:"asset--plant--planting", required: true } );
organicMatterConvention.addRelationship( { containerEntity:"log--input--organic_matter" , relationName:"quantity" , mentionedEntity:"quantity--standard--area_percentage" , required: false } );
organicMatterConvention.addRelationship( { containerEntity:"quantity--standard--area_percentage" , relationName:"units" , mentionedEntity:"taxonomy_term--unit--%" , required: false } );
organicMatterConvention.addRelationship( { containerEntity:"log--input--organic_matter" , relationName:"quantity" , mentionedEntity:"quantity--standard--moisture_percentage" , required: false } );
organicMatterConvention.addRelationship( { containerEntity:"quantity--standard--moisture_percentage" , relationName:"units" , mentionedEntity:"taxonomy_term--unit--%" , required: false } );
organicMatterConvention.addRelationship( { containerEntity:"log--input--organic_matter" , relationName:"quantity" , mentionedEntity:"quantity--standard--nitrogen_percentage" , required: false } );
organicMatterConvention.addRelationship( { containerEntity:"quantity--standard--nitrogen_percentage" , relationName:"units" , mentionedEntity:"taxonomy_term--unit--%" , required: false } );
organicMatterConvention.addRelationship( { containerEntity:"log--input--organic_matter" , relationName:"quantity" , mentionedEntity:"quantity--standard--phosphorus_percentage" , required: false } );
organicMatterConvention.addRelationship( { containerEntity:"quantity--standard--phosphorus_percentage" , relationName:"units" , mentionedEntity:"taxonomy_term--unit--%" , required: false } );
organicMatterConvention.addRelationship( { containerEntity:"log--input--organic_matter" , relationName:"quantity" , mentionedEntity:"quantity--standard--potassium_percentage" , required: false } );
organicMatterConvention.addRelationship( { containerEntity:"quantity--standard--potassium_percentage" , relationName:"units" , mentionedEntity:"taxonomy_term--unit--%" , required: false } );
organicMatterConvention.addRelationship( { containerEntity:"log--input--organic_matter" , relationName:"quantity" , mentionedEntity:"quantity--material--rate" , required: false } );
organicMatterConvention.addRelationship( { containerEntity:"quantity--material--rate", relationName:"material_type" , mentionedEntity:"taxonomy_term--material_type--organic_matter" , required: true } );
organicMatterConvention.addRelationship( { containerEntity:"quantity--material--rate", relationName:"units" , mentionedEntity:"taxonomy_term--unit--organic_matter" , required: true } );

//examples
let asset__plant__planting_attributes = organicMatterConvention.overlays['asset--plant--planting'].validExamples?.[0]?.attributes;
let asset__plant__planting_uuid = organicMatterConvention.overlays['asset--plant--planting'].validExamples?.[0]?.id;

//LOOK - need to review how to get attributes because valid examples array is [null]
let quantity__material__rate_attributes = organicMatterConvention.overlays['quantity--material--rate'].validExamples?.[0]?.attributes;
let quantity__material__rate_uuid = organicMatterConvention.overlays["quantity--material--rate"].validExamples?.[0]?.id;

var organicMatterConventionExample = {
    id: organic_matter_convention_uuid,
    type: "Object",
    "asset--plant--planting": {
        id: asset__plant__planting_uuid,
        attributes: asset__plant__planting_attributes
    },
    "log--input--organic_matter": {
        id: log__input_organic_matter_uuid,
        attributes: {
            name: "example organic matter log",
            status: "done",
            timestamp: ( new Date() ).toISOString(),
        },
        relationships: {
            asset: { data: [
                {
                    type: "asset--plant",
                    id: asset__plant__planting_uuid
                }
            ] },
            category: { data: [
                {
                    type: "taxonomy_term--log_category",
                    id:taxonomy_term__log_category__amendment_uuid
                }
            ] },
            quantity: { data: [
                {
                    type: "quantity--standard",
                    id: quantity__standard__area_percentage_uuid
                },
                {
                    type: "quantity--standard",
                    id: quantity__standard__moisture_percentage_uuid
                },
                {
                    type: "quantity--standard",
                    id: quantity__standard__nitrogen_percentage_uuid
                },
                {
                    type: "quantity--standard",
                    id: quantity__standard__phosphorus_percentage_uuid
                },
                {
                    type: "quantity--standard",
                    id: quantity__standard__potassium_percentage_uuid
                },
                {
                    type: "quantity--material",
                    id: quantity__material__rate_uuid
                }
            ] }
        },
    },
    "quantity--material--rate": {
        id: quantity__material__rate_uuid,
        attributes: quantity__material__rate_attributes,
        relationships: {
            material_type: { data: [
                {
                    type:"taxonomy_term--material_type",
                    id: taxonomy_term__material_type__organic_matter_uuid
                }
            ] },
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: taxonomy_term__unit__organic_matter_uuid
                }
            ] }
        }
    },
    "taxonomy_term--material_type--organic_matter":  {
        id: taxonomy_term__material_type__organic_matter_uuid,
        attributes: {
            name: "manure_dairy"
        },   
    },
    "taxonomy_term--unit--organic_matter":  {
        id: taxonomy_term__unit__organic_matter_uuid,
        attributes: {
            name: "lbs"
        },   
    },
    "taxonomy_term--log_category--amendment": {
            id: taxonomy_term__log_category__amendment_uuid,
            attributes: {
                name: "amendment"
            }
    },
    "quantity--standard--area_percentage": {
        id: quantity__standard__area_percentage_uuid,
        attributes: {
            label: "area"
        },
        relationships: {
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: taxonomy_term__unit__percentage_uuid
                }
            ] }
        }
    },
    "taxonomy_term--unit--%":  {
        id: taxonomy_term__unit__percentage_uuid,
        attributes: {
            name: "%"
        },   
    },
    "quantity--standard--moisture_percentage": {
            id: quantity__standard__moisture_percentage_uuid,
            attributes: {
                label: "moisture",
                measure: "water_content"
            },
            relationships: {
                units: { data: [
                    {
                        type:"taxonomy_term--unit",
                        id: taxonomy_term__unit__percentage_uuid
                    }
                ] }
            }
    },
    "quantity--standard--nitrogen_percentage": {
        id: quantity__standard__nitrogen_percentage_uuid,
        attributes: {
            label: "n",
            measure: "ratio"
        },
        relationships: {
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: taxonomy_term__unit__percentage_uuid
                }
            ] }
        }
    },
    "quantity--standard--phosphorus_percentage": {
        id: quantity__standard__phosphorus_percentage_uuid,
        attributes: {
            label: "p",
            measure: "ratio"
        },
        relationships: {
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: taxonomy_term__unit__percentage_uuid
                }
            ] }
        }
    },
    "quantity--standard--potassium_percentage": {
        id: quantity__standard__potassium_percentage_uuid,
        attributes: {
            label: "k",
            measure: "ratio"
        },
        relationships: {
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: taxonomy_term__unit__percentage_uuid
                }
            ] }
        }
    }
};

var organicMatterConventionError = {
    id: organic_matter_convention_uuid,
    type: "Object",
    "asset--plant--planting": {
        id: asset__plant__planting_uuid,
        attributes: asset__plant__planting_attributes
    },
    "log--input--organic_matter": {
        id: log__input_organic_matter_uuid,
        attributes: {
            name: "example organic matter log",
            status: "done",
            timestamp: ( new Date() ).toISOString(),
        },
        relationships: {
            quantity: { data: [
                {
                    type: "quantity--standard",
                    id: quantity__standard__area_percentage_uuid
                },
                {
                    type: "quantity--standard",
                    id: quantity__standard__moisture_percentage_uuid
                },
                {
                    type: "quantity--standard",
                    id: quantity__standard__nitrogen_percentage_uuid
                },
                {
                    type: "quantity--standard",
                    id: quantity__standard__phosphorus_percentage_uuid
                },
                {
                    type: "quantity--standard",
                    id: quantity__standard__potassium_percentage_uuid
                }
            ] }
        },
    },
    "quantity--standard--area_percentage": {
        id: quantity__standard__area_percentage_uuid,
        attributes: {
            label: "area"
        },
        relationships: {
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: taxonomy_term__unit__percentage_uuid
                }
            ] }
        }
    }
};

organicMatterConvention.validExamples = [organicMatterConventionExample];
organicMatterConvention.erroredExamples = [organicMatterConventionError];

let test = organicMatterConvention.testExamples();
let storageOperation = organicMatterConvention.store();
