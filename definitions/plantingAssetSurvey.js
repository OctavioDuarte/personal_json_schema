// Survey: https://app.surveystack.io/surveys/6346c1e467f839000121503e/edit
// Section: 11, planting_div_veg
const builder = require(`../src/convention_builder`);
const { randomUUID } = require('crypto');
const fs = require('fs');

// ids for the examples
let asset__plant__planting_uuid = randomUUID();
let taxonomy_term__season__season_uuid = randomUUID();
let taxonomy_term__plant_type__species_uuid = randomUUID();
let taxonomy_term__plant_type__variety_uuid = randomUUID();
let planting_convention_uuid = randomUUID();

// // Main entity: Plant Asset - moved to global
//let plantingAssetExample = {
//    id:asset__plant__planting_uuid,
//    attributes: {
//        name: "example div veg plant asset",
//        geometry: null,
//        status:"active"
//    }
//};
//let plantingAssetError = {
//    id:asset__plant__planting_uuid,
//    attributes: {
//        name: "example div veg plant asset",
//        geometry: "[91.93934434,-40.345345]",
//        status:"true"
//    }
//};
//let plantingPlantAsset = new builder.SchemaOverlay({
//    typeAndBundle: 'asset--plant',
//    name: 'planting',
//    validExamples: [plantingAssetExample],
//    erroredExamples: [plantingAssetError]    
//});

//plantingPlantAsset.setMainDescription("The planting is the main asset that all management logs will reference.");
//plantingPlantAsset.setConstant({
//    attribute:"status",
//    value:"active"
//});
//plantingPlantAsset.setConstant({
//    attribute:"geometry",
//    value:null
//});


// let Taxonomy Terms
let taxonomy_term__plant_type__variety_example = {
    id: taxonomy_term__plant_type__variety_uuid,
    attributes: {
        name: "laccinato"
    }
};
let taxonomy_term__plant_type__variety_error = {
    id: taxonomy_term__plant_type__variety_uuid,
    attributes: {
        label: "laccinato"
    }
};
let taxonomy_term__plant_type__variety = new builder.SchemaOverlay({
    typeAndBundle: "taxonomy_term--plant_type",
    name: 'variety',
    validExamples: [taxonomy_term__plant_type__variety_example],
    erroredExamples: [taxonomy_term__plant_type__variety_error]
});
taxonomy_term__plant_type__variety.setMainDescription("The variety of the vegetable planted.");

let taxonomy_term__plant_type__species_example = {
    id: taxonomy_term__plant_type__species_uuid,
    attributes: {
        name: "kale"
    }
};
let taxonomy_term__plant_type__species_error = {
    id: taxonomy_term__plant_type__species_uuid,
    attributes: {
        label: "kale"
    }
};
let taxonomy_term__plant_type__species = new builder.SchemaOverlay({
    typeAndBundle: "taxonomy_term--plant_type",
    name: "species",
    validExamples: [taxonomy_term__plant_type__species_example],
    erroredExamples: [taxonomy_term__plant_type__species_error],
});
taxonomy_term__plant_type__species.setMainDescription("The species of the vegetable planted.");

let taxonomy_term__season__season_example = {
    id: taxonomy_term__season__season_uuid,
    attributes: {
        name: "spring, 2022"
    }
};
let taxonomy_term__season__season_error = {
    id: taxonomy_term__season__season_uuid,
    attributes: {
        label: "spring, 2022"
    }
};
let taxonomy_term__season__season = new builder.SchemaOverlay({
    typeAndBundle: "taxonomy_term--season",
    name: "season",
    validExamples: [taxonomy_term__season__season_example],
    erroredExamples: [taxonomy_term__season__season_error]
});
taxonomy_term__season__season.setMainDescription("A season taxonomy term should exist for each working period, and logs and assets should be associated to the related seasons. It is up to the user to decide how they want to categorize their seasons - ex: 2016, or Fall, 2022.");


//// use a taxonomy_term--season in the plant asset

// // // Convention
// object
let plantingConvention = new builder.ConventionSchema({
    title: "Planting",
    version: "0.0.1",
    schemaName:"asset--plant--div_veg_planting",
    repoURL:"www.gitlabrepo.com/version/farmos_conventions",
    description:`## Purpose\n
This is the main entity that represents a cultivar which will be referenced by all subsequent managment logs.\n
## Specification\n
text\n`,
    validExamples: [],
    erroredExamples: []
});

//add local attributes
plantingConvention.addAttribute( { schemaOverlayObject:taxonomy_term__plant_type__species, attributeName: "taxonomy_term--plant_type--species", required: true } );
plantingConvention.addAttribute( { schemaOverlayObject:taxonomy_term__plant_type__variety, attributeName: "taxonomy_term--plant_type--variety", required: false } );
plantingConvention.addAttribute( { schemaOverlayObject:taxonomy_term__season__season, attributeName: "taxonomy_term--season--season", required: false } );

//add global attributes
plantingConvention.addAttribute( { schemaOverlayObject:"asset--plant--planting", attributeName: "asset--plant--planting", required: true } );
//plantingConvention.addAttribute( { schemaOverlayObject:season, attributeName: "season_taxonomy", required: true } );

plantingConvention.addRelationship( { containerEntity:"asset--plant--planting" , relationName:"plant_type" , mentionedEntity:"taxonomy_term--plant_type--species" , required: true } );
plantingConvention.addRelationship( { containerEntity:"asset--plant--planting" , relationName:"plant_type" , mentionedEntity:"taxonomy_term--plant_type--variety" , required: false } );
plantingConvention.addRelationship( { containerEntity:"asset--plant--planting" , relationName:"season" , mentionedEntity:"taxonomy_term--season--season" , required: true } );

let plantingConventionExample = {
    id: planting_convention_uuid,
    "asset--plant--planting": {
        id:asset__plant__planting_uuid,
        attributes: {
            name: "Example div veg plant asset",
            geometry: null,
            status:"active"
        },
        relationships: {
            plant_type: { data: [
                {
                    type: "taxonomy_term--plant_type",
                    id:taxonomy_term__plant_type__species_uuid
                },
                {
                    type: "taxonomy_term--plant_type",
                    id:taxonomy_term__plant_type__variety_uuid
                }
            ] },
            season: { data: [
                {
                    type: "taxonomy_term--season",
                    id: taxonomy_term__season__season_uuid
                }
            ] }
        }
    },
    "taxonomy_term--season--season": taxonomy_term__season__season_example,
    "taxonomy_term--plant_type--species": taxonomy_term__plant_type__species_example,
    "taxonomy_term--plant_type--variety": taxonomy_term__plant_type__variety_example
};

let plantingConventionError = {
    id: planting_convention_uuid,
    "asset--plant--planting": {
        id:asset__plant__planting_uuid,
        attributes: {
            name: "Example div veg plant asset",
            geometry: null,
            status:"active"
        },
        relationships: {
            plant_type: { data: [
                {
                    type: "taxonomy_term--plant_type",
                    id:taxonomy_term__plant_type__variety_uuid
                }
            ] }
        }
    },
    "taxonomy_term--season--season": taxonomy_term__season__season_example,
    "taxonomy_term--plant_type--species": taxonomy_term__plant_type__species_example,
    "taxonomy_term--plant_type--variety": taxonomy_term__plant_type__variety_example
};

plantingConvention.validExamples = [plantingConventionExample];
plantingConvention.erroredExamples = [plantingConventionError];

let test = plantingConvention.testExamples();
let storageOperation = plantingConvention.store();
