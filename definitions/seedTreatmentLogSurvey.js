// Survey: https://app.surveystack.io/surveys/6346c1e467f839000121503e/edit
// Section: 11, planting_div_veg
const builder = require(`../src/convention_builder`);
const { randomUUID } = require('crypto');
const fs = require('fs');

//UUIDs
let log__input__seed_treatment_uuid = randomUUID();
let seed_treatment_convention_uuid = randomUUID();
let taxonomy_term__unit__percentage_uuid = randomUUID();
let quantity__standard__area_percentage_uuid = randomUUID();

//Examples
let log__input__seed_treatment_example = {
    id: log__input__seed_treatment_uuid,
    attributes: {
        status: "done"
    }
};

let log__input__seed_treatment_error = {
    id: log__input__seed_treatment_uuid,
    attributes: {
        status: "in process"
    }
};

//Main entity
let log__input__seed_treatment = new builder.SchemaOverlay({
    typeAndBundle: 'log--input',
    name: 'seed_treatment',
    validExamples: [log__input__seed_treatment_example],
    erroredExamples: [log__input__seed_treatment_error]
});
log__input__seed_treatment.setMainDescription("This adds a seed treatment as an input log if one is noted in planting information.");

log__input__seed_treatment.setConstant({
    attribute:"status",
    value:"done",
    description: "The status should always be set to done to inherit the area."
});

//quantity - material (LOOK I don't see a quantity in the survey?)

//Convention
// Object
let seedTreatmentConvention = new builder.ConventionSchema({
    title: "Seed Treatment",
    version: "0.0.1",
    schemaName:"log--input--seed_treatment",
    repoURL:"www.gitlabrepo.com/version/farmos_conventions",
    description:`## Purpose\n
Seed treatment log to be applied to a plant asset only if the farmer selected one in their planting records.\n
## Specification\n
text\n`,
    // if you want to define this later, don't add it when first creating the object
    validExamples: [],
    erroredExamples: []
});

//add local attributes
seedTreatmentConvention.addAttribute( { schemaOverlayObject:log__input__seed_treatment, attributeName: "log--input--seed_treatment", required: true } );

//add global attributes
seedTreatmentConvention.addAttribute({ schemaOverlayObject:"taxonomy_term--log_category--seeding", attributeName: "taxonomy_term--log_category--seeding", required: true});
seedTreatmentConvention.addAttribute( { schemaOverlayObject:"asset--plant--planting", attributeName: "asset--plant--planting", required: true } );
seedTreatmentConvention.addAttribute( { schemaOverlayObject:"quantity--standard--area_percentage", attributeName: "quantity--standard--area_percentage", required: true } );
seedTreatmentConvention.addAttribute( { schemaOverlayObject:"taxonomy_term--unit--%", attributeName: "taxonomy_term--unit--%", required: true } );

////add relationships
seedTreatmentConvention.addRelationship({ containerEntity: "log--input--seed_treatment", relationName:"category", mentionedEntity:"taxonomy_term--log_category--seeding", required: true});
seedTreatmentConvention.addRelationship( { containerEntity:"log--input--seed_treatment", relationName:"asset", mentionedEntity:"asset--plant--planting", required: true } );
seedTreatmentConvention.addRelationship( { containerEntity:"log--input--seed_treatment" , relationName:"quantity" , mentionedEntity:"quantity--standard--area_percentage" , required: false } );
seedTreatmentConvention.addRelationship( { containerEntity:"quantity--standard--area_percentage" , relationName:"units" , mentionedEntity:"taxonomy_term--unit--%" , required: false } );

let asset__plant__planting_attributes = seedTreatmentConvention.overlays['asset--plant--planting'].validExamples[0].attributes;
let asset__plant__planting_uuid = seedTreatmentConvention.overlays['asset--plant--planting'].validExamples[0].id;

let taxonomy_term__log_category__seeding_attributes = seedTreatmentConvention.overlays['taxonomy_term--log_category--seeding'].validExamples[0];
let taxonomy_term__log_category__seeding_uuid = seedTreatmentConvention.overlays['taxonomy_term--log_category--seeding'].validExamples[0].id;

var seedTreatmentConventionExample = {
    id: seed_treatment_convention_uuid,
    type: "Object",
    "asset--plant--planting": {
        id: asset__plant__planting_uuid,
        attributes: asset__plant__planting_attributes
    },
    "log--input--seed_treatment": {
        id: log__input__seed_treatment_uuid,
        attributes: {
            name: "example seed treatment log",
            status: "done",
            timestamp: ( new Date() ).toISOString(),
        },
        relationships: {
            asset: { data: [
                {
                    type: "asset--plant",
                    id: asset__plant__planting_uuid
                }
            ]},
            category: { data: [
                {
                    type: "taxonomy_term--log_category",
                    id:taxonomy_term__log_category__seeding_uuid
                }
            ] }
        },
    },
    "taxonomy_term--log_category--seeding": {
        id: taxonomy_term__log_category__seeding_uuid,
        attributes: {
            name: "seeding"
        }
    },
    "quantity--standard--area_percentage": {
        id: quantity__standard__area_percentage_uuid,
        attributes: {
            label: "area"
        },
        relationships: {
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: taxonomy_term__unit__percentage_uuid
                }
            ] }
        }
    },
    "taxonomy_term--unit--%":  {
        id: taxonomy_term__unit__percentage_uuid,
        attributes: {
            name: "%"
        },   
    },
};

var seedTreatmentConventionError = {
    id: seed_treatment_convention_uuid,
    type: "Object",
    "asset--plant--planting": {
        id: asset__plant__planting_uuid,
        attributes: asset__plant__planting_attributes
    },
    "log--input--seed_treatment": {
        id: log__input__seed_treatment_uuid,
        attributes: {
            name: "example seed treatment log",
            status: "false",
            timestamp: ( new Date() ).toISOString()
        },
        relationships: {
            asset: { data: [
                {
                    type: "asset--plant",
                    id: asset__plant__planting_uuid
                }
            ]},
            category: { data: [
                {
                    type: "taxonomy_term--log_category",
                    id:taxonomy_term__log_category__seeding_uuid
                }
            ] }
        },
    },
    "taxonomy_term--log_category--seeding": {
        id: taxonomy_term__log_category__seeding_uuid,
        attributes: {
            name: "blah"
        }
    },
    area_unit:  {
        id: taxonomy_term__unit__percentage_uuid,
        attributes: {
            name: "%"
        },   
    },
};

seedTreatmentConvention.validExamples = [seedTreatmentConventionExample];
seedTreatmentConvention.erroredExamples = [seedTreatmentConventionError];

let test = seedTreatmentConvention.testExamples();
let storageOperation = seedTreatmentConvention.store();
