// Survey: https://app.surveystack.io/surveys/6346c1e467f839000121503e/edit
// Section: 11, planting_div_veg
const builder = require(`../src/convention_builder`);
const { randomUUID } = require('crypto');
const fs = require('fs');

// ids for the examples
let log__seeding__seeding_uuid = randomUUID();
let taxonomy_term__log_category__seeding_uuid = randomUUID();
let quantity__standard__area_percentage_uuid = randomUUID();
let quantity__standard__seeding_rate_uuid = randomUUID();
let quantity__standard__bed_width_uuid = randomUUID();
let convention_uuid = randomUUID();
let taxonomy_term__unit__percentage_uuid = randomUUID();
let taxonomy_term__unit__seeding_rate_uuid = randomUUID();
let taxonomy_term__unit__bed_width_uuid = randomUUID();


// // Main entity: Seeding Log
let log__seeding__seeding_example = {
    id:log__seeding__seeding_uuid,
    attributes: {
        name: "example seeding log",
        status:"done",
        is_movement:"false"
    }
};
let log__seeding__seeding_error = {
    id:log__seeding__seeding_uuid,
    attributes: {
        name: "example seeding log",
        status:"true",
        is_movement:"true"
    }
};
let log__seeding__seeding = new builder.SchemaOverlay({
    typeAndBundle: 'log--seeding',
    name: 'seeding',
    validExamples: [log__seeding__seeding_example],
    erroredExamples: [log__seeding__seeding_error]    
});

log__seeding__seeding.setMainDescription("A seeding or transplanting log sets the location of the plant asset.");

// required attribute is status
log__seeding__seeding.setConstant({
    attribute:"status",
    value:"done"
});

// required to set location of asset
log__seeding__seeding.setConstant({
    attribute:"is_movement",
    value:"false"
});

//taxonomy term - log_category
//let logCategory = new builder.SchemaOverlay({
//    typeAndBundle: "taxonomy_term--log_category",
//    name: "seeding",
//    validExamples: [logCategoryExample],
//    erroredExamples: [logCategoryError],
//});
//logCategory.setConstant({
//    attribute:"name",
//    value:"seeding"
//});
//logCategory.setMainDescription("The log category is set to seeding.");

//// Quantities
//area overlay - moved to global
//let quantity__standard__area_percentage_example = {
//    id: quantity__standard__area_percentage_uuid,
//    attributes: {
//        label: "area",
//        measure: "area"
//    }
//};
//let quantity__standard__area_percentage = new builder.SchemaOverlay({
//    typeAndBundle: "quantity--standard",
//    name: "area_percentage",
//    validExamples: [quantity__standard__area_percentage_example]
//    //erroredExamples: [areaPercentageError]
//});
//quantity__standard__area_percentage.setMainDescription("Area is the percentage of the field that the seeding applies to.");
//quantity__standard__area_percentage.setConstant({
//    attribute: "measure",
//    value:"area"
//});
//quantity__standard__area_percentage.setConstant({
//    attribute: "label",
//    value:"area"
//});

// area unit example
//let unitExample = {
//    id: taxonomy_term__unit__percentage_uuid,
//    name: "%"
//};
//taxonomy term - units - moved to global
//let unitPercentage = new builder.SchemaOverlay({
//    typeAndBundle: "taxonomy_term--unit",
//    name: "%"
//});
//unitPercentage.setMainDescription("Units for area are: % of acres.");
//unitPercentage.setConstant({
//    attribute: "name",
//    value: "%"
//});

// seeding rate quantity
let quantity__standard__seeding_rate_example = {
    id: quantity__standard__seeding_rate_uuid,
    attributes: {
        measure:"rate",
        label: "seeding rate"
    }
};
let quantity__standard__seeding_rate = new builder.SchemaOverlay({
    typeAndBundle: "quantity--standard",
    name: "seeding_rate",
    validExamples: [ quantity__standard__seeding_rate_example ]
});
quantity__standard__seeding_rate.setMainDescription("The seeding rate indicates the mass of seed used for every surface unit.");
quantity__standard__seeding_rate.setConstant({
    attribute: "measure",
    value:"rate"
});
quantity__standard__seeding_rate.setConstant({
    attribute: "label",
    value:"seeding rate"
});

// seeding rate unit
let taxonomy_term__unit__seeding_rate_example = {
    id: taxonomy_term__unit__seeding_rate_uuid,
    attributes: {
        name: "kg/hec"
    }
};
let taxonomy_term__unit__seeding_rate_error = {
    id: taxonomy_term__unit__seeding_rate_uuid,
    attributes: {
        name: "kg/m^2"
    }
};
let taxonomy_term__unit__seeding_rate = new builder.SchemaOverlay({
    typeAndBundle: "taxonomy_term--unit",
    name: "seeding_rate",
    validExamples: [ taxonomy_term__unit__seeding_rate_example ],
    erroredExamples: [ taxonomy_term__unit__seeding_rate_error ]
});
taxonomy_term__unit__seeding_rate.setEnum({
    attribute: "name",
    valuesArray: [
        "lbs/ac",
        "seeds/acre",
        "kg/hec",
        "seeds/hec",
        "plants per sq ft",
        "plants per sq m"
    ],
    description: "Several units are available, both imperial and metric. They are all compatible mass to area ratios."
});
taxonomy_term__unit__seeding_rate.setMainDescription("Unit used to measure the rate quantity.");

// bed width
let quantity__standard__bed_width_example = {
    id: quantity__standard__bed_width_uuid,
    attributes: {
        measure:"length/depth",
        label: "bed width"
    }
};
let quantity__standard__bed_width = new builder.SchemaOverlay({
    typeAndBundle: "quantity--standard",
    name: "bed_width",
    validExamples: [ quantity__standard__bed_width_example ]
});
quantity__standard__bed_width.setMainDescription("The bed width indicates the size of permanent beds.");
quantity__standard__bed_width.setConstant({
    attribute: "measure",
    value:"length/depth"
});
quantity__standard__bed_width.setConstant({
    attribute: "label",
    value:"bed width"
});

// bed width unit
let taxonomy_term__unit__bed_width_example = {
    id: taxonomy_term__unit__bed_width_uuid,
    attributes: {
        name: "feet"
    }
};
let taxonomy_term__unit__bed_width_error = {
    id: taxonomy_term__unit__bed_width_uuid,
    attributes: {
        name: "kg"
    }
};
let taxonomy_term__unit__bed_width = new builder.SchemaOverlay({
    typeAndBundle: "taxonomy_term--unit",
    name: "bed_width",
    validExamples: [ taxonomy_term__unit__bed_width_example ],
    erroredExamples: [ taxonomy_term__unit__bed_width_error ]
});
taxonomy_term__unit__bed_width.setEnum({
    attribute: "name",
    valuesArray: [
        "feet",
        "meters"
    ],
    description: "Several units are available, both imperial and metric."
});
taxonomy_term__unit__bed_width.setMainDescription("Unit used to measure the bed width.");


// Convention
let seedingConvention = new builder.ConventionSchema({
    title: "Seeding",
    version: "0.0.1",
    schemaName:"log--seeding--seeding",
    repoURL:"www.gitlabrepo.com/version/farmos_conventions",
    description:`## Purpose\n
A seeding log is required for every plant asset.\n
## Specification\n
text\n`,
    validExamples: [],
    erroredExamples: []
});

//add local attributes
seedingConvention.addAttribute( { schemaOverlayObject:log__seeding__seeding, attributeName: "log--seeding--seeding", required: true } );
seedingConvention.addAttribute( {schemaOverlayObject:quantity__standard__seeding_rate, attributeName:"quantity--standard--seeding_rate", required: false} );
seedingConvention.addAttribute( {schemaOverlayObject:taxonomy_term__unit__seeding_rate, attributeName:"taxonomy_term--unit--seeding_rate", required: false} );
seedingConvention.addAttribute( {schemaOverlayObject:quantity__standard__bed_width, attributeName:"quantity--standard--bed_width", required: false} );
seedingConvention.addAttribute( {schemaOverlayObject:taxonomy_term__unit__bed_width, attributeName:"taxonomy_term--unit--bed_width", required: false} );

//add global attributes
seedingConvention.addAttribute( { schemaOverlayObject:"taxonomy_term--log_category--seeding", attributeName: "taxonomy_term--log_category--seeding", required: true } );
seedingConvention.addAttribute( { schemaOverlayObject:"quantity--standard--area_percentage", attributeName: "quantity--standard--area_percentage", required: true } );
seedingConvention.addAttribute( { schemaOverlayObject:"taxonomy_term--unit--%", attributeName: "taxonomy_term--unit--%", required: true } );
// NEW We can add a relationship to an existing overlay using it's name, like here. In this case, we checked in collection/overlays and saw a folder called `asset--plant--planting`. We can call that overlay using that exact name. This is exactly equivalent to using an overlay we've just written in this file. Below (line 342), I will add a relationship to it.
seedingConvention.addAttribute( { schemaOverlayObject:"asset--plant--planting", attributeName: "asset--plant--planting", required: false } );

//add relationships
seedingConvention.addRelationship( { containerEntity:"log--seeding--seeding" , relationName:"category" , mentionedEntity:"taxonomy_term--log_category--seeding" , required: true } );
seedingConvention.addRelationship( { containerEntity:"log--seeding--seeding" , relationName:"quantity" , mentionedEntity:"quantity--standard--area_percentage" , required: true } );
seedingConvention.addRelationship( { containerEntity:"quantity--standard--area_percentage" , relationName:"units" , mentionedEntity:"taxonomy_term--unit--%" , required: true } );
seedingConvention.addRelationship( { containerEntity:"quantity--standard--seeding_rate" , relationName:"units" , mentionedEntity:"taxonomy_term--unit--seeding_rate" , required: true } );
seedingConvention.addRelationship( { containerEntity:"quantity--standard--bed_width" , relationName:"units" , mentionedEntity:"taxonomy_term--unit--bed_width" , required: true } );
// Let's write the relationship between our main entity for this convention (the seeding log) and the asset--plant--planting overlay.
seedingConvention.addRelationship(
    {
        // The containerEntity is the main entity, the "log--seeding--seeding"
        containerEntity:"log--seeding--seeding" ,
        // Looking into the schema for the log--seeding and it's "relationship" fields, we can relate the land asset under the field "location" (LOOK shouldn't this be asset?)
        relationName:"asset",
        // The mentioned entity is added using it's attribute name, as when invokint addAttribute. We've effectively used "addAttribute", so it is the same as if we had just created the overlay.
        // We used `attributeName: "asset--plant--planting"`
        mentionedEntity:"asset--plant--planting",
        required: true
    } );

let asset__plant__planting_attributes = seedingConvention.overlays['asset--plant--planting'].validExamples?.[0]?.attributes;
let asset__plant__planting_uuid = seedingConvention.overlays['asset--plant--planting'].validExamples?.[0]?.id;
    

var seedingConventionExample = {
   id: convention_uuid,
   type: "Object",
    "asset--plant--planting": {
        id: asset__plant__planting_uuid,
        attributes: asset__plant__planting_attributes
    },
   "log--seeding--seeding": {
    id: log__seeding__seeding_uuid,
    attributes: {
        name: "example div veg seeding log",
        status: "done",
        is_movement: "false",
        timestamp: ( new Date() ).toISOString()
    },
    relationships: {
        asset: { data: [
            {
                type: "asset--plant",
                id: asset__plant__planting_uuid
            }
        ] },
        category: { data: [
            {
            type: "taxonomy_term--log_category",
            id:taxonomy_term__log_category__seeding_uuid
            }
        ] },
        quantity: { data: [
            {
                type: "quantity--standard",
                id: quantity__standard__area_percentage_uuid
            },
            {
                type: "quantity--standard",
                id: quantity__standard__seeding_rate_uuid
            },
            {
                type: "quantity--standard",
                id: quantity__standard__bed_width_uuid
            }
        ] }
    },
   },
   "taxonomy_term--log_category--seeding": {
        id:taxonomy_term__log_category__seeding_uuid,
        attributes: {
            name: "seeding"
        }
   },
    "quantity--standard--area_percentage": {
        id: quantity__standard__area_percentage_uuid,
        attributes: {
            label: "area"
        },
        relationships: {
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: taxonomy_term__unit__percentage_uuid
                }
            ] }
        }
    },
    "quantity--standard--seeding_rate": {
        id: quantity__standard__seeding_rate_uuid,
        attributes: {
            measure:"rate",
            label: "seeding rate"
        },
        relationships: {
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: taxonomy_term__unit__seeding_rate_uuid
                }
            ] }
        }
    },
    "quantity--standard--bed_width": {
        id: quantity__standard__bed_width_uuid,
        attributes: {
            measure:"length/depth",
            label: "bed width"
        },
        relationships: {
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: taxonomy_term__unit__bed_width_uuid
                }
            ] }
        }
    },
    "taxonomy_term--unit--bed_width": taxonomy_term__unit__bed_width_example,
    "taxonomy_term--unit--seeding_rate": taxonomy_term__unit__seeding_rate_example,
    "taxonomy_term--unit--%": {
        id: taxonomy_term__unit__percentage_uuid,
        attributes: {
            name: "%"
        },
    }
};

var seedingConventionError = {
    id: convention_uuid,
    type: "Object",
    "asset--plant--planting": {
        id: asset__plant__planting_uuid,
        attributes: asset__plant__planting_attributes
    }, 
    "log--seeding--seeding": {
     id: log__seeding__seeding_uuid,
     attributes: {
         name: "example div veg seeding log",
         status: "done",
         is_movement: false,
         timestamp: ( new Date() ).toISOString(),
     },
     relationships: {
         category: { data: [
             {
             type: "taxonomy_term--log_category",
             id:taxonomy_term__log_category__seeding_uuid
             }
         ] }
     },
    },
    "taxonomy_term--log_category--seeding": {
        id: taxonomy_term__log_category__seeding_uuid,
        attributes: {
            name: "activity"
        }
    },
    "quantity--standard--area_percentage": {
        id: quantity__standard__area_percentage_uuid,
        attributes: {
            label: "area"
        },
        relationships: {
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: taxonomy_term__unit__percentage_uuid
                }
            ] }
        }
    }
};

seedingConvention.validExamples = [seedingConventionExample];
seedingConvention.erroredExamples = [seedingConventionError];

let test = seedingConvention.testExamples();
let storageOperation = seedingConvention.store();
