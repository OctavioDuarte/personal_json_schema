// Survey: https://app.surveystack.io/surveys/6346c1e467f839000121503e/edit
// Section: 11, planting_div_veg
const builder = require(`../src/convention_builder`);
const { randomUUID } = require('crypto');
const fs = require('fs');

//UUIDs
let log__input__seedling_treatment_uuid = randomUUID();
let taxonomy_term__log_category__amendment_uuid = randomUUID();
let seedling_treatment_convention_uuid = randomUUID();
let quantity__standard__area_percentage_uuid = randomUUID();
let taxonomy_term__unit__percentage_uuid = randomUUID();

//Main entity
let log__input__seedling_treatment_example = {
    id: log__input__seedling_treatment_uuid,
    attributes: {
        status: "done"
    }
};
let log__input__seedling_treatment_error = {
    id: log__input__seedling_treatment_uuid,
    attributes: {
        status: "true"
    }
};
let log__input__seedling_treatment = new builder.SchemaOverlay({
    typeAndBundle: 'log--input',
    name: 'seedling_treatment',
    validExamples: [log__input__seedling_treatment_example],
    erroredExamples: [log__input__seedling_treatment_error]    
});
log__input__seedling_treatment.setMainDescription("This adds a seedling treatment that records the application of fungicide, insecticide, or a combination of both if one is indicated as used in the planting information.");

log__input__seedling_treatment.setConstant({
    attribute:"status",
    value:"done",
    description: "The status should always be set to done to inherit the area."
});

//taxonomy term - log_category
//let logCategoryExample = {
//    id: taxonomy_term__log_category__amendment_uuid,
//    attributes: {
//        name: "amendment"
//    }
//};
//let logCategoryError = {
//    id: taxonomy_term__log_category__amendment_uuid,
//    attributes: {
//        name: "irrigation"
//    }
//};
//let logCategory = new builder.SchemaOverlay({
//    typeAndBundle: "taxonomy_term--log_category",
//    name: ["amendment", "seeding"],
//    validExamples: [logCategoryExample],
//    erroredExamples: [logCategoryError],
//});
//logCategory.setEnum({
//    attribute: "name",
//    valuesArray: [
//        "amendment",
//        "seeding"
//    ],
//    description: "These are the activies accepted as representing seedling treatment purpose."
//});

//logCategory.setMainDescription("The log category is set to amendment and/or seeding.");

//quantity - material (LOOK I don't see a quantity in the survey?)

//Convention
let seedlingTreatmentConvention = new builder.ConventionSchema({
    title: "Seedling Treatment",
    version: "0.0.1",
    schemaName:"log--input--seedling_treatment",
    repoURL:"www.gitlabrepo.com/version/farmos_conventions",
    description:`## Purpose\n
A seedling treatment log is applied to a plant asset only if the farmer selected one.\n
## Specification\n
text\n`,
    validExamples: [seedlingTreatmentConventionExample],
    erroredExamples: [seedlingTreatmentConventionError]
});

////add local attributes
seedlingTreatmentConvention.addAttribute( { schemaOverlayObject:log__input__seedling_treatment, attributeName: "log--input--seedling_treatment", required: true } );

//add global attributes
seedlingTreatmentConvention.addAttribute( { schemaOverlayObject:"taxonomy_term--log_category--amendment", attributeName: "taxonomy_term--log_category--amendment", required: true } );
seedlingTreatmentConvention.addAttribute( { schemaOverlayObject:"asset--plant--planting", attributeName: "asset--plant--planting", required: true } );
seedlingTreatmentConvention.addAttribute( { schemaOverlayObject:"quantity--standard--area_percentage", attributeName: "quantity--standard--area_percentage", required: true } );
seedlingTreatmentConvention.addAttribute( { schemaOverlayObject:"taxonomy_term--unit--%", attributeName: "taxonomy_term--unit--%", required: true } );

////add relationships
seedlingTreatmentConvention.addRelationship({ containerEntity: "log--input--seedling_treatment", relationName:"category", mentionedEntity:"taxonomy_term--log_category--amendment", required: true});
seedlingTreatmentConvention.addRelationship( { containerEntity:"log--input--seedling_treatment", relationName:"asset", mentionedEntity:"asset--plant--planting", required: true } );
seedlingTreatmentConvention.addRelationship( { containerEntity:"log--input--seedling_treatment" , relationName:"quantity" , mentionedEntity:"quantity--standard--area_percentage" , required: true } );
seedlingTreatmentConvention.addRelationship( { containerEntity:"quantity--standard--area_percentage" , relationName:"units" , mentionedEntity:"taxonomy_term--unit--%" , required: true } );

let asset__plant__planting_attributes = seedlingTreatmentConvention.overlays["asset--plant--planting"].validExamples[0].attributes;
let asset__plant__planting_uuid = seedlingTreatmentConvention.overlays["asset--plant--planting"].validExamples[0].id;

var seedlingTreatmentConventionExample = {
    id: seedling_treatment_convention_uuid,
    type: "Object",
    "asset--plant--planting": {
        id: asset__plant__planting_uuid,
        attributes: asset__plant__planting_attributes
    },
    "log--input--seedling_treatment": {
        id: log__input__seedling_treatment_uuid,
        attributes: {
            name: "example seedling treatment log",
            status: "done",
            timestamp: ( new Date() ).toISOString(),
        },
        relationships: {
            quantity: {data: [
                {
                    type: "quantity--standard",
                    id:quantity__standard__area_percentage_uuid,
                }
            ]},
            asset: { data: [
                {
                    type: "asset--plant",
                    id: asset__plant__planting_uuid
                }
            ]},
            category: { data: [
                {
                    type: "taxonomy_term--log_category",
                    id:taxonomy_term__log_category__amendment_uuid
                }
            ] }
        },
    },
    "taxonomy_term--log_category--amendment": {
        id: taxonomy_term__log_category__amendment_uuid,
        attributes: {
            name: "amendment"
        }
    },
    "quantity--standard--area_percentage": {
       id: quantity__standard__area_percentage_uuid,
        attributes: {
            label: "area"
        },
        relationships: {
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: taxonomy_term__unit__percentage_uuid
                }
            ] }
        }
    },
    "taxonomy_term--unit--%":  {
        id: taxonomy_term__unit__percentage_uuid,
        attributes: {
            name: "%"
        },   
    },
};

var seedlingTreatmentConventionError = {
    id: seedling_treatment_convention_uuid,
    type: "Object",
    "asset--plant--planting": {
        id: asset__plant__planting_uuid,
        attributes: asset__plant__planting_attributes
    },
    "log--input--seedling_treatment": {
        id: log__input__seedling_treatment_uuid,
        attributes: {
            name: "example seedling treatment log",
            status: "pending",
        },
        relationships: {
            asset: { data: [
                {
                    type: "asset--plant",
                    id: asset__plant__planting_uuid
                }
            ]},
            category: { data: [
                {
                    type: "taxonomy_term--log_category",
                    id:taxonomy_term__log_category__amendment_uuid
                }
            ] }
        },
    },
    "taxonomy_term--log_category--amendment": {
        id: taxonomy_term__log_category__amendment_uuid,
        attributes: {
            name: "amendment"
        }
    },
//    area_quantity: {
//        id: areaPercentageUUID,
//        attributes: {
//            label: "area"
//        },
//        relationships: {
//            units: { data: [
//                {
//                    type:"taxonomy_term--unit",
//                    id: percentageUnitUUID
//                }
//            ] }
//        }
//    },
//    area_unit:  {
//        id: percentageUnitUUID,
//        attributes: {
//            name: "%"
//        },   
//    },
};

seedlingTreatmentConvention.validExamples = [seedlingTreatmentConventionExample];
seedlingTreatmentConvention.erroredExamples = [seedlingTreatmentConventionError];

let test = seedlingTreatmentConvention.testExamples();
let storageOperation = seedlingTreatmentConvention.store();
