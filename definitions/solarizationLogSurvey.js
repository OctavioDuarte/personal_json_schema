// Survey: https://app.surveystack.io/surveys/6346c1e467f839000121503e/edit
// Section: 30, solarization
const builder = require(`../src/convention_builder`);
const { randomUUID } = require('crypto');
const fs = require('fs');

// ids for the examples
let log__activity__solarization_uuid = randomUUID();
let solarization_convention_uuid = randomUUID();
let quantity__standard__area_percentage_uuid = randomUUID();
let taxonomy_term__log_category__weed_control_uuid = randomUUID();
let taxonomy_term__unit__percentage_uuid = randomUUID();

//Main entity
//examples
let log__activity__solarization_example = {
    id: log__activity__solarization_uuid,
    attributes: {
        status: "done",
        name: "end solarization"
    }
};
let log__activity__solarization_example2 = {
    id: log__activity__solarization_uuid,
    attributes: {
        status: "done",
        name: "start tarping"
    }
};
let log__activity__solarization_error = {
    id: log__activity__solarization_uuid,
    attributes: {
        status: "done",
        name: "solarization"
    }
};
let log__activity__solarization_error2 = {
    id: log__activity__solarization_uuid,
    attributes: {
        status: "processing",
        name: "start"
    }
};

let log__activity__solarization = new builder.SchemaOverlay({
    typeAndBundle: 'log--activity',
    name: 'solarization',
    validExamples: [log__activity__solarization_example, log__activity__solarization_example2],
    erroredExamples: [log__activity__solarization_error,log__activity__solarization_error2]    
});
log__activity__solarization.setMainDescription("This log is used to record solairzation or tarping events.");

log__activity__solarization.setConstant({
    attribute:"status",
    value:"done"
});

log__activity__solarization.setPattern({
    attribute:"name",
    pattern:"(?=.*(start|end))(?=.*(solarization|tarping))",
    description:"There are several synonymous words for solarization and the log needs to represent the start or end date of the activity."
});

//let test2 = log__activity__solarization.testExamples();

//Convention
let solarizationConvention = new builder.ConventionSchema({
    title: "Solarization",
    version: "0.0.1",
    schemaName:"log--activity--solarization",
    repoURL:"www.gitlabrepo.com/version/farmos_conventions",
    description:`## Purpose\n
Use this log to record a solarization event to a field or planting, typically used for weed control.\n
## Specification\n
text\n`,
    validExamples: [solarizationConventionExample],
    erroredExamples: [solarizationConventionError]
});

//add local attributes
solarizationConvention.addAttribute( { schemaOverlayObject:log__activity__solarization, attributeName: "log--activity--solarization", required: true } );

//add global attributes
solarizationConvention.addAttribute( { schemaOverlayObject:"taxonomy_term--log_category--weed_control", attributeName: "taxonomy_term--log_category--weed_control", required: true } );
solarizationConvention.addAttribute( { schemaOverlayObject:"quantity--standard--area_percentage", attributeName: "quantity--standard--area_percentage", required: false});
solarizationConvention.addAttribute( { schemaOverlayObject:"taxonomy_term--unit--%", attributeName: "taxonomy_term--unit--%", required: false});
solarizationConvention.addAttribute( { schemaOverlayObject:"asset--plant--planting", attributeName: "plant_asset", required: true } );

//relationships
solarizationConvention.addRelationship( { containerEntity:"log--activity--solarization" , relationName:"category" , mentionedEntity:"taxonomy_term--log_category--weed_control" , required: true } );
solarizationConvention.addRelationship( { containerEntity:"log--activity--solarization" , relationName:"quantity" , mentionedEntity:"quantity--standard--area_percentage" , required: false } );
solarizationConvention.addRelationship( { containerEntity:"quantity--standard--area_percentage" , relationName:"units" , mentionedEntity:"taxonomy_term--unit--%" , required: false } );
solarizationConvention.addRelationship( { containerEntity:"log--activity--solarization", relationName:"asset", mentionedEntity:"plant_asset", required: true } );

let plantAssetExampleAttributes = solarizationConvention.overlays.plant_asset.validExamples[0].attributes;
let plantAssetUUID = solarizationConvention.overlays.plant_asset.validExamples[0].id;

var solarizationConventionExample = {
    id: solarization_convention_uuid,
    type: "Object",
    plant_asset: {
        id: plantAssetUUID,
        attributes: plantAssetExampleAttributes
    },
    "log--activity--solarization": {
        id: log__activity__solarization_uuid,
        attributes: {
            name: "example start solarization log",
            status: "done",
            timestamp: ( new Date() ).toISOString(),
        },
        relationships: {
            asset: { data: [
                {
                    type: "asset--plant",
                    id: plantAssetUUID
                }
            ]},
            quantity: { data: [
                {
                    type: "quantity--standard",
                    id: quantity__standard__area_percentage_uuid
                }
            ] },
            category: { data: [
                {
                    type: "taxonomy_term--log_category",
                    id:taxonomy_term__log_category__weed_control_uuid
                }
            ] }
        },
    },
    "taxonomy_term--log_category--weed_control": {
        id: taxonomy_term__log_category__weed_control_uuid,
        attributes: {
            name: "weed_control"
        }
    },
    "quantity--standard--area_percentage": {
        id: quantity__standard__area_percentage_uuid,
        attributes: {
            label: "area"
        },
        relationships: {
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: taxonomy_term__unit__percentage_uuid
                }
            ] }
        }
    },
    "taxonomy_term--unit--%":  {
        id: taxonomy_term__unit__percentage_uuid,
        attributes: {
            name: "%"
        },   
    },
};

var solarizationConventionError = {
    id: solarization_convention_uuid,
    type: "Object",
    plant_asset: {
        id: plantAssetUUID,
        attributes: plantAssetExampleAttributes
    },
    "log--activity--solarization": {
        id: log__activity__solarization_uuid,
        attributes: {
            name: "example solarization log",
            status: "false",
        },
        relationships: {
            asset: { data: [
                {
                    type: "asset--plant",
                    id: plantAssetUUID
                }
            ]},
            quantity: { data: [
                {
                    type: "quantity--standard",
                    id: quantity__standard__area_percentage_uuid
                }
            ] },
            category: { data: [
                {
                    type: "taxonomy_term--log_category",
                    id:taxonomy_term__log_category__weed_control_uuid
                }
            ] }
        },
    },
    "quantity--standard--area_percentage": {
        id: quantity__standard__area_percentage_uuid,
        attributes: {
            label: "area"
        },
        relationships: {
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: taxonomy_term__unit__percentage_uuid
                }
            ] }
        }
    },
    "taxonomy_term--unit--%":  {
        id: taxonomy_term__unit__percentage_uuid,
        attributes: {
            name: "%"
        },   
    },
};


solarizationConvention.validExamples = [solarizationConventionExample];
solarizationConvention.erroredExamples = [solarizationConventionError];

let test = solarizationConvention.testExamples();
let storageOperation = solarizationConvention.store();
