// Survey: https://app.surveystack.io/surveys/6346c1e467f839000121503e/edit
// Section: 11, planting_div_veg
const builder = require(`../src/convention_builder`);
const { randomUUID } = require('crypto');
const fs = require('fs');

//UUIDs
let log__activity__termination_uuid = randomUUID();
let termination_convention_uuid = randomUUID();
let taxonomy_term__log_category__termination_uuid = randomUUID();
let quantity__standard__area_percentage_uuid = randomUUID();
let taxonomy_term__unit__percentage_uuid = randomUUID();

//Main entity
let log__activity__termination_example = {
    id: log__activity__termination_uuid,
    attributes: {
        status: "done"
    }
};

let log__activity__termination_error = {
    id: log__activity__termination_uuid,
    attributes: {
        status: "processing"
    }
};
let log__activity__termination = new builder.SchemaOverlay({
    typeAndBundle: 'log--activity',
    name: 'termination',
    validExamples: [log__activity__termination_example],
    erroredExamples: [log__activity__termination_error]    
});
log__activity__termination.setMainDescription("Add termination event if an termination date is included in the planting information.");

log__activity__termination.setConstant({
    attribute:"status",
    value:"done",
    description: "The status should always be set to done to inherit the area."
});

log__activity__termination.setConstant({
    attribute:"is_termination",
    value:"true",
});

//taxonomy term - log_category - moved to global
//let taxonomy_term__log_category__termination_example = {
//    id: logCategoryUUID,
//    attributes: {
//        name: "termination",
//    }
//};
//let taxonomy_term__log_category__termination_error = {
//    id: logCategoryUUID,
//    attributes: {
//        name: "done",
//    }
//};
//let taxonomy_term__log_category__termination = new builder.SchemaOverlay({
//    typeAndBundle: "taxonomy_term--log_category",
//    name: "termination",
//    validExamples: [taxonomy_term__log_category__termination_example],
//    erroredExamples: [taxonomy_term__log_category__termination_error],
//});
//taxonomy_term__log_category__termination.setMainDescription("Log category is set to termination.") 


//do we need anything for log harvest here? like log category or yield quantity or should that be a separate convention

//Convention
// Object
let terminationConvention = new builder.ConventionSchema({
    title: "Termination",
    version: "0.0.1",
    schemaName:"log--activity--termination",
    repoURL:"www.gitlabrepo.com/version/farmos_conventions",
    description:`## Purpose\n
Termination log to be applied to a plant asset only if the farmer indicated the date of termination.\n
## Specification\n
text\n`,
    validExamples: [terminationConventionExample],
    erroredExamples: [terminationConventionError]
});

////add local attributes
terminationConvention.addAttribute( { schemaOverlayObject:log__activity__termination, attributeName: "log--activity--termination", required: true } );

//add global attributes
terminationConvention.addAttribute( { schemaOverlayObject:"taxonomy_term--log_category--termination", attributeName: "taxonomy_term--log_category--termination", required: true});
terminationConvention.addAttribute( { schemaOverlayObject:"asset--plant--planting", attributeName: "asset--plant--planting", required: true } );
terminationConvention.addAttribute( { schemaOverlayObject: "quantity--standard--area_percentage", attributeName: "quantity--standard--area_percentage", required: false});
terminationConvention.addAttribute( { schemaOverlayObject: "taxonomy_term--unit--%", attributeName: "taxonomy_term--unit--%", required: false});

////add relationships
terminationConvention.addRelationship( { containerEntity: "log--activity--termination", relationName:"category", mentionedEntity:"taxonomy_term--log_category--termination", required: true});
terminationConvention.addRelationship( { containerEntity:"log--activity--termination", relationName:"asset", mentionedEntity:"asset--plant--planting", required: true } );
terminationConvention.addRelationship( { containerEntity:"log--activity--termination" , relationName:"quantity" , mentionedEntity:"quantity--standard--area_percentage" , required: false } );
terminationConvention.addRelationship( { containerEntity:"quantity--standard--area_percentage" , relationName:"units" , mentionedEntity:"taxonomy_term--unit--%" , required: false } );

let asset__plant__planting_attributes = terminationConvention.overlays["asset--plant--planting"].validExamples[0].attributes;
let asset__plant__planting_uuid = terminationConvention.overlays["asset--plant--planting"].validExamples[0].id;

var terminationConventionExample = {
        id: termination_convention_uuid,
        type: "Object",
        "asset--plant--planting": {
            id: asset__plant__planting_uuid,
            attributes: asset__plant__planting_attributes
        },
        "log--activity--termination": {
            id: log__activity__termination_uuid,
            attributes: {
                name: "example termination log",
                status: "done",
                is_termination: "true",
                timestamp: ( new Date() ).toISOString(),
            },
            relationships: {
                category: { data: [
                    {
                        type: "taxonomy_term--log_category",
                        id:taxonomy_term__log_category__termination_uuid
                    }
                ] },
                asset: { data: [
                    {
                        type: "asset--plant",
                        id: asset__plant__planting_uuid
                    },
                ] }
            },
        },
        "taxonomy_term--log_category--termination": {
                id: taxonomy_term__log_category__termination_uuid,
                attributes: {
                    name: "termination"
                }
        },
        "quantity--standard--area_percentage": {
            id: quantity__standard__area_percentage_uuid,
            attributes: {
                label: "area"
            },
            relationships: {
                units: { data: [
                    {
                        type:"taxonomy_term--unit",
                        id: taxonomy_term__unit__percentage_uuid
                    }
                ] }
            }
        },
        "taxonomy_term--unit--%":  {
            id: taxonomy_term__unit__percentage_uuid,
            attributes: {
                name: "%"
            },   
        },
};

var terminationConventionError = {
    id: termination_convention_uuid,
    type: "Object",
    "asset--plant--planting": {
        id: asset__plant__planting_uuid,
        attributes: asset__plant__planting_attributes
    },
    "log--activity--termination": {
        id: log__activity__termination_uuid,
        attributes: {
            name: "example termination log",
            timestamp: ( new Date() ).toISOString(),
            //status: "done",
            //is_termination: "true"
        },
        relationships: {
            category: { data: [
                {
                    type: "taxonomy_term--log_category",
                    id:taxonomy_term__log_category__termination_uuid
                }
            ] },
            asset: { data: [
                {
                    type: "asset--plant",
                    id: asset__plant__planting_uuid
                },
            ] }
        },
    },
    "taxonomy_term--log_category--termination": {
        id: taxonomy_term__log_category__termination_uuid,
            attributes: {
                name: "termination"
            }
    },
    "quantity--standard--area_percentage": {
        id: quantity__standard__area_percentage_uuid,
        attributes: {
            label: "area"
        },
        relationships: {
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: taxonomy_term__unit__percentage_uuid
                }
            ] }
        }
    },
    "taxonomy_term--unit--%":  {
        id: taxonomy_term__unit__percentage_uuid,
        attributes: {
            name: "%"
        },   
    },
};

terminationConvention.validExamples = [terminationConventionExample];
terminationConvention.erroredExamples = [terminationConventionError];

let test = terminationConvention.testExamples();
let storageOperation = terminationConvention.store();
