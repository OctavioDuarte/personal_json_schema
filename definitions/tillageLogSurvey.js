// Survey: https://app.surveystack.io/surveys/6346c1e467f839000121503e/edit
// Section: 19, soil disturbance
const builder = require(`../src/convention_builder`);
const { randomUUID } = require('crypto');
const fs = require('fs');

//ids for the examples
let quantity__standard__stir_uuid = randomUUID();
let quantity__standard__tillage_depth_uuid = randomUUID();
let tillage_convention_uuid = randomUUID();
let quantity__standard__area_percentage_uuid = randomUUID();
let taxonomy_term__unit__percentage_uuid = randomUUID();
let taxonomy_term__log_category__tillage_uuid = randomUUID();
let log__activity__tillage_uuid = randomUUID();
//let implementUUID = randomUUID();
//let implementUnitUUID = randomUUID();
let taxonomy_term__unit__depth_uuid = randomUUID();
let quantity__standard__tillage_speed_uuid = randomUUID();
let taxonomy_term__unit__speed_uuid = randomUUID();

// main entity examples -- rewritten in next section
// let validExample = { attributes:{ name: "tillage", status:"done", data: "plow" } };
// let errorExample_1 = { attributes:{ name: "tillage_error", data: "activity" } };
// let errorExample_2 = { attributes:{ name: "tillage_error", status:2, data: "wrong" } };

//main entity
////Example
let log__activity__tillage_example = {
    id:log__activity__tillage_uuid,
    attributes: {
        name: "tillage",
        status:"done",
        data:"plow",
    }
};
let log__activity__tillage_error = {
    id:log__activity__tillage_uuid,
    attributes: {
        name: "tillage_error",
        status:"false",
        data:"activity",
    }
};
let log__activity__tillage_error2 = {
    id:log__activity__tillage_uuid,
    attributes: {
        name: "tillage_error",
        status:"2",
        data:"wrong",
    }
};

let log__activity__tillage = new builder.SchemaOverlay({
    typeAndBundle: "log--activity",
    name: "tillage",
    validExamples: [log__activity__tillage_example],
    erroredExamples: [ log__activity__tillage_error, log__activity__tillage_error2 ]
});

log__activity__tillage.setMainDescription("Must be related to a taxonomy_term--log_category named *tillage* and be related to an *asset--land*. Should have quantity--standard--stir, quantity--standard--residue, quantity--standard--tillage_depth. May have other taxonomy_term--log_category. Originally hosted in [link](https://gitlab.com/OpenTEAMAg/ag-data-wallet/openteam-convention/-/blob/main/descriptions/log--activity--tillage.md)");
// required attribute is status
log__activity__tillage.setConstant({
    attribute:"status",
    value:"done"
});


//stir quantity overlay
let quantity__standard__stir = new builder.SchemaOverlay({
    typeAndBundle: "quantity--standard",
    name: "stir"
});
quantity__standard__stir.setMainDescription("Must be labelled as *stir* and it's measure type is *ratio*. See documentation ADD LINK to get the standard specification for this ratio.");
quantity__standard__stir.setConstant( {
    attribute:"measure",
    value:"ratio",
} );

//depth quantity overlay
let quantity__standard__tillage_depth = new builder.SchemaOverlay({
    typeAndBundle: "quantity--standard",
    name: "tillage_depth"
});
quantity__standard__tillage_depth.setMainDescription("Must be labelled as *depth* and it's measure type is *length*.");
quantity__standard__tillage_depth.setConstant( {
    attribute:"label",
    value:"depth",
} );
quantity__standard__tillage_depth.setConstant( {
    attribute:"measure",
    value:"length",
} );

//LOOK do we need a depth unit if it's always inches?
let taxonomy_term__unit__depth = new builder.SchemaOverlay({
    typeAndBundle: "taxonomy_term--unit",
    name: "depth"
});
taxonomy_term__unit__depth.setMainDescription("Tillage depth is in inches");

taxonomy_term__unit__depth.setConstant( {
    attribute: "name",
    value: "in"
})

//speed overlay
let quantity__standard__tillage_speed = new builder.SchemaOverlay({
    typeAndBundle: "quantity--standard",
    name: "tillage_speed"
});
quantity__standard__tillage_speed.setMainDescription("Must be labelled as *speed* and it's measure type is *speed*.");
quantity__standard__tillage_speed.setConstant( {
    attribute:"label",
    value:"speed",
} );
quantity__standard__tillage_speed.setConstant( {
    attribute:"measure",
    value:"speed",
} );

//LOOK do we need a speed unit if it's always mph?
let taxonomy_term__unit__speed = new builder.SchemaOverlay({
    typeAndBundle: "taxonomy_term--unit",
    name: "speed"
});
taxonomy_term__unit__speed.setMainDescription("The speed for mechanized tillage should always be entered in mph");

taxonomy_term__unit__speed.setConstant( {
    attribute: "name",
    value: "mph"
});


//tillage implement
////Examples
/*let implementExample = {
    id:implementUUID,
    attributes: {
        name: "lbs_acres",
    }
};
let implementError = {
    id:implementUnitUUID,
    attributes: {
        name: "ft",
    }
};
////Overlays and constants
let implement = new builder.SchemaOverlay({
    typeAndBundle: "taxonomy_term--unit",
    name: "lime unit",
    validExamples: [ limeUnitExample ],
    erroredExamples: [ limeUnitError ]
});
implement.setEnum({
    attribute: "name",
    valuesArray: [
        "add"
    ],
    description: ""
});*/

let taxonomy_term__log_category__tillage = new builder.SchemaOverlay({
    typeAndBundle: "taxonomy_term--log_category",
    name:"tillage"
});

taxonomy_term__log_category__tillage.setMainDescription("The only identifier for a tillage log activity *MUST* be its tillage log category taxonomy term.");
taxonomy_term__log_category__tillage.setConstant( {
    attribute:"name",
    value:"tillage"
} );

//tillage convention
let tillageConvention = new builder.ConventionSchema({
    title: "Tillage",
    version:"0.0.1",
    schemaName:"log--activity--tillage",
    repoURL:"www.gitlabrepo.com/version/farmos_conventions",
    description:`## Purpose\n
A tillage log encompasses all information we can gather about a tillage operation.\n
## Specification\n
text\n`,
    validExamples: [  ],
    erroredExamples: [  ]
});

//add local attributes
tillageConvention.addAttribute( { schemaOverlayObject:log__activity__tillage, attributeName: "log--activity--tillage", required: true } );
tillageConvention.addAttribute( { schemaOverlayObject:quantity__standard__stir, attributeName: "quantity--standard--stir", required: false } );
tillageConvention.addAttribute( { schemaOverlayObject:quantity__standard__tillage_depth, attributeName: "quantity--standard--tillage_depth", required: false } );
tillageConvention.addAttribute({ schemaOverlayObject:taxonomy_term__log_category__tillage, attributeName: "taxonomy_term--log_category--tillage", required: false});
tillageConvention.addAttribute({ schemaOverlayObject:taxonomy_term__unit__depth, attributeName: "taxonomy_term--unit--depth", required: false});
tillageConvention.addAttribute({ schemaOverlayObject:quantity__standard__tillage_speed, attributeName: "quantity--standard--tillage_speed", required: false});
tillageConvention.addAttribute({ schemaOverlayObject:taxonomy_term__unit__speed, attributeName: "taxonomy_term--unit--speed", required: false});

//add global attributes
tillageConvention.addAttribute( { schemaOverlayObject:"quantity--standard--area_percentage", attributeName: "quantity--standard--area_percentage", required: false});
tillageConvention.addAttribute( { schemaOverlayObject:"taxonomy_term--unit--%", attributeName: "taxonomy_term--unit--%", required: false});
tillageConvention.addAttribute( { schemaOverlayObject:"asset--plant--planting", attributeName: "asset--plant--planting", required: true } );

//add relationships
tillageConvention.addRelationship( { containerEntity:"log--activity--tillage" , relationName:"quantity" , mentionedEntity:"quantity--standard--stir" , required: false } );
tillageConvention.addRelationship( { containerEntity:"log--activity--tillage" , relationName:"quantity" , mentionedEntity:"quantity--standard--tillage_depth" , required: false } );
tillageConvention.addRelationship( { containerEntity:"quantity--standard--tillage_depth" , relationName:"units" , mentionedEntity:"taxonomy_term--unit--depth" , required: false } );
tillageConvention.addRelationship( { containerEntity:"log--activity--tillage" , relationName:"quantity" , mentionedEntity:"quantity--standard--area_percentage" , required: false } );
tillageConvention.addRelationship( { containerEntity:"quantity--standard--area_percentage" , relationName:"units" , mentionedEntity:"taxonomy_term--unit--%" , required: false } );
tillageConvention.addRelationship( { containerEntity:"log--activity--tillage" , relationName:"quantity" , mentionedEntity:"quantity--standard--tillage_speed" , required: false } );
tillageConvention.addRelationship( { containerEntity:"quantity--standard--tillage_speed" , relationName:"units" , mentionedEntity:"taxonomy_term--unit--speed" , required: false } );
tillageConvention.addRelationship( { containerEntity:"log--activity--tillage", relationName:"asset", mentionedEntity:"asset--plant--planting", required: true } );
tillageConvention.addRelationship( { containerEntity:"log--activity--tillage", relationName:"category", mentionedEntity:"taxonomy_term--log_category--tillage", required: true } );

let asset__plant__planting_attributes = tillageConvention.overlays['asset--plant--planting'].validExamples[0].attributes;
let asset__plant__planting_uuid = tillageConvention.overlays['asset--plant--planting'].validExamples[0].id;

let tillageConventionExample = {
    id: tillage_convention_uuid,
    type: "Object",
    "asset--plant--planting": {
        id: asset__plant__planting_uuid,
        attributes: asset__plant__planting_attributes
    },
    // each entity will be an attribute in the convention, with a reference to a known entity type.
    // we need more precission, for example constants.
    "log--activity--tillage": {
        id: log__activity__tillage_uuid,
        attributes: {
            status:'done',
            name: "tillage",
            timestamp: ( new Date() ).toISOString(),
        },
        relationships: {
            quantity: { data: [
                {
                    type: "quantity--standard",
                    id: quantity__standard__stir_uuid
                },
                {
                    type: "quantity--standard",
                    id: quantity__standard__tillage_depth_uuid
                },
                {
                    type: "quantity--standard",
                    id: quantity__standard__tillage_speed_uuid
                },
                {
                    type: "quantity--standard",
                    id: quantity__standard__area_percentage_uuid
                }
            ] },
            asset: { data: [
                {
                    type: "asset--plant",
                    id: asset__plant__planting_uuid
                }
            ]},
            category: { data: [
                {
                    type: "taxonomy_term--log_category",
                    id:taxonomy_term__log_category__tillage_uuid
                }
            ] },
        },
    },
    "quantity--standard--stir": {
        attributes: {label: "STIR"},
        id: quantity__standard__stir_uuid
    },
    "quantity--standard--tillage_depth": {
        attributes: {
            label:"depth"
        },
        relationships: {
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: taxonomy_term__unit__depth_uuid
                }
            ] }
        },
        id: quantity__standard__tillage_depth_uuid
    },
    "taxonomy_term--unit--depth":  {
        id: taxonomy_term__unit__depth_uuid,
        attributes: {
            name: "in"
        },   
    },
    "quantity--standard--tillage_speed": {
        attributes: {
            label:"speed"
        },
        relationships: {
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: taxonomy_term__unit__speed_uuid
                }
            ] }
        },
        id: quantity__standard__tillage_speed_uuid
    },
    "taxonomy_term--unit--speed":  {
        id: taxonomy_term__unit__speed_uuid,
        attributes: {
            name: "mph"
        },   
    },
    "quantity--standard--area_percentage": {
        id: quantity__standard__area_percentage_uuid,
        attributes: {
            label: "area"
        },
        relationships: {
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: taxonomy_term__unit__percentage_uuid
                }
            ] }
        }
    },
    "taxonomy_term--unit--%":  {
        id: taxonomy_term__unit__percentage_uuid,
        attributes: {
            name: "%"
        },   
    },
    "taxonomy_term--log_category--tillage":  {
        id: taxonomy_term__log_category__tillage_uuid,
        attributes: {
            name: "tillage"
        },   
    }
};

let tillageConventionError = {
    // each entity will be an attribute in the convention, with a reference to a known entity type.
    // we need more precission, for example constants.
    "log--activity--tillage": {
        id: log__activity__tillage_uuid,
        attributes: {
            status:'done',
            name: "tillage log"
        },
        relationships: {
            quantity: { data: [ {
                type: "quantity--standard",
                id: '695e1d38-a1d6-4ed7-8d9d-c0b818a2cbfe'
            } ] }
        },
        relationships: {
            quantity: { data: [ {
                type: "quantity--standard",
                id: 'b71f7bb3-da43-40b1-8dc3-965e5f912763'
            } ] }
        }
    },
    "quantity--standard--stir": {
        id: quantity__standard__stir_uuid,
        attributes: {label: "stir"}
    },
};

tillageConvention.validExamples = [tillageConventionExample];
tillageConvention.erroredExamples = [tillageConventionError];

let test = tillageConvention.testExamples();
let storageOperation = tillageConvention.store();
