// Survey: https://app.surveystack.io/surveys/6346c1e467f839000121503e/edit
// Section: 11, planting_div_veg
const builder = require(`../src/convention_builder`);
const { randomUUID } = require('crypto');

//UUIDs
let log__transplanting__transplanting_uuid = randomUUID();
let taxonomy_term__log_category__transplanting_uuid = randomUUID();
let transplanting_convention_uuid = randomUUID();
let taxonomy_term__unit__percentage_unit_uuid = randomUUID();
let quantity__standard__area_percentage_uuid = randomUUID();

//Main entity
let log__transplanting__transplanting_example = {
    id:log__transplanting__transplanting_uuid,
    attributes: {
        name: "example transplanting log",
        status: "done",
        is_movement: "true",
        timestamp: String( new Date().getTime() / 1000 + 60 )
    }
};
let log__transplanting__transplanting_error = {
    id:log__transplanting__transplanting_uuid,
    attributes: {
        name: "example transplanting log",
        status: "pending",
        is_movement: "false"
    }
};
let log__transplanting__transplanting = new builder.SchemaOverlay({
    typeAndBundle: 'log--transplanting',
    name: 'transplanting',
    validExamples: [log__transplanting__transplanting_example],
    erroredExamples: [log__transplanting__transplanting_error]    
});
log__transplanting__transplanting.setMainDescription("If the crop was seeded and then moved a transplanting log is required.");

log__transplanting__transplanting.setConstant({
    attribute:"status",
    value:"done",
    description: "The status should always be set to done to inherit the area."
});
log__transplanting__transplanting.setConstant({
    attribute: "is_movement",
    value: "true"
});

//moved to global
//log category examples
//let logCategoryExample = {
//    id: logCategoryUUID,
//    attributes: {
//        name: "transplanting"
//    }
//};
//let logCategoryError = {
//    id: logCategoryUUID,
//    attributes: {
//        label: "activity"
//    }
//};
//taxonomy term - log_category
//let logCategory = new builder.SchemaOverlay({
//    typeAndBundle: "taxonomy_term--log_category",
//    name: "transplanting",
//    validExamples: [logCategoryExample],
//    erroredExamples: [logCategoryError],
//});
//logCategory.setConstant({
//    attribute: "name",
//    value: "transplanting"
//});
//logCategory.setMainDescription("Log category is set to transplanting.")

//Convention
let transplantingConvention = new builder.ConventionSchema({
    title: "Transplanting",
    version: "0.0.1",
    schemaName:"log--transplanting--div_veg_planting",
    repoURL:"www.gitlabrepo.com/version/farmos_conventions",
    description:`## Purpose\n
A transplanting log is required when crops are seeded and then moved.\n
## Specification\n
text\n`,
    // LOOK removed "OLD" to get the new, working example.
    validExamples: [],
    erroredExamples: []
});

//add local attributes
transplantingConvention.addAttribute({ schemaOverlayObject:log__transplanting__transplanting, attributeName: "log--transplanting--transplanting", required: true});

//add global attributes
transplantingConvention.addAttribute({ schemaOverlayObject:"taxonomy_term--log_category--transplanting", attributeName: "taxonomy_term--log_category--transplanting", required: true});
transplantingConvention.addAttribute({ schemaOverlayObject:"quantity--standard--area_percentage", attributeName: "quantity--standard--area_percentage", required: true});
transplantingConvention.addAttribute({ schemaOverlayObject:"taxonomy_term--unit--%", attributeName: "taxonomy_term--unit--%", required: true});
transplantingConvention.addAttribute( { schemaOverlayObject:"asset--plant--planting", attributeName: "asset--plant--planting", required: true } );

//Add relationships
transplantingConvention.addRelationship({ containerEntity:"log--transplanting--transplanting", relationName:"category", mentionedEntity:"taxonomy_term--log_category--transplanting", required:true});
transplantingConvention.addRelationship({ containerEntity:"log--transplanting--transplanting", relationName:"quantity", mentionedEntity:"quantity--standard--area_percentage", required:true});
transplantingConvention.addRelationship({ containerEntity:"quantity--standard--area_percentage", relationName:"units", mentionedEntity:"taxonomy_term--unit--%", required:true});
transplantingConvention.addRelationship( { containerEntity:"log--transplanting--transplanting", relationName:"asset", mentionedEntity:"asset--plant--planting", required: true } );

let asset__plant__planting_attributes = transplantingConvention.overlays['asset--plant--planting'].validExamples[0].attributes;
let asset__plant__planting_uuid = transplantingConvention.overlays['asset--plant--planting'].validExamples[0].id;

let transplantingConventionExample = {
    id: transplanting_convention_uuid,
    "asset--plant--planting": {
        id: asset__plant__planting_uuid,
        attributes: asset__plant__planting_attributes
    },
    "log--transplanting--transplanting": {
        id:log__transplanting__transplanting_uuid,
        attributes: {
            name: "Example div veg transplanting log",
            is_movement: "true",
            status:"done",
            timestamp: ( new Date() ).toISOString()
        },
        relationships: {
            asset: { data: [
                {
                    type: "asset--plant",
                    id: asset__plant__planting_uuid
                }
            ] },
            quantity: { data: [
                {
                    type: "quantity--standard",
                    id: quantity__standard__area_percentage_uuid
                }
            ] },
            category: { data: [
                {
                    type: "taxonomy_term--log_category",
                    id:taxonomy_term__log_category__transplanting_uuid
                }
            ] }
        },
    },
    // This is the right, top level.
    "taxonomy_term--log_category--transplanting": {
        id: taxonomy_term__log_category__transplanting_uuid,
        attributes: {
            name: "transplanting"
        }
    },
    "quantity--standard--area_percentage": {
        id: quantity__standard__area_percentage_uuid,
        attributes: {
            label: "area"
        },
        relationships: {
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: taxonomy_term__unit__percentage_unit_uuid
                }
            ] }
        }
    },
    "taxonomy_term--unit--%":  {
        id: taxonomy_term__unit__percentage_unit_uuid,
        attributes: {
            name: "%"
        },   
    }
};

let transplantingConventionError = {
    id: transplanting_convention_uuid,
    "log--transplanting--transplanting": {
        id: log__transplanting__transplanting_uuid,
        attributes: {
            name: "Example div veg transplanting log",
            status: "done"
        },
        relationships: {
            category: { data: [
                {
                    type: "taxonomy_term--log_category",
                    id: taxonomy_term__log_category__transplanting_uuid
                }
            ] }
        },
    },
    "taxonomy_term--log_category--transplanting": {
        id: taxonomy_term__log_category__transplanting_uuid,
        attributes: {
            name: "blah"
        }
    }
};

transplantingConvention.validExamples = [transplantingConventionExample];
transplantingConvention.erroredExamples = [transplantingConventionError];

let test = transplantingConvention.testExamples();
let storageOperation = transplantingConvention.store();


// let error = test.failedExamples[0].errors[0];
// transplantingConvention.schema.properties['log--transplanting--transplanting'].properties.attributes.properties.timestamp;
