// this file is companion to our tutorial blogpost 'How to define your own conventions'
const builder = require(`../src/convention_builder`);
// to populate the id attributes
const { randomUUID } = require('crypto');


// First overlay: plant asset.

let plantingPlantAsset = new builder.SchemaOverlay({
    // this is the type and bundle, which should be available in our input collection
    typeAndBundle: 'asset--plant',
    // this is a name used internally. The overlay will always be named by adding this together with the typeandBundle, so this one will be called 'asset--plant--planting'
    name: 'planting',
    // we have the option to provide valid and invalid examples, which allows to control if we are effectively accepting the cases we want to accept and rejecting the ones we want to reject.
    // validExamples:,
    // erroredExamples:    
});


plantingPlantAsset.setMainDescription("This overlay represents plants as they are encoded in Octavio's farm, a medium sized comercial orchard in Argentina.");

plantingPlantAsset.setConstant({
    // mention what we are fixing to a constant
    attribute:"status",
    // choose the fixed value
    value:"active",
    // explain why we do this
    description:"In this farm, plantings are only entered when already active."
});

plantingPlantAsset.setConstant({
    attribute:"geometry",
    value:null,
    description: "Geometry will be stored into the 'field' convention, therefore we omit it here."
});


plantingPlantAsset.setPattern({
    // as with the other restriction we've seen (setConstant), we choose a target attribute between the ones defined by the schema.
    attribute:"name",
    // Instead of a unique value, we provide a regular expression representing how we encode our planting names.
    pattern:"((lettuce)|(tomato)|(corn))-([a-z0-9]{4})-([0-9]{2})",
    description: "This regular expression represents the planting name format in this farm: the involved species name, a 4 character alphanumeric code and the current decade, represented by two further numbers."
});

let plantAssetUUID = randomUUID();

// first, I want to ensure valid examples are accepted for each crop I'm cultivating.
let validTomato = {
    id:plantAssetUUID,
    attributes: {
        name: "tomato-ae7y-23",
        geometry: null,
        status:"active"
    }
};
let validLettuce = {
    id:plantAssetUUID,
    attributes: {
        name: "lettuce-4e7y-22",
        geometry: null,
        status:"active"
    }
};
let validCorn = {
    id:plantAssetUUID,
    attributes: {
        name: "corn-4e8y-23",
        geometry: null,
        status:"active"
    }
};

let validPlantAssetExamples = [
    validTomato, validLettuce, validCorn
];

// I will create examples with invalid names to ensure my pattern is working and also with a missing status.
let errorInDecadeNoStatus = {
    id:plantAssetUUID,
    attributes: {
        // this decade has only one instead of the two mandatory digits
        name: "tomato-ae7y-3",
        geometry: null,
        // the enforced property 'status' is missing
    }
};
let unknownCropWrongStatus = {
    id:plantAssetUUID,
    attributes: {
        // this is not contemplated by our pattern
        name: "chard-ae7y-23",
        geometry: null,
        // the enforced property 'status' has a value other than our constant
        status: "ready"
    }
};

let plantAssetErrorExamples = [ errorInDecadeNoStatus, unknownCropWrongStatus ];

// let's populate the fields that were empty in our overlay with these new examples.
plantingPlantAsset.validExamples = validPlantAssetExamples;
plantingPlantAsset.erroredExamples = plantAssetErrorExamples;

// let's evaluate if our examples turn out as expected
let plantAssetTestResult = plantingPlantAsset.testExamples();


// second overlay: species

// let's first create the examples, and the UUID we need to create them.
let speciesUUID = randomUUID();
let speciesExample = {
    id: speciesUUID,
    attributes: {
        name: "corn"
    }
};
let speciesError = {
    id: speciesUUID,
    attributes: {
        // let's choose a species we are not cultivating
        name: "kale"
    }
};

// now, let's create the full overlat at once
let plantingSpecies = new builder.SchemaOverlay({
    typeAndBundle: "taxonomy_term--plant_type",
    name: "species",
    validExamples: [speciesExample],
    erroredExamples: [speciesError],
});
// remember to document your work!
plantingSpecies.setMainDescription("The species of the vegetable planted. This establishment only works with tomato, lettuce, and corn.");

// let's limit the species name to the ones we know we will deal with.
plantingSpecies.setEnum({
    attribute:"name",
    valuesArray:["tomato", "lettuce", "corn"],
    description:"Limited to a subset of know crops.",
});


let plantingSpeciesTest = plantingSpecies.testExamples();


// third overlay: variety

// let's first create the examples, and the UUID we need to create them.
let varietyUUID = randomUUID();
let varietyExample = {
    id: varietyUUID,
    attributes: {
        name: "tomato-black_russian"
    }
};
let varietyError = {
    id: speciesUUID,
    attributes: {
        // let's choose an ill formed pattern
        name: "black_russian-tomato"
    }
};

// now, let's create the full overlat at once
let plantingVariety = new builder.SchemaOverlay({
    typeAndBundle: "taxonomy_term--plant_type",
    name: "variety",
    validExamples: [varietyExample],
    erroredExamples: [varietyError],
});
// remember to document your work!
plantingVariety.setMainDescription("The variety. The name has a fixed format, prepended by the species in lowercase, separeted by a dash from the variety name.");

// let's limit the species name to the ones satisfying our format.
plantingVariety.setPattern({
    attribute:"name",
    pattern:"^((tomato)|(lettuce)|(corn))-",
    description:"Our farm's format is enforced: the species goes first, than a dash, than the variety.",
});


let plantingVarietyTest = plantingVariety.testExamples();

// Fourth overlay: season

let seasonUUID = randomUUID();
let seasonExample = {
    id: seasonUUID,
    attributes: {
        name: "spring-2022"
    }
};
let seasonError = {
    id: seasonUUID,
    attributes: {
        // let's use a wrong century
        name: "spring-2122"
    }
};
let season = new builder.SchemaOverlay({
    typeAndBundle: "taxonomy_term--season",
    name: "season",
    validExamples: [seasonExample],
    erroredExamples: [seasonError]
});
season.setMainDescription("A season taxonomy term should exist for each working period, and logs and assets should be associated to the related seasons. Seasons are always a temperate season, separated by a dash from it's beggining year.");


let plantingSeasonTest = season.testExamples();


season.setPattern({
    attribute: "name",
    pattern:"((spring)|(summer)|(winter)|(autumn))-20[0-9]{2}",
    description: "Seasons are always a temperate season, separated by a dash from it's beggining year."
});


// test again

plantingSeasonTest = season.testExamples();




// // // Convention
let plantingConvention = new builder.ConventionSchema({
    title: "Octavio's Orchard Vegetable Planting",
    version: "0.1.0",
    schemaName:"asset--plant--octavio_orchard_planting",
    repoURL:"www.gitlabrepo.com/version/farmos_conventions",
    description:`## Purpose\n
This is the main entity that represents a cultivar. It will be referenced by all subsequent managment logs.`,
    validExamples: [],
    erroredExamples: []
});

plantingConvention.addAttribute( { schemaOverlayObject:plantingPlantAsset, attributeName: "plant_asset", required: true } );
plantingConvention.addAttribute( { schemaOverlayObject:plantingSpecies, attributeName: "species_taxonomy", required: true } );
plantingConvention.addAttribute( { schemaOverlayObject:plantingVariety, attributeName: "variety_taxonomy", required: false } );
plantingConvention.addAttribute( { schemaOverlayObject:season, attributeName: "season_taxonomy", required: true } );

plantingConvention.addRelationship( { containerEntity:"plant_asset" , relationName:"plant_type" , mentionedEntity:"species_taxonomy" , required: true } );
plantingConvention.addRelationship( { containerEntity:"plant_asset" , relationName:"plant_type" , mentionedEntity:"variety_taxonomy" , required: true } );
plantingConvention.addRelationship( { containerEntity:"plant_asset" , relationName:"season" , mentionedEntity:"season_taxonomy" , required: true } );

let conventionUUID = randomUUID();
let plantingConventionExample = {
    id: conventionUUID,
    plant_asset: {
        id:plantAssetUUID,
        attributes: validTomato.attributes,
        relationships: {
            plant_type: { data: [
                {
                    type: "taxonomy_term--plant_type",
                    id:speciesUUID
                },
                {
                    type: "taxonomy_term--plant_type",
                    id:varietyUUID
                }
            ] },
            season: { data: [
                {
                    type: "taxonomy_term--season",
                    id: seasonUUID
                }
            ] }
        }
    },
    season_taxonomy: seasonExample,
    species_taxonomy: speciesExample,
    variety_taxonomy: varietyExample
};

let plantingConventionError = {
    id: conventionUUID,
    plant_asset: {
        id:plantAssetUUID,
        attributes: {
            name: "Example div veg plant asset",
            geometry: null,
            status:"active"
        },
        relationships: {
            plant_type: { data: [
                {
                    type: "taxonomy_term--plant_type",
                    id:varietyUUID
                }
            ] }
        }
    },
    season_taxonomy: seasonError,
    species_taxonomy: speciesError,
    variety_taxonomy: varietyError
};

// add the examples to our convention

plantingConvention.validExamples = [plantingConventionExample];
plantingConvention.erroredExamples = [plantingConventionError];

let conventionTest = plantingConvention.testExamples();
let storageOperation = plantingConvention.store();
