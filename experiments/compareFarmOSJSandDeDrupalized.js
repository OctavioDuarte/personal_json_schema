// in this example we obtain a schema, test how the de drupalizing function can adapt a schema to allow it to be used with AJV and create some examples both accepted and rejected. It is worth of notice that the required fields are also covered by the schema and able to trigger validation errors.
const axios = require('axios');
const { randomUUID } = require('crypto');
const {prepareDrupalSchemaForAJV, buildValidator} = require('../src/schema_utilities.js');
const farmOS = require('farmos').default;

require('dotenv').config({ path:'../.env' });
const aggregatorKey = process.env.FARMOS_KEY;

// let draft6Schema = JSON.parse(fs.readFileSync("./schemata/draft6.json"));

let farmDomain = 'https://ourscitest.farmos.net';

let landAssetSchemaDrupal = axios.get(`${farmDomain}/api/asset/land/resource/schema`)
  .then( res => res.data )
;

let landAssetSchema = landAssetSchemaDrupal
    .then( schema => prepareDrupalSchemaForAJV(schema) )
;

const farmosOptions = {
    remote: {
        host: farmDomain,
        clientId: 'farm',
    },
};


const aggregatorKeyAuth = (request, authOpts = {}) => {
    request.interceptors.request.use(
        config => {
            return {
                ...config,
                headers: {
                    ...config.headers,
                    'api-key': aggregatorKey,
                },
                params: {
                    'farm_id': 1,
                },
            };
        },
        Promise.reject,
    );
    return {
        authorize(aggregatorKey) {
        },
    };
};

// Default token functions.
let token;
const getToken = () => token;
const setToken = (t) => { token = t; };

let farm = farmOS(
    {
        remote: {
            host: farmDomain,
            clientId: "farm",
            getToken: getToken,
            setToken: setToken,
            auth: aggregatorKeyAuth
        }
    }
);

const farmOSjsSchemata = farm.schema.fetch();

let landExample = {
    attributes: {
        name: "Example Land Asset",
        status: "active",
        land_type: "field"
    },
    relationships: {
        location: { data: [ {
            type: "asset--land",
            id: randomUUID()
        } ] }
    }
};

let landExample2 = {
    attributes:{
        name: "Example Land Asset",
        land_type: 2
    },
    relationships: {
        location: { data: [ {
            type: "asset--land",
        } ] }
    }
};

let validator = buildValidator();
let validate = landAssetSchema.then( schema => validator.compile(schema) );
validate.then( f => { return { is_valid: f(landExample), errors: f.errors }; } ).then(console.log).catch(console.error);
validate.then( f => { return { is_valid: f(landExample2), errors: f.errors }; } ).then(console.log).catch(console.error);

landAssetSchema.then( d => d.properties.relationships.properties.location.properties.data.items ).then( console.log );


let validatorFOJS = buildValidator();
let validateFOJS = farmOSjsSchemata.then( d => { let output = d.data.asset.land; delete output.$schema; return output; } ).then( schema => validatorFOJS.compile(schema) );
validateFOJS.then( f => { return { is_valid: f(landExample), errors: f.errors, errorsDetail: f.errors[0].params }; } ).then(console.log).catch(console.error);
validateFOJS.then( f => { return { is_valid: f(landExample2), errors: f.errors }; } ).then(console.log).catch(console.error);


let landExampleFOJS = {
    attributes: {
        name: "Example Land Asset",
        status: "active",
        land_type: "field"
    },
    relationships: {
        location: [ {
            type: "asset--land",
            id: randomUUID()
        } ] 
    }
};

validateFOJS.then( f => { return { is_valid: f(landExampleFOJS), errors: f.errors }; } ).then(console.log).catch(console.error);

landAssetSchema.then( d => d.properties.relationships.properties.location ).then(console.log);
farmOSjsSchemata.then( d => d.data.asset.land.properties.relationships.properties.location ).then(console.log);
