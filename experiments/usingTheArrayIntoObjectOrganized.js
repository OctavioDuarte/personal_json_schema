// This example has entities forming two diferent logs, a seeding and a tillage one.
// Knowing which Conventions are involved, we wanto to transform this array succesively into the structured objects representing each convention.
const builder = require(`./../src/convention_builder`);
const { randomUUID } = require('crypto');
const { exampleImporter } = require("./../src/schema_utilities.js");
const fs = require('fs');

// obtain the preexisting convention
let tillageConventionSrc = JSON.parse( fs.readFileSync( `./../output/collection/conventions/log--activity--tillage/object.json` ) );
let tillageConvention = new builder.ConventionSchema( tillageConventionSrc );

let seedingConventionSrc = JSON.parse( fs.readFileSync( `./../output/collection/conventions/log--seeding--div_veg_planting/object.json` ) );
let seedingConvention = new builder.ConventionSchema( seedingConventionSrc );

// reading the example
let arrayExample = exampleImporter(filename = "tillageLogSurvey_mechanized_complete.json");

// Object.keys(tillageConvention.schema.properties);
// empty object
let formattedExample = {
    tillage_log: {},
    stir_quantity: {},
    depth_quantity:{},
    area_quantity:{},
    area_unit: {},
    plant_asset: {},
    log_category: {}
};

//the 1st entity in the convention is quantity--standard, area
formattedExample.area_quantity = arrayExample[0];

//the 2nd entity in the convention is the taxonomy_term--unit, %
formattedExample.area_unit = arrayExample[1];

//the 3rd entity in the convention is quantity--standard, depth
formattedExample.depth_quantity = arrayExample[2];

//the 4th entity in the convention is taxonomy_term--unit, in - missing?

//the 5th entity in the convention is quantity--standard, speed - missing

//the 6th entity is taxonomy_term--unit, mph - missing

//7th entity is quantity--standard, stir
formattedExample.stir_quantity = arrayExample[6];

//8th entity is log--activity
formattedExample.tillage_log = arrayExample[7];

//9th entity is asset--plant
formattedExample.plant_asset = arrayExample[8];

//10th entity is taxonomy_term--log_category
formattedExample.log_category = arrayExample[9];

//11th entity is also log_category
// it is "seeding", which doesn't relate to this convention and can be safely ignored. 

//12th entity is asset--equipment
// It is a "bed_shaper", I'm not sure about it relating to tillage or seeding. 


// Get all entities related to this one, via the relationships.
// Notice that it is a trivial step in this case, but wouldn't be trivial if we had several logs of the same convention for a unique field. 
function getRelatedEntities(currentEntity,entitiesArray,idsSet=false) {
    if (!idsSet) {
        idsSet = new Set([currentEntity.id]);
    };
    let relationshipFields = Object.keys( currentEntity.relationships )
        .filter( relKey => currentEntity.relationships[relKey]?.data?.length > 0 )
    ;
    relationshipFields.forEach( field => {
        currentEntity.relationships[field].data.forEach( entry => {
            if (!idsSet.has(entry.id)) {
                idsSet.add(entry.id);
                let newEntity = entitiesArray.find( ent => ent.id == entry.id );
                if (newEntity.relationships) {
                    getRelatedEntities(newEntity,entitiesArray,idsSet);
                };
            }
        } );
    } );
    return idsSet;
};
getRelatedEntities( arrayExample[7], arrayExample );

// Look into the schemata for the convention and extract candidates for each role. We will use the 'const' values in each overlay to find candidates.
let tillageObject = {};
let freeEntities = new Set( arrayExample.map( d => d.id ) );
let tillageRestrictions = Object.keys( tillageConvention.overlays )
    // find all constants
    .map( overlay => {
        let typeAndBundle = tillageConvention.overlays[overlay].typeAndBundle;
        let modifiedProperties = tillageConvention.overlays[overlay].overlay.properties.attributes.properties;
        let restrictions = Object.keys(modifiedProperties)
            .filter( prop => modifiedProperties[prop].const )
            .map( prop => {
                return {
                    key: prop,
                    value: modifiedProperties[prop].const,
                    class: 'constant'
                };
            } )
        ;
        let enumerations = Object.keys(modifiedProperties)
            .filter( prop => modifiedProperties[prop].enum )
            .map( prop => {
                return {
                    key: prop,
                    values: modifiedProperties[prop].enum,
                    class: 'ontology'
                };
            } )
        ;
        let candidates = arrayExample
            .filter( entity => entity.type == typeAndBundle )
            .filter( entity => {
                let restrictionValues = restrictions.map( currentRestriction => {
                    return entity.attributes[currentRestriction.key] == currentRestriction.value;
                } );
                return restrictionValues.every( d => d );
            } )
        ;

        // remove candidates from the list of remaining free entities
        candidates.forEach( d => freeEntities.delete(d.id) );

        if (candidates.length == 1) {
            tillageObject[overlay] = candidates[0];
        } else if (candidates.length == 0) {
            tillageObject[overlay] = {
                id: null,
                type: undefined,
                attributes: undefined,
                status: "not found"
            };
        } else if (candidates.length > 0) {
            tillageObject[overlay] = {
                id: null,
                type: undefined,
                attributes: undefined,
                status: "ambiguous",
                candidates: candidates
            };
        };

        return {
            attributeName: overlay,
            typeAndBundle: typeAndBundle,
            restrictions: restrictions,
            enumerations: enumerations,
            candidates: candidates,
            candidatesAmount: candidates.length
        };
    } )
;


function organizeEntitiesArrayIntoConvention( { entitiesArray, conventionObject } ) {
    // let relatedIds = getRelatedEntities( -- main entity needs to be identified... -- , entitiesArray );
    let conventionalObject = {};
    let freeEntities = new Set( entitiesArray.map( d => d.id ) );

    let searchObjects = Object.keys( conventionObject.overlays )
    // find all constants and enums, which will allow to identify candidates.
        .map( overlay => {
            let typeAndBundle = conventionObject.overlays[overlay].typeAndBundle;
            let modifiedProperties = conventionObject.overlays[overlay].overlay.properties.attributes.properties;
            let restrictions = Object.keys(modifiedProperties)
                .filter( prop => modifiedProperties[prop].const )
                .map( prop => {
                    return {
                        key: prop,
                        value: modifiedProperties[prop].const,
                        class: 'constant',
                        required: conventionObject.overlays[overlay].schema.properties.attributes.properties.required?.includes(prop)
                    };
                } )
            ;
            let enumerations = Object.keys(modifiedProperties)
                .filter( prop => modifiedProperties[prop].enum )
                .map( prop => {
                    return {
                        key: prop,
                        values: modifiedProperties[prop].enum,
                        class: 'ontology',
                        required: conventionObject.overlays[overlay].schema.properties.attributes.properties.required?.includes(prop)
                    };
                } )
            ;
            let candidates = entitiesArray
                .filter( entity => entity.type == typeAndBundle )
                .filter( entity => {
                    let restrictionValues = restrictions.map( currentRestriction => {
                        let result;
                        if (entity.attributes[currentRestriction.key]) {
                            result = entity.attributes[currentRestriction.key] ==  currentRestriction.value;
                        } else if (currentRestriction.required) {
                            result = false;
                        } else {
                            result = true;
                        }
                        return result;
                    } );
                    let enumerationValues = enumerations.map( currentEnum => {
                        
                        let result;
                        if (entity.attributes[currentEnum.key]) {
                            result = currentEnum.values.includes( entity.attributes[currentEnum.key] );
                        } else if (currentEnum.required) {
                            result = false;
                        } else {
                            result = true;
                        }
                        return result;
                    } );
                    return [ ... restrictionValues, ... enumerationValues ].every( d => d );
                } )
            ;

            // remove candidates from the list of remaining free entities
            candidates.forEach( d => freeEntities.delete(d.id) );

            if (candidates.length == 1) {
                conventionalObject[overlay] = candidates[0];
            } else if (candidates.length == 0) {
                conventionalObject[overlay] = {
                    id: null,
                    type: undefined,
                    attributes: undefined,
                    status: "not found"
                };
            } else if (candidates.length > 0) {
                conventionalObject[overlay] = {
                    id: null,
                    type: undefined,
                    attributes: undefined,
                    status: "ambiguous",
                    candidates: candidates
                };
            };

            return {
                attributename: overlay,
                typeandbundle: typeAndBundle,
                restrictions: restrictions,
                enumerations: enumerations,
                candidates: candidates,
                candidatesAmount: candidates.length
            };
        } )
    ;
    return {
        search_details: searchObjects,
        structured_convention: conventionalObject,
        unused_entities: entitiesArray.filter( d => freeEntities.has(d.id) )
    };
};

let searchResultTillage = organizeEntitiesArrayIntoConvention( {
    entitiesArray: arrayExample,
    conventionObject:tillageConvention
} );
let searchResultSeeding = organizeEntitiesArrayIntoConvention( { entitiesArray: arrayExample, conventionObject:seedingConvention } );

// explaining missing stuff

// area quantity is missing the 'measure' attribute
searchResultTillage.search_details.find( d => d.attributeName == "area_quantity" );
searchResultTillage.unused_entities.find( d => d.attributes.label == "area" );

// depth quantity is missing the 'measure' attribute
searchResultTillage.search_details.find( d => d.attributeName == "depth_quantity" );
searchResultTillage.unused_entities.find( d => d.attributes.label == "depth" );

// speed quantity, same
searchResultTillage.search_details.find( d => d.attributeName.includes("speed") );
searchResultTillage.unused_entities.find( d => d.attributes.label == "speed" );

// STIR, also missing 'measure'
searchResultTillage.search_details.find( d => d.attributeName.includes("stir") );
searchResultTillage.unused_entities.find( d => d.attributes.label == "stir" );

// Asset -- plant is missing attributes because we auto generated it from the 'find or create' order.
// It is simply missing the "active" status.
searchResultTillage.search_details.find( d => d.attributeName.includes("plant") );
searchResultTillage.unused_entities.find( d => d.type == "asset--plant" );

// seeding is not used because it belongs to another convention

// equipment is not mentioned in this convention.




// another example
// obtain the preexisting convention
let solarizationConventionSrc = JSON.parse( fs.readFileSync( `./../output/collection/conventions/log--activity--solarization/object.json` ) );
let solarizationConvention = new builder.ConventionSchema( solarizationConventionSrc );

// reading the example
let solarizationExample = exampleImporter(filename = "solarizationLogSurvey_complete.json");

let searchResultSolarization = organizeEntitiesArrayIntoConvention( { entitiesArray: solarizationExample, conventionObject:solarizationConvention } );




// how to finish the work


// look into the delivered entity. It is ready when no more fields have a 'status'
searchResultTillage.structured_convention;
// create an unrelated copy of the delivered object.
let tillageFinalObj = structuredClone(searchResultTillage.structured_convention);

// NOTE we changed the area_percentage overlay. Code below is old.
// // area_unit has an 'ambiguous' status. 
// searchResultTillage.structured_convention.area_unit.candidates;
// // let's have a look at the search details
// searchResultTillage.search_details.find( d => d.attributeName == "area_unit" );
// // there are no restrictions. This seems to be wrong.
// // let's look at the schema for this overlay
// tillageConvention.overlays.area_unit.schemaName;
// tillageConvention.overlays.area_unit.schema;
// tillageConvention.overlays.area_unit.schema.properties.attributes;
// // grab the proper one
// let areaUnitChosenCandidate = tillageFinalObj
//     .area_unit
//     .candidates
//     .find( entity => entity.attributes.name == '%' )
// ;
// // replace it on the field
// tillageFinalObj.area_unit = areaUnitChosenCandidate;
// NOTE we changed the area_percentage overlay. Code above is old.

// the asset--plant needs to be manually feed with detail, since its a placeholder created by the examplesImporter function.


// look into the search details to check on what we need
// we can see in 'restrictions' that we need to have a status field. Apparently, that's all.
searchResultTillage.search_details.find( detail => detail.typeAndBundle == "asset--plant" );

// grab the entity, which is among the unused entities, as it wasn't matched by anything
let assetPlantEntity = searchResultTillage.unused_entities.find( entity => entity.type == "asset--plant" );
// add what's missing, read on the search_details object.
assetPlantEntity.attributes.status = 'active';
// replace with the new, compliant entity
tillageFinalObj.plant_asset = assetPlantEntity;


