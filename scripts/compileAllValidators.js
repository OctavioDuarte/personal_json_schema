// compiles static AJV validators for all schemas obtained from a farm.
const {compileConventionsValidator, compileGeneralValidator, storeDeDrupalizedSchemas} = require('../src/schema_utilities.js');
const fs = require('fs');

let farmosSchemata = JSON.parse( fs.readFileSync(`${ __dirname }/../input/farmos.json`) );
// let hyperSchema = JSON.parse( fs.readFileSync(`${ __dirname }/../input/collection/hyper-schema.json`) );

// Currently not done due to hughe increase in package size.
// Should check if tree shaking negates this, as the convenience would be massive.
// Build all individual validators
// Object.keys(farmosSchemata).forEach( typeKey => {
//     Object.keys(farmosSchemata[typeKey]).forEach( bundleKey => {
//         buildStaticValidator( typeKey, bundleKey, farmosSchemata );
//         // buildStaticValidatorESM(typeKey, bundleKey, farmosSchemata);
//     } );
// } );


fs.mkdirSync( "output/validators", { recursive: true }, console.error );

// build general validator
compileGeneralValidator(farmosSchemata);
compileConventionsValidator();

// store de drupalized schemas
storeDeDrupalizedSchemas(farmosSchemata);
