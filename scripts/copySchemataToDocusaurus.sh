for file in $( find output/collection/conventions/ | grep schema.json )
  do
      path=$( sed 's/output\/collection\/conventions/scripts\/docusaurus\/farm_schemas\/static\/schemas/' <<< "$file" | sed 's/schema.json//' ); 
      mkdir -p $path
      cp $file $path
done;
