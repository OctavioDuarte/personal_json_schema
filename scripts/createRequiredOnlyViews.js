let {reduceSchemaToRequiredFields} = require("../src/schema_utilities.js");
let fs = require("fs");

let staticSchemas = `${__dirname}/../scripts/docusaurus/farm_schemas/static/schemas`;

let schemata = fs.readdirSync(staticSchemas);

schemata.forEach( folder => {
    let doc = JSON.parse(fs.readFileSync(`${staticSchemas}/${ folder }/schema.json`));
    let requiredOnlyVersion = reduceSchemaToRequiredFields(doc);
    fs.writeFileSync( `${staticSchemas}/${ folder }/schema_required_only.json`, JSON.stringify(requiredOnlyVersion), console.error );
} );
