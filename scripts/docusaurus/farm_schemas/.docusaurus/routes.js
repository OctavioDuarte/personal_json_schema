import React from 'react';
import ComponentCreator from '@docusaurus/ComponentCreator';

export default [
  {
    path: '/software/json_schema_distribution/staging_wiki/blog',
    component: ComponentCreator('/software/json_schema_distribution/staging_wiki/blog', 'c23'),
    exact: true
  },
  {
    path: '/software/json_schema_distribution/staging_wiki/blog/archive',
    component: ComponentCreator('/software/json_schema_distribution/staging_wiki/blog/archive', 'c6c'),
    exact: true
  },
  {
    path: '/software/json_schema_distribution/staging_wiki/blog/convention-builder-doc-example',
    component: ComponentCreator('/software/json_schema_distribution/staging_wiki/blog/convention-builder-doc-example', '701'),
    exact: true
  },
  {
    path: '/software/json_schema_distribution/staging_wiki/blog/tags',
    component: ComponentCreator('/software/json_schema_distribution/staging_wiki/blog/tags', 'a50'),
    exact: true
  },
  {
    path: '/software/json_schema_distribution/staging_wiki/blog/tags/code',
    component: ComponentCreator('/software/json_schema_distribution/staging_wiki/blog/tags/code', '194'),
    exact: true
  },
  {
    path: '/software/json_schema_distribution/staging_wiki/blog/tags/conventions',
    component: ComponentCreator('/software/json_schema_distribution/staging_wiki/blog/tags/conventions', '651'),
    exact: true
  },
  {
    path: '/software/json_schema_distribution/staging_wiki/blog/tags/javascript',
    component: ComponentCreator('/software/json_schema_distribution/staging_wiki/blog/tags/javascript', '6ff'),
    exact: true
  },
  {
    path: '/software/json_schema_distribution/staging_wiki/blog/tags/json-schema',
    component: ComponentCreator('/software/json_schema_distribution/staging_wiki/blog/tags/json-schema', '105'),
    exact: true
  },
  {
    path: '/software/json_schema_distribution/staging_wiki/blog/tags/node',
    component: ComponentCreator('/software/json_schema_distribution/staging_wiki/blog/tags/node', 'a77'),
    exact: true
  },
  {
    path: '/software/json_schema_distribution/staging_wiki/blog/using-the-convention-builder',
    component: ComponentCreator('/software/json_schema_distribution/staging_wiki/blog/using-the-convention-builder', '7aa'),
    exact: true
  },
  {
    path: '/software/json_schema_distribution/staging_wiki/docs',
    component: ComponentCreator('/software/json_schema_distribution/staging_wiki/docs', 'e1c'),
    routes: [
      {
        path: '/software/json_schema_distribution/staging_wiki/docs',
        component: ComponentCreator('/software/json_schema_distribution/staging_wiki/docs', '6d5'),
        routes: [
          {
            path: '/software/json_schema_distribution/staging_wiki/docs',
            component: ComponentCreator('/software/json_schema_distribution/staging_wiki/docs', 'eb0'),
            routes: [
              {
                path: '/software/json_schema_distribution/staging_wiki/docs/',
                component: ComponentCreator('/software/json_schema_distribution/staging_wiki/docs/', '643'),
                exact: true
              },
              {
                path: '/software/json_schema_distribution/staging_wiki/docs/Conventions/asset/asset--land--field',
                component: ComponentCreator('/software/json_schema_distribution/staging_wiki/docs/Conventions/asset/asset--land--field', '840'),
                exact: true,
                sidebar: "tutorialSidebar"
              },
              {
                path: '/software/json_schema_distribution/staging_wiki/docs/Conventions/asset/asset--plant--div_veg_planting',
                component: ComponentCreator('/software/json_schema_distribution/staging_wiki/docs/Conventions/asset/asset--plant--div_veg_planting', '2c1'),
                exact: true,
                sidebar: "tutorialSidebar"
              },
              {
                path: '/software/json_schema_distribution/staging_wiki/docs/Conventions/asset/asset--plant--octavio_orchard_planting',
                component: ComponentCreator('/software/json_schema_distribution/staging_wiki/docs/Conventions/asset/asset--plant--octavio_orchard_planting', '266'),
                exact: true,
                sidebar: "tutorialSidebar"
              },
              {
                path: '/software/json_schema_distribution/staging_wiki/docs/Conventions/log/log--activity--flaming',
                component: ComponentCreator('/software/json_schema_distribution/staging_wiki/docs/Conventions/log/log--activity--flaming', 'e33'),
                exact: true,
                sidebar: "tutorialSidebar"
              },
              {
                path: '/software/json_schema_distribution/staging_wiki/docs/Conventions/log/log--activity--grazing',
                component: ComponentCreator('/software/json_schema_distribution/staging_wiki/docs/Conventions/log/log--activity--grazing', '7bc'),
                exact: true,
                sidebar: "tutorialSidebar"
              },
              {
                path: '/software/json_schema_distribution/staging_wiki/docs/Conventions/log/log--activity--mowing',
                component: ComponentCreator('/software/json_schema_distribution/staging_wiki/docs/Conventions/log/log--activity--mowing', '36d'),
                exact: true,
                sidebar: "tutorialSidebar"
              },
              {
                path: '/software/json_schema_distribution/staging_wiki/docs/Conventions/log/log--activity--solarization',
                component: ComponentCreator('/software/json_schema_distribution/staging_wiki/docs/Conventions/log/log--activity--solarization', '86b'),
                exact: true,
                sidebar: "tutorialSidebar"
              },
              {
                path: '/software/json_schema_distribution/staging_wiki/docs/Conventions/log/log--activity--termination',
                component: ComponentCreator('/software/json_schema_distribution/staging_wiki/docs/Conventions/log/log--activity--termination', '33b'),
                exact: true,
                sidebar: "tutorialSidebar"
              },
              {
                path: '/software/json_schema_distribution/staging_wiki/docs/Conventions/log/log--activity--tillage',
                component: ComponentCreator('/software/json_schema_distribution/staging_wiki/docs/Conventions/log/log--activity--tillage', '147'),
                exact: true,
                sidebar: "tutorialSidebar"
              },
              {
                path: '/software/json_schema_distribution/staging_wiki/docs/Conventions/log/log--input--herbicide',
                component: ComponentCreator('/software/json_schema_distribution/staging_wiki/docs/Conventions/log/log--input--herbicide', 'e4c'),
                exact: true,
                sidebar: "tutorialSidebar"
              },
              {
                path: '/software/json_schema_distribution/staging_wiki/docs/Conventions/log/log--input--irrigation',
                component: ComponentCreator('/software/json_schema_distribution/staging_wiki/docs/Conventions/log/log--input--irrigation', '5c6'),
                exact: true,
                sidebar: "tutorialSidebar"
              },
              {
                path: '/software/json_schema_distribution/staging_wiki/docs/Conventions/log/log--input--lime',
                component: ComponentCreator('/software/json_schema_distribution/staging_wiki/docs/Conventions/log/log--input--lime', '0ab'),
                exact: true,
                sidebar: "tutorialSidebar"
              },
              {
                path: '/software/json_schema_distribution/staging_wiki/docs/Conventions/log/log--input--npk_fertilizer',
                component: ComponentCreator('/software/json_schema_distribution/staging_wiki/docs/Conventions/log/log--input--npk_fertilizer', '9a8'),
                exact: true,
                sidebar: "tutorialSidebar"
              },
              {
                path: '/software/json_schema_distribution/staging_wiki/docs/Conventions/log/log--input--organic_matter',
                component: ComponentCreator('/software/json_schema_distribution/staging_wiki/docs/Conventions/log/log--input--organic_matter', '291'),
                exact: true,
                sidebar: "tutorialSidebar"
              },
              {
                path: '/software/json_schema_distribution/staging_wiki/docs/Conventions/log/log--input--seed_treatment',
                component: ComponentCreator('/software/json_schema_distribution/staging_wiki/docs/Conventions/log/log--input--seed_treatment', 'a13'),
                exact: true,
                sidebar: "tutorialSidebar"
              },
              {
                path: '/software/json_schema_distribution/staging_wiki/docs/Conventions/log/log--input--seedling_treatment',
                component: ComponentCreator('/software/json_schema_distribution/staging_wiki/docs/Conventions/log/log--input--seedling_treatment', '82e'),
                exact: true,
                sidebar: "tutorialSidebar"
              },
              {
                path: '/software/json_schema_distribution/staging_wiki/docs/Conventions/log/log--lab_test--modus_lab_test',
                component: ComponentCreator('/software/json_schema_distribution/staging_wiki/docs/Conventions/log/log--lab_test--modus_lab_test', '579'),
                exact: true,
                sidebar: "tutorialSidebar"
              },
              {
                path: '/software/json_schema_distribution/staging_wiki/docs/Conventions/log/log--seeding--seeding',
                component: ComponentCreator('/software/json_schema_distribution/staging_wiki/docs/Conventions/log/log--seeding--seeding', '8bf'),
                exact: true,
                sidebar: "tutorialSidebar"
              },
              {
                path: '/software/json_schema_distribution/staging_wiki/docs/Conventions/log/log--transplanting--div_veg_planting',
                component: ComponentCreator('/software/json_schema_distribution/staging_wiki/docs/Conventions/log/log--transplanting--div_veg_planting', '532'),
                exact: true,
                sidebar: "tutorialSidebar"
              },
              {
                path: '/software/json_schema_distribution/staging_wiki/docs/Definitions',
                component: ComponentCreator('/software/json_schema_distribution/staging_wiki/docs/Definitions', '2f6'),
                exact: true,
                sidebar: "tutorialSidebar"
              },
              {
                path: '/software/json_schema_distribution/staging_wiki/docs/Description and Specification',
                component: ComponentCreator('/software/json_schema_distribution/staging_wiki/docs/Description and Specification', '55a'),
                exact: true,
                sidebar: "tutorialSidebar"
              },
              {
                path: '/software/json_schema_distribution/staging_wiki/docs/Frequently Asked Questions',
                component: ComponentCreator('/software/json_schema_distribution/staging_wiki/docs/Frequently Asked Questions', 'ac7'),
                exact: true,
                sidebar: "tutorialSidebar"
              },
              {
                path: '/software/json_schema_distribution/staging_wiki/docs/intro',
                component: ComponentCreator('/software/json_schema_distribution/staging_wiki/docs/intro', 'ee2'),
                exact: true
              }
            ]
          }
        ]
      }
    ]
  },
  {
    path: '/software/json_schema_distribution/staging_wiki/',
    component: ComponentCreator('/software/json_schema_distribution/staging_wiki/', 'c87'),
    exact: true
  },
  {
    path: '*',
    component: ComponentCreator('*'),
  },
];
