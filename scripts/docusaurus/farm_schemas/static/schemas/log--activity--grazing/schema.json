{
 "title": "Grazing log",
 "type": "object",
 "$id": "www.gitlabrepo.com/version/farmos_conventions/0.0.1/conventions/log--activity--grazing",
 "$schema": "https://json-schema.org/draft/2020-12/schema",
 "properties": {
  "log--activity--grazing": {
   "title": "Activity log",
   "type": "object",
   "properties": {
    "id": {
     "type": "string",
     "format": "uuid"
    },
    "type": {
     "const": "log--activity"
    },
    "meta": {
     "type": "object"
    },
    "attributes": {
     "description": "Entity attributes",
     "type": "object",
     "properties": {
      "name": {
       "type": "string",
       "title": "Name",
       "maxLength": 255,
       "description": "The name of the log. Leave this blank to automatically generate a name.",
       "default": ""
      },
      "timestamp": {
       "type": "string",
       "title": "Timestamp",
       "format": "date-time",
       "description": "Timestamp of the event being logged."
      },
      "status": {
       "const": "done"
      },
      "is_termination": {
       "type": "boolean",
       "title": "Is termination",
       "description": "If this log is a termination, then all assets referenced by it will be marked as terminated and archived at the time the log takes place. The log must be complete in order for the movement to take effect."
      },
      "data": {
       "type": "string",
       "title": "Data"
      },
      "notes": {
       "type": "object",
       "properties": {
        "value": {
         "type": "string",
         "title": "Text"
        },
        "format": {
         "type": "string",
         "title": "Text format"
        }
       },
       "required": [
        "value"
       ],
       "title": "Notes"
      },
      "flag": {
       "type": "array",
       "title": "Flags",
       "description": "Add flags to enable better sorting and filtering of records.",
       "items": {
        "type": "string",
        "title": "Text value"
       }
      },
      "geometry": {
       "type": "object",
       "properties": {
        "value": {
         "type": "string",
         "title": "Geometry"
        },
        "geo_type": {
         "type": "string",
         "title": "Geometry Type"
        },
        "lat": {
         "type": "number",
         "title": "Centroid Latitude"
        },
        "lon": {
         "type": "number",
         "title": "Centroid Longitude"
        },
        "left": {
         "type": "number",
         "title": "Left Bounding"
        },
        "top": {
         "type": "number",
         "title": "Top Bounding"
        },
        "right": {
         "type": "number",
         "title": "Right Bounding"
        },
        "bottom": {
         "type": "number",
         "title": "Bottom Bounding"
        },
        "geohash": {
         "type": "string",
         "title": "Geohash"
        },
        "latlon": {
         "type": "string",
         "title": "LatLong Pair"
        }
       },
       "title": "Geometry",
       "description": "Add geometry data to this log to describe where it took place."
      },
      "is_movement": {
       "type": "boolean",
       "title": "Is movement",
       "description": "If this log is a movement, then all assets referenced by it will be located in the referenced locations and/or geometry at the time the log takes place. The log must be complete in order for the movement to take effect."
      },
      "surveystack_id": {
       "type": "string",
       "title": "Surveystack ID",
       "maxLength": 255
      },
      "quick": {
       "type": "array",
       "title": "Quick form",
       "description": "References the quick form that was used to create this record.",
       "items": {
        "type": "string",
        "title": "Text value",
        "maxLength": 255
       }
      }
     },
     "required": [
      "status"
     ],
     "additionalProperties": false
    },
    "relationships": {
     "description": "Entity relationships",
     "properties": {
      "file": {
       "type": "object",
       "properties": {
        "data": {
         "type": "array",
         "title": "Files",
         "items": {
          "type": "object",
          "required": [
           "id",
           "type"
          ],
          "properties": {
           "id": {
            "type": "string",
            "title": "Resource ID",
            "format": "uuid",
            "maxLength": 128
           },
           "type": {
            "type": "string"
           }
          }
         }
        }
       }
      },
      "image": {
       "type": "object",
       "properties": {
        "data": {
         "type": "array",
         "title": "Images",
         "items": {
          "type": "object",
          "required": [
           "id",
           "type"
          ],
          "properties": {
           "id": {
            "type": "string",
            "title": "Resource ID",
            "format": "uuid",
            "maxLength": 128
           },
           "type": {
            "type": "string"
           }
          }
         }
        }
       }
      },
      "location": {
       "type": "object",
       "properties": {
        "data": {
         "type": "array",
         "title": "Location",
         "items": {
          "type": "object",
          "required": [
           "id",
           "type"
          ],
          "properties": {
           "id": {
            "type": "string",
            "title": "Resource ID",
            "format": "uuid",
            "maxLength": 128
           },
           "type": {
            "type": "string"
           }
          }
         }
        }
       }
      },
      "asset": {
       "type": "object",
       "properties": {
        "data": {
         "type": "array",
         "minContains": 1,
         "uniqueItems": true,
         "contains": {
          "oneOf": [
           {
            "type": "object",
            "properties": {
             "type": {
              "const": "asset--plant"
             },
             "id": {
              "const": {
               "$data": "/asset--plant--planting/id"
              }
             }
            }
           }
          ]
         }
        }
       }
      },
      "category": {
       "type": "object",
       "properties": {
        "data": {
         "type": "array",
         "minContains": 1,
         "uniqueItems": true,
         "contains": {
          "oneOf": [
           {
            "type": "object",
            "properties": {
             "type": {
              "const": "taxonomy_term--log_category"
             },
             "id": {
              "const": {
               "$data": "/taxonomy_term--log_category--grazing/id"
              }
             }
            }
           }
          ]
         }
        }
       }
      },
      "quantity": {
       "type": "object",
       "properties": {
        "data": {
         "type": "array",
         "minContains": 0,
         "uniqueItems": true,
         "contains": {
          "oneOf": [
           {
            "type": "object",
            "properties": {
             "type": {
              "const": "quantity--standard"
             },
             "id": {
              "const": {
               "$data": "/quantity--standard--area_percentage/id"
              }
             }
            }
           },
           {
            "type": "object",
            "properties": {
             "type": {
              "const": "quantity--standard"
             },
             "id": {
              "const": {
               "$data": "/quantity--standard--animal_count/id"
              }
             }
            }
           },
           {
            "type": "object",
            "properties": {
             "type": {
              "const": "quantity--standard"
             },
             "id": {
              "const": {
               "$data": "/quantity--standard--animal_weight/id"
              }
             }
            }
           },
           {
            "type": "object",
            "properties": {
             "type": {
              "const": "quantity--standard"
             },
             "id": {
              "const": {
               "$data": "/quantity--standard--subpaddocks/id"
              }
             }
            }
           }
          ]
         }
        }
       }
      },
      "owner": {
       "type": "object",
       "properties": {
        "data": {
         "type": "array",
         "title": "Owners",
         "items": {
          "type": "object",
          "required": [
           "id",
           "type"
          ],
          "properties": {
           "id": {
            "type": "string",
            "title": "Resource ID",
            "format": "uuid",
            "maxLength": 128
           },
           "type": {
            "type": "string"
           }
          }
         }
        }
       }
      }
     },
     "type": "object",
     "additionalProperties": false,
     "required": [
      "category",
      "asset"
     ]
    }
   },
   "required": [
    "id",
    "attributes",
    "relationships"
   ],
   "description": "Grazing logs are used to track when a herd was moved in and/or out of an area. This log records field, % of area covered, type of livestock, grazing type, animal count, average weight, and grazing purpose"
  },
  "quantity--standard--animal_count": {
   "title": "Standard quantity",
   "type": "object",
   "properties": {
    "id": {
     "type": "string",
     "format": "uuid"
    },
    "type": {
     "const": "quantity--standard"
    },
    "meta": {
     "type": "object"
    },
    "attributes": {
     "description": "Entity attributes",
     "type": "object",
     "properties": {
      "measure": {
       "const": "count"
      },
      "value": {
       "type": "object",
       "properties": {
        "numerator": {
         "type": "integer",
         "title": "Numerator value"
        },
        "denominator": {
         "type": "integer",
         "title": "Denominator value"
        }
       },
       "title": "Value",
       "description": "Value of the quantity."
      },
      "label": {
       "type": "string",
       "title": "Label",
       "maxLength": 255,
       "description": "Label of the quantity.",
       "default": ""
      }
     },
     "additionalProperties": false
    },
    "relationships": {
     "description": "Entity relationships",
     "properties": {
      "units": {
       "type": "object",
       "properties": {
        "data": {
         "title": "Units",
         "type": "object",
         "required": [
          "id",
          "type"
         ],
         "properties": {
          "id": {
           "type": "string",
           "title": "Resource ID",
           "format": "uuid",
           "maxLength": 128
          },
          "type": {
           "type": "string"
          }
         }
        }
       }
      }
     },
     "type": "object",
     "additionalProperties": false
    }
   },
   "required": [
    "id",
    "attributes"
   ],
   "description": "Animal count refers to the number of animals involved in this grazing event."
  },
  "quantity--standard--animal_weight": {
   "title": "Standard quantity",
   "type": "object",
   "properties": {
    "id": {
     "type": "string",
     "format": "uuid"
    },
    "type": {
     "const": "quantity--standard"
    },
    "meta": {
     "type": "object"
    },
    "attributes": {
     "description": "Entity attributes",
     "type": "object",
     "properties": {
      "measure": {
       "const": "weight"
      },
      "value": {
       "type": "object",
       "properties": {
        "numerator": {
         "type": "integer",
         "title": "Numerator value"
        },
        "denominator": {
         "type": "integer",
         "title": "Denominator value"
        }
       },
       "title": "Value",
       "description": "Value of the quantity."
      },
      "label": {
       "type": "string",
       "title": "Label",
       "maxLength": 255,
       "description": "Label of the quantity.",
       "default": ""
      }
     },
     "additionalProperties": false
    },
    "relationships": {
     "description": "Entity relationships",
     "properties": {
      "units": {
       "type": "object",
       "properties": {
        "data": {
         "type": "array",
         "minContains": 0,
         "uniqueItems": true,
         "contains": {
          "oneOf": [
           {
            "type": "object",
            "properties": {
             "type": {
              "const": "taxonomy_term--unit"
             },
             "id": {
              "const": {
               "$data": "/taxonomy_term--unit--weight/id"
              }
             }
            }
           }
          ]
         }
        }
       }
      }
     },
     "type": "object",
     "additionalProperties": false
    }
   },
   "required": [
    "id",
    "attributes"
   ],
   "description": "Animal weight is an average weight of the herd involved in the grazing event. The measure attribute is set to weight."
  },
  "taxonomy_term--unit--weight": {
   "title": "Unit taxonomy term",
   "type": "object",
   "properties": {
    "id": {
     "type": "string",
     "format": "uuid"
    },
    "type": {
     "const": "taxonomy_term--unit"
    },
    "meta": {
     "type": "object"
    },
    "attributes": {
     "description": "Entity attributes",
     "type": "object",
     "properties": {
      "status": {
       "type": "boolean",
       "title": "Published",
       "default": true
      },
      "name": {
       "enum": [
        "lbs"
       ],
       "description": "Units used to measure the average weight of the grazing herd."
      },
      "description": {
       "type": "object",
       "properties": {
        "value": {
         "type": "string",
         "title": "Text"
        },
        "format": {
         "type": "string",
         "title": "Text format"
        }
       },
       "required": [
        "value"
       ],
       "title": "Description"
      },
      "weight": {
       "type": "integer",
       "title": "Weight",
       "description": "The weight of this term in relation to other terms.",
       "default": 0
      }
     },
     "required": [
      "name"
     ],
     "additionalProperties": false
    },
    "relationships": {
     "description": "Entity relationships",
     "properties": {
      "vid": {
       "type": "object",
       "properties": {
        "data": {
         "title": "Vocabulary",
         "type": "object",
         "required": [
          "id",
          "type"
         ],
         "properties": {
          "id": {
           "type": "string",
           "title": "Resource ID",
           "format": "uuid",
           "maxLength": 128
          },
          "type": {
           "type": "string"
          }
         }
        }
       }
      },
      "parent": {
       "type": "object",
       "properties": {
        "data": {
         "type": "array",
         "title": "Term Parents",
         "items": {
          "type": "object",
          "required": [
           "id",
           "type"
          ],
          "properties": {
           "id": {
            "type": "string",
            "title": "Resource ID",
            "format": "uuid",
            "maxLength": 128
           },
           "type": {
            "type": "string"
           }
          }
         }
        }
       }
      }
     },
     "type": "object",
     "additionalProperties": false
    }
   },
   "required": [
    "id",
    "attributes"
   ],
   "description": "Units in kg or lb."
  },
  "quantity--standard--subpaddocks": {
   "title": "Standard quantity",
   "type": "object",
   "properties": {
    "id": {
     "type": "string",
     "format": "uuid"
    },
    "type": {
     "const": "quantity--standard"
    },
    "meta": {
     "type": "object"
    },
    "attributes": {
     "description": "Entity attributes",
     "type": "object",
     "properties": {
      "measure": {
       "const": "count"
      },
      "value": {
       "type": "object",
       "properties": {
        "numerator": {
         "type": "integer",
         "title": "Numerator value"
        },
        "denominator": {
         "type": "integer",
         "title": "Denominator value"
        }
       },
       "title": "Value",
       "description": "Value of the quantity."
      },
      "label": {
       "type": "string",
       "title": "Label",
       "maxLength": 255,
       "description": "Label of the quantity.",
       "default": ""
      }
     },
     "additionalProperties": false
    },
    "relationships": {
     "description": "Entity relationships",
     "properties": {
      "units": {
       "type": "object",
       "properties": {
        "data": {
         "title": "Units",
         "type": "object",
         "required": [
          "id",
          "type"
         ],
         "properties": {
          "id": {
           "type": "string",
           "title": "Resource ID",
           "format": "uuid",
           "maxLength": 128
          },
          "type": {
           "type": "string"
          }
         }
        }
       }
      }
     },
     "type": "object",
     "additionalProperties": false
    }
   },
   "required": [
    "id",
    "attributes"
   ],
   "description": "The number of subpaddocks or sections the grazed field is split into."
  },
  "taxonomy_term--log_category--grazing": {
   "title": "Log category taxonomy term",
   "type": "object",
   "properties": {
    "id": {
     "type": "string",
     "format": "uuid"
    },
    "type": {
     "const": "taxonomy_term--log_category"
    },
    "meta": {
     "type": "object"
    },
    "attributes": {
     "description": "Entity attributes",
     "type": "object",
     "properties": {
      "status": {
       "type": "boolean",
       "title": "Published",
       "default": true
      },
      "name": {
       "const": "grazing"
      },
      "description": {
       "type": "object",
       "properties": {
        "value": {
         "type": "string",
         "title": "Text"
        },
        "format": {
         "type": "string",
         "title": "Text format"
        }
       },
       "required": [
        "value"
       ],
       "title": "Description"
      },
      "weight": {
       "type": "integer",
       "title": "Weight",
       "description": "The weight of this term in relation to other terms.",
       "default": 0
      }
     },
     "required": [
      "name"
     ],
     "additionalProperties": false
    },
    "relationships": {
     "description": "Entity relationships",
     "properties": {
      "vid": {
       "type": "object",
       "properties": {
        "data": {
         "title": "Vocabulary",
         "type": "object",
         "required": [
          "id",
          "type"
         ],
         "properties": {
          "id": {
           "type": "string",
           "title": "Resource ID",
           "format": "uuid",
           "maxLength": 128
          },
          "type": {
           "type": "string"
          }
         }
        }
       }
      },
      "parent": {
       "type": "object",
       "properties": {
        "data": {
         "type": "array",
         "title": "Term Parents",
         "items": {
          "type": "object",
          "required": [
           "id",
           "type"
          ],
          "properties": {
           "id": {
            "type": "string",
            "title": "Resource ID",
            "format": "uuid",
            "maxLength": 128
           },
           "type": {
            "type": "string"
           }
          }
         }
        }
       }
      }
     },
     "type": "object",
     "additionalProperties": false
    }
   },
   "required": [
    "id",
    "attributes"
   ],
   "description": "Log category is set to grazing."
  },
  "quantity--standard--area_percentage": {
   "title": "Standard quantity",
   "type": "object",
   "properties": {
    "id": {
     "type": "string",
     "format": "uuid"
    },
    "type": {
     "const": "quantity--standard"
    },
    "meta": {
     "type": "object"
    },
    "attributes": {
     "description": "Entity attributes",
     "type": "object",
     "properties": {
      "measure": {
       "const": "area"
      },
      "value": {
       "type": "object",
       "properties": {
        "numerator": {
         "type": "integer",
         "title": "Numerator value"
        },
        "denominator": {
         "type": "integer",
         "title": "Denominator value"
        }
       },
       "title": "Value",
       "description": "Value of the quantity."
      },
      "label": {
       "const": "area"
      }
     },
     "additionalProperties": false
    },
    "relationships": {
     "description": "Entity relationships",
     "properties": {
      "units": {
       "type": "object",
       "properties": {
        "data": {
         "type": "array",
         "minContains": 0,
         "uniqueItems": true,
         "contains": {
          "oneOf": [
           {
            "type": "object",
            "properties": {
             "type": {
              "const": "taxonomy_term--unit"
             },
             "id": {
              "const": {
               "$data": "/taxonomy_term--unit--%/id"
              }
             }
            }
           }
          ]
         }
        }
       }
      }
     },
     "type": "object",
     "additionalProperties": false
    }
   },
   "required": [
    "id",
    "attributes"
   ],
   "description": "Area is the percentage of the field that the seeding applies to."
  },
  "taxonomy_term--unit--%": {
   "title": "Unit taxonomy term",
   "type": "object",
   "properties": {
    "id": {
     "type": "string",
     "format": "uuid"
    },
    "type": {
     "const": "taxonomy_term--unit"
    },
    "meta": {
     "type": "object"
    },
    "attributes": {
     "description": "Entity attributes",
     "type": "object",
     "properties": {
      "status": {
       "type": "boolean",
       "title": "Published",
       "default": true
      },
      "name": {
       "const": "%"
      },
      "description": {
       "type": "object",
       "properties": {
        "value": {
         "type": "string",
         "title": "Text"
        },
        "format": {
         "type": "string",
         "title": "Text format"
        }
       },
       "required": [
        "value"
       ],
       "title": "Description"
      },
      "weight": {
       "type": "integer",
       "title": "Weight",
       "description": "The weight of this term in relation to other terms.",
       "default": 0
      }
     },
     "required": [
      "name"
     ],
     "additionalProperties": false
    },
    "relationships": {
     "description": "Entity relationships",
     "properties": {
      "vid": {
       "type": "object",
       "properties": {
        "data": {
         "title": "Vocabulary",
         "type": "object",
         "required": [
          "id",
          "type"
         ],
         "properties": {
          "id": {
           "type": "string",
           "title": "Resource ID",
           "format": "uuid",
           "maxLength": 128
          },
          "type": {
           "type": "string"
          }
         }
        }
       }
      },
      "parent": {
       "type": "object",
       "properties": {
        "data": {
         "type": "array",
         "title": "Term Parents",
         "items": {
          "type": "object",
          "required": [
           "id",
           "type"
          ],
          "properties": {
           "id": {
            "type": "string",
            "title": "Resource ID",
            "format": "uuid",
            "maxLength": 128
           },
           "type": {
            "type": "string"
           }
          }
         }
        }
       }
      }
     },
     "type": "object",
     "additionalProperties": false
    }
   },
   "required": [
    "id",
    "attributes"
   ],
   "description": "Units for area are: % of acres."
  },
  "asset--plant--planting": {
   "title": "Plant asset",
   "type": "object",
   "properties": {
    "id": {
     "type": "string",
     "format": "uuid"
    },
    "type": {
     "const": "asset--plant"
    },
    "meta": {
     "type": "object"
    },
    "attributes": {
     "description": "Entity attributes",
     "type": "object",
     "properties": {
      "name": {
       "type": "string",
       "title": "Name",
       "maxLength": 255,
       "description": "The name of the asset."
      },
      "status": {
       "const": "active"
      },
      "archived": {
       "type": "string",
       "title": "Timestamp",
       "format": "date-time",
       "description": "The time the asset was archived."
      },
      "data": {
       "type": "string",
       "title": "Data"
      },
      "notes": {
       "type": "object",
       "properties": {
        "value": {
         "type": "string",
         "title": "Text"
        },
        "format": {
         "type": "string",
         "title": "Text format"
        }
       },
       "required": [
        "value"
       ],
       "title": "Notes"
      },
      "flag": {
       "type": "array",
       "title": "Flags",
       "description": "Add flags to enable better sorting and filtering of records.",
       "items": {
        "type": "string",
        "title": "Text value"
       }
      },
      "id_tag": {
       "type": "array",
       "title": "ID tags",
       "description": "List any identification tags that this asset has. Use the fields below to describe the type, location, and ID of each.",
       "items": {
        "type": "object",
        "properties": {
         "id": {
          "type": "string",
          "title": "ID of the tag"
         },
         "type": {
          "type": "string",
          "title": "Type of the tag"
         },
         "location": {
          "type": "string",
          "title": "Location of the tag"
         }
        }
       }
      },
      "geometry": {
       "const": null
      },
      "intrinsic_geometry": {
       "type": "object",
       "properties": {
        "value": {
         "type": "string",
         "title": "Geometry"
        },
        "geo_type": {
         "type": "string",
         "title": "Geometry Type"
        },
        "lat": {
         "type": "number",
         "title": "Centroid Latitude"
        },
        "lon": {
         "type": "number",
         "title": "Centroid Longitude"
        },
        "left": {
         "type": "number",
         "title": "Left Bounding"
        },
        "top": {
         "type": "number",
         "title": "Top Bounding"
        },
        "right": {
         "type": "number",
         "title": "Right Bounding"
        },
        "bottom": {
         "type": "number",
         "title": "Bottom Bounding"
        },
        "geohash": {
         "type": "string",
         "title": "Geohash"
        },
        "latlon": {
         "type": "string",
         "title": "LatLong Pair"
        }
       },
       "title": "Intrinsic geometry",
       "description": "Add geometry data to this asset to describe its intrinsic location. This will only be used if the asset is fixed."
      },
      "is_location": {
       "type": "boolean",
       "title": "Is location",
       "description": "If this asset is a location, then other assets can be moved to it."
      },
      "is_fixed": {
       "type": "boolean",
       "title": "Is fixed",
       "description": "If this asset is fixed, then it can have an intrinsic geometry. If the asset will move around, then it is not fixed and geometry will be determined by movement logs."
      },
      "surveystack_id": {
       "type": "string",
       "title": "Surveystack ID",
       "maxLength": 255
      },
      "quick": {
       "type": "array",
       "title": "Quick form",
       "description": "References the quick form that was used to create this record.",
       "items": {
        "type": "string",
        "title": "Text value",
        "maxLength": 255
       }
      }
     },
     "required": [
      "name",
      "status"
     ],
     "additionalProperties": false
    },
    "relationships": {
     "description": "Entity relationships",
     "properties": {
      "termination": {
       "type": "object",
       "properties": {
        "data": {
         "title": "Terminated",
         "type": "object",
         "required": [
          "id",
          "type"
         ],
         "properties": {
          "id": {
           "type": "string",
           "title": "Resource ID",
           "format": "uuid",
           "maxLength": 128
          },
          "type": {
           "type": "string"
          }
         }
        }
       }
      },
      "file": {
       "type": "object",
       "properties": {
        "data": {
         "type": "array",
         "title": "Files",
         "items": {
          "type": "object",
          "required": [
           "id",
           "type"
          ],
          "properties": {
           "id": {
            "type": "string",
            "title": "Resource ID",
            "format": "uuid",
            "maxLength": 128
           },
           "type": {
            "type": "string"
           }
          }
         }
        }
       }
      },
      "image": {
       "type": "object",
       "properties": {
        "data": {
         "type": "array",
         "title": "Images",
         "items": {
          "type": "object",
          "required": [
           "id",
           "type"
          ],
          "properties": {
           "id": {
            "type": "string",
            "title": "Resource ID",
            "format": "uuid",
            "maxLength": 128
           },
           "type": {
            "type": "string"
           }
          }
         }
        }
       }
      },
      "location": {
       "type": "object",
       "properties": {
        "data": {
         "type": "array",
         "title": "Current location",
         "items": {
          "type": "object",
          "required": [
           "id",
           "type"
          ],
          "properties": {
           "id": {
            "type": "string",
            "title": "Resource ID",
            "format": "uuid",
            "maxLength": 128
           },
           "type": {
            "type": "string"
           }
          }
         }
        }
       }
      },
      "owner": {
       "type": "object",
       "properties": {
        "data": {
         "type": "array",
         "title": "Owners",
         "items": {
          "type": "object",
          "required": [
           "id",
           "type"
          ],
          "properties": {
           "id": {
            "type": "string",
            "title": "Resource ID",
            "format": "uuid",
            "maxLength": 128
           },
           "type": {
            "type": "string"
           }
          }
         }
        }
       }
      },
      "parent": {
       "type": "object",
       "properties": {
        "data": {
         "type": "array",
         "title": "Parents",
         "items": {
          "type": "object",
          "required": [
           "id",
           "type"
          ],
          "properties": {
           "id": {
            "type": "string",
            "title": "Resource ID",
            "format": "uuid",
            "maxLength": 128
           },
           "type": {
            "type": "string"
           }
          }
         }
        }
       }
      },
      "plant_type": {
       "type": "object",
       "properties": {
        "data": {
         "type": "array",
         "title": "Crop/variety",
         "items": {
          "type": "object",
          "required": [
           "id",
           "type"
          ],
          "properties": {
           "id": {
            "type": "string",
            "title": "Resource ID",
            "format": "uuid",
            "maxLength": 128
           },
           "type": {
            "type": "string"
           }
          }
         }
        }
       }
      },
      "season": {
       "type": "object",
       "properties": {
        "data": {
         "type": "array",
         "title": "Season",
         "items": {
          "type": "object",
          "required": [
           "id",
           "type"
          ],
          "properties": {
           "id": {
            "type": "string",
            "title": "Resource ID",
            "format": "uuid",
            "maxLength": 128
           },
           "type": {
            "type": "string"
           }
          }
         }
        }
       }
      }
     },
     "type": "object",
     "required": [
      "plant_type"
     ],
     "additionalProperties": false
    }
   },
   "required": [
    "id",
    "attributes"
   ],
   "description": "The planting is the main asset that all management logs will reference."
  }
 },
 "description": "## Purpose\n\nGrazing logs can be created for planting termination or for tracking general herd managment activities.\n\n## Specification\n\ntext\n",
 "required": [
  "log--activity--grazing",
  "taxonomy_term--log_category--grazing",
  "asset--plant--planting"
 ]
}