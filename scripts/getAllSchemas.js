// Obtain all the entity schemas from a farm and store both the original and de drupalized versions. If a source repo is provided instead, the farmOS entity convetions will be copied from it.
const fs = require('fs');
const axios = require('axios');
const farmOS = require('farmos').default;
const entities = require('../node_modules/farmos/dist/cjs/entities');

// we need to parametrize the farmOS.js object to add more entity types
const { default: defaultEntities, defaultOptions } = entities;
const profile = {
    nomenclature: {
        name: 'profile',
        shortName: 'profile',
        plural: 'profiles',
        shortPlural: 'profiles',
        display: 'Profile',
        displayPlural: 'Profiles',
    },
    defaultOptions,
};

if (!process.env.FARM_DOMAIN) {
    require('dotenv').config({ path:`${__dirname}/../.env` });
};

const aggregatorKey = process.env.FARMOS_KEY;
const farmDomain = process.env.FARM_DOMAIN;
const sourceRepo = process.env.SOURCE_REPO_PROJECT_ID;


// If no source repo is available, inform that also farmos key is missing.
if ( !sourceRepo && ( !aggregatorKey || !farmDomain ) ) {
    let missing = [];
    if (!aggregatorKey) {
        missing.push("FARMOS_KEY");
    };
    if (!farmDomain) {
        missing.push("FARM_DOMAIN");
    };
    if (!sourceRepo) {
        missing.push("SOURCE_REPO_PROJECT_ID");
        throw `One or more fundamental environment variables are missing:  ${missing.join(", ")}. Provide them via an environemt var, either in an .env file or in CI reserved variables.`;
    };

};



if ( !sourceRepo ) {
    // A farm is the source of our main entities. Retrieve them using farmOS
    const aggregatorKeyAuth = (request, authOpts = {}) => {
        request.interceptors.request.use(
            config => {
                return {
                    ...config,
                    headers: {
                        ...config.headers,
                        'api-key': aggregatorKey,
                    },
                    params: {
                        'farm_id': 1,
                    },
                };
            },
            Promise.reject,
        );
        return {
            authorize(aggregatorKey) {
            },
        };
    };

    // Default token functions.
    let token;
    const getToken = () => token;
    const setToken = (t) => { token = t; };

    let farm = farmOS(
        {
            remote: {
                host: farmDomain,
                clientId: "farm",
                getToken: getToken,
                setToken: setToken,
                auth: aggregatorKeyAuth
            },
            entities: {
                ... defaultEntities,
                profile
            }
        }
    );

    console.log(`Retrieving schemas from ${farmDomain}`);

    let farmOSjsSchemata = {};

    try {
        farmOSjsSchemata = farm.schema.fetch();
        fs.mkdirSync( "./input/collection/", { recursive: true }, console.error );
        farmOSjsSchemata.then( schemata => {
            fs.writeFileSync(`${__dirname}/../input/farmos.json`, JSON.stringify( schemata.data ) );
        } );
        console.log("Schemata retrieved.");
    } catch(e) {
        console.log("error");
        console.log(e);
    };
} else if (sourceRepo) {
    // We will get our entities from another repo
    let fileAddress = `https://gitlab.com/api/v4/projects/${sourceRepo}/jobs/artifacts/main/raw/input/farmos.json?job=copy_schemas`;
    let farmOSjsSchemata = axios.get(fileAddress);
    farmOSjsSchemata.then( schemata => {
        fs.writeFileSync(`${__dirname}/../input/farmos.json`, JSON.stringify( schemata.data ) );
    } );
    console.log("Schemata retrieved.");
}
