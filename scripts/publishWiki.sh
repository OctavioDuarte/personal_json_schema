#!/bin/bash

## $1 is the current branch

export BRANCH=$1

## copy some environment variables into static JSON files (describing the public artifacts)
node scripts/storeRepoData.js

## copy the conventions documentation into the source folder
rm -r scripts/docusaurus/farm_schemas/docs/Conventions
cp -r output/documentation/Conventions scripts/docusaurus/farm_schemas/docs/
cp scripts/skeleton_documentation/docs/* scripts/docusaurus/farm_schemas/docs/
cp scripts/skeleton_documentation/main_index.mdx scripts/docusaurus/farm_schemas/src/pages/index.mdx
## copy the schemata, to feed the schema plugin
rm -r scripts/docusaurus/farm_schemas/static/schemas
./scripts/copySchemataToDocusaurus.sh
## processes each schema and creates a new version, only keeping required fields. It is one of the versions we show in documentation
node ./scripts/createRequiredOnlyViews.js
## preparse a json document contaning the structure we want to show in the sidebar for our documentation.
node ./scripts/writeSidebarDocumentationSection.js

cd scripts/docusaurus/farm_schemas
npm ci

if [ $1 = "main" ]; then
    cp docusaurus_main.config.js docusaurus.config.js
elif [ $1 = "staging" ]; then
    cp docusaurus_staging.config.js docusaurus.config.js
fi

npm run build

cd ../../..

if [ $1 = "main" ]; then
    rm -r public/wiki
    mkdir -p public/wiki
    mv scripts/docusaurus/farm_schemas/build/* public/wiki/
elif [ $1 = "staging" ]; then
    rm -r public/staging_wiki
    mkdir -p public/staging_wiki
    mv scripts/docusaurus/farm_schemas/build/* public/staging_wiki/
fi
