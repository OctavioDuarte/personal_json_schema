# Documentation for FarmOS Conventions

## What are conventions?

  Conventions are complex *schemas* orchestrating potentially many FarmOS *entities* into fixed roles, with some fixed values enforcing a meaningful, repeatable and comparable way to encode certain pieces of data.
  

## What is this repo?

  This repo has been forked by a user who wanted to encode, share and validate their own *conventions* to enable broad compairability. In here, you will find the conventions themselves expressed as **JSON Schemas**, as **validation software** and also as **writen docuemnts**, contained in this wiki.
