const fs = require('fs');

// obtaining the folders that we want to list in the sidebar
let folders = fs.readdirSync( `${__dirname}/docusaurus/farm_schemas/docs/Conventions`, console.error );
let conventionCategories = folders.flatMap( folder => {
    let files = fs.readdirSync( `${__dirname}/docusaurus/farm_schemas/docs/Conventions/${folder}`, console.error);
    let itemsArray = files
        .map( file => { return file.split('.')[0]; } )
        .map( file => `Conventions/${folder}/${file}` )
    ;
    let output = {
        type: 'category',
        label: folder,
        items: itemsArray
    };
    return output;
} );

fs.writeFileSync(`${__dirname}/docusaurus/farm_schemas/static/convention_folders_structure.json`, JSON.stringify({ conventionCategories: conventionCategories }));


