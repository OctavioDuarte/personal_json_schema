// functions helping build and maintain the repo

/**
 * Given the repo parameters, it will produce valid permanent links to job artifacts, which we need to publish conventions.
 * If you have an organzation in GitLab called "agro-developers", which has a group of projects called "data-systematization" under which you have a project/repo called "schemata", then organization: "agro-developers", repo: "schemata" and repoGroup: "data-systematization".
 * @param {string} organization -- Name of your organization in GitLab. It might be your username if the repo is hosted on a user repo.
 * @param {string} repo -- Name of the repository under which you are hosting your schemata.
 * @param {string} repoGroup -- Optional. If the repo is grouped inside a group, provide its name, otherwise leave the default value of false.
 * @param {path} file -- Target filepath as seen while navigating the artifacts (follows the repo structure). 
 * @param {string} branch -- Job branch to point to.
 * @returns {string} link -- The permanent link. 
 */
export function buildArtifactLink({ organization, repo, repoGroup = false, file, branch = "main"}) {
    let firstSection = `https://gitlab.com/${organization}`;
    let secondSection = repoGroup ? `${ repoGroup }` : "";
    let thirdSection = `${repo}/-/jobs/artifacts/${branch}/browse/${file}?job=copy_schemas`;
    let link = [firstSection, secondSection, thirdSection].join("/");
    return(link);
};


/**
 * Creates a link to a section of the published gitlab pages.
 * @param {string} organization -- Name of your organization in GitLab. It might be your username if the repo is hosted on a user repo.
 * @param {string} repo -- Name of the repository under which you are hosting your schemata.
 * @param {boolean/string} repoGroup -- Optional. If the repo is grouped inside a group, provide its name, otherwise leave the default value of false.
 * @param {string} section -- Page you want to link to, the structure is the folder structure copied inside the "public" folder in the "pages" job.
 * @param {string} baseUrl -- Parameter obtained from docusaurus.config.js. It allows to infer if we are working on the 'staging' or 'main' wiki.
 * @returns {string} link -- The permanent link. 
 */
export function buildDocumentationLink({organization, repoGroup = false, repo, section="wiki"}) {
    let link = `https://${organization}.gitlab.io/${ repoGroup ? repoGroup.concat('/') : '' }${repo}/${section}`;
    return link;
};


/**
 * Given the package name the repo is exporting, it will provide links to the NPM package and the browser version as seen in  jsdelivr.
 * @param {string} packageName -- Package name, as declared in package.json.
 * @returns {object} links -- An object contanining both links. 
 */
export function buildPackageLinks({packageName}) {
    let npmLink = `https://www.npmjs.com/package/${packageName}`;
    let cdnLink = `https://cdn.jsdelivr.net/npm/${packageName}/dist/module/farmos_schemata_validator.js`;
    let output = {
        npm: npmLink,
        cdn: cdnLink
    };
    return output;
};
