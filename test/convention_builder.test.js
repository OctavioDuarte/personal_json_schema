const builder = require("../src/convention_builder.js");
const fs = require("fs");
const { randomUUID } = require('crypto');

let conventionsFolder = `test/unity_test_conventions`;

fs.rmSync(`${__dirname}/unity_test_conventions`, {recursive:true, force:true});

fs.mkdirSync( `${__dirname}/unity_test_conventions`, { recursive: true }, console.error );
fs.mkdirSync( `${__dirname}/unity_test_conventions/overlays`, { recursive: true }, console.error );

describe( "Testing the SchemaOverlay class, used to define specific versions of entity types.", () => {
    test("A schema is acquired.", () => {
        const tillageLogActivity = new builder.SchemaOverlay({ typeAndBundle:"log--activity", name:"tillage" });
        expect(tillageLogActivity.baseSchema.title).toBe("Activity log");
    });

    test("An error is generated for invalid baseSchema names.", () => {
        expect( () => new builder.SchemaOverlay({ typeAndBundle:"log/activity", name:"tillage" })).toThrow(`No schema for log/activity was found. This parameter should be a type--bundle pair, as farmOS mandates (examples are "log--lab_test" and "asset--land").`);
    });

    test("An error is generated for non existent, valid baseSchema names.", () => {
        expect( () => new builder.SchemaOverlay({ typeAndBundle:"log--non_existent_activity", name:"tillage" })).toThrow(`No schema for log--non_existent_activity was found. This might be due to the schema not existing in the farm, or the current "input/collection" folder structure being out of date. You can update it using the "getAllSchemas.js" script, in the script folder.`);
    });

    test("A constant is applied.", () => {
        const tillageLogActivity = new builder.SchemaOverlay(
            {
                typeAndBundle:"log--activity",
                name:"tillage",
                validExamples: [ { name: "tillage" } ],
                erroredExamples: [{identification:"tillage"}]
            }
        );
        tillageLogActivity.setConstant( { attribute:"name", value:"tillage" } );
        expect(tillageLogActivity.schema.properties.attributes.properties.name.const).toBe("tillage");
    });


    test("A description is added when a constant is set.", () => {
        const tillageLogActivity = new builder.SchemaOverlay({ typeAndBundle:"log--activity", name:"tillage" });
        tillageLogActivity.setConstant( { attribute:"name", value:"tillage", description:"Test description." } );
        expect(tillageLogActivity.schema.properties.attributes.properties.name.description).toBe("Test description.");
    });

    test("A regex pattern is applied and enforced.", () => {
        const tillageLogActivity = new builder.SchemaOverlay({
            typeAndBundle:"log--activity",
            name:"tillage",
            validExamples: [ { attributes: {name:"123tillageAdasdasd", status:"done",timestamp: ( new Date() ).toISOString()}, id: randomUUID() } ],
            erroredExamples: [ { attributes: {name:"123", status:"done"}, id: randomUUID() } ]
        });
        tillageLogActivity.setPattern( { attribute:"name", pattern:"tillage", description:"Test description." } );
        expect(tillageLogActivity.schema.properties.attributes.properties.name.pattern).toBe("tillage");

        let testResult = tillageLogActivity.testExamples();
        expect(testResult.success).toBe(true);
    });

    test("An enumeration is applied if set to 'strictEnums'.", () => {
        let example = {
            attributes: {
                name: "tillage",
                status:"done",
                timestamp: ( new Date() ).toISOString()
            },
            id: randomUUID()
        };
        let error = {
            attributes: {
                name: "another",
                status:"done",
                timestamp: ( new Date() ).toISOString()
            },
            id: randomUUID()
        };
        const tillageLogActivity = new builder.SchemaOverlay({
            typeAndBundle:"log--activity",
            name:"tillage",
            validExamples: [ example ],
            erroredExamples: [error]
        });
        tillageLogActivity.strictEnums = true;
        tillageLogActivity.setEnum( { attribute:"name", valuesArray:[ "tillage", "tillage_log", "plow" ], description:"Accepting all synonims." } );
        let test = tillageLogActivity.testExamples();
        expect(tillageLogActivity.schema.properties.attributes.properties.name.enum).toStrictEqual([ "tillage", "tillage_log", "plow" ]);
        expect(tillageLogActivity.schema.properties.attributes.properties.name.description).toBe("Accepting all synonims.");
        expect(test.success).toBe(true);
    });

    test("An enumeration is applied with anyOf when strictEnums is false.", () => {
            let exampleEnum = {
                attributes: {
                    name: "tillage",
                    status:"done",
                    timestamp: ( new Date() ).toISOString()
                },
                id: randomUUID()
            };
            let exampleString = {
                attributes: {
                    name: "not in list",
                    status:"done",
                    timestamp: ( new Date() ).toISOString()
                },
                id: randomUUID()
            };
            let error = {
                attributes: {
                    name: 2,
                    status:"done",
                    timestamp: ( new Date() ).toISOString()
                }
            };
            const tillageLogActivity = new builder.SchemaOverlay({
                typeAndBundle:"log--activity",
                name:"tillage",
                validExamples: [ exampleEnum, exampleString ],
                erroredExamples: [error],
                strictEnums: false
            });
            tillageLogActivity.setEnum( { attribute:"name", valuesArray:[ "tillage", "tillage_log", "plow" ], description:"Accepting all synonims." } );
            let test = tillageLogActivity.testExamples();
            expect(tillageLogActivity.schema.properties.attributes.properties.name.anyOf.find(d => d.enum).enum).toStrictEqual([ "tillage", "tillage_log", "plow" ]);
                expect(tillageLogActivity.schema.properties.attributes.properties.name.anyOf.find(d => d.type).type).toBe("string");
            expect(test.success).toBe(true);
        });

    test("Setting non existing attributes triggers an error.", () => {
        const tillageLogActivity = new builder.SchemaOverlay({ typeAndBundle:"log--activity", name:"tillage" });
        expect( () => tillageLogActivity.setConstant( { attribute:"non_existing_attr", value:"tillage", description:"Test description." } ) ).toThrow("Attribute non_existing_attr is unknown to the schema log--activity.");
    });

    test("Overwriting specifications triggers an error.", () => {
        const tillageLogActivity = new builder.SchemaOverlay({ typeAndBundle:"log--activity", name:"tillage" });
        tillageLogActivity.setConstant( { attribute:"name", value:"1_tillage", description:"Test description." } );
        expect( () => tillageLogActivity.setConstant( { attribute:"name", value:"2_tillage", description:"Test description." } ) ).toThrow("Attribute name has already been modified for this overlay, you are overwriting previously declared changes. Instead, make only one operation encompassing all the changes together.");
    });

    test("testExamples method is able to detect success", () => {
        let validExample = { attributes:{ name: "tillage", status:"done", data: "plow",timestamp: ( new Date() ).toISOString() }, id: randomUUID() };
        let errorExample_1 = { attributes:{ name: "tillage_error", data: "activity",timestamp: ( new Date() ).toISOString() }, id: randomUUID() };
        let errorExample_2 = { attributes:{ name: "tillage_error", status:2, data: "wrong",timestamp: ( new Date() ).toISOString() }, id: randomUUID() };
        const tillageLogActivity = new builder.SchemaOverlay(
            {
                typeAndBundle:"log--activity",
                name:"tillage",
                validExamples: [validExample],
                erroredExamples: [ errorExample_1, errorExample_2 ]
            });
        tillageLogActivity.setConstant( { attribute:"name", value:"tillage", description:"Test description." } );
        tillageLogActivity.setEnum( { attribute:"data", valuesArray:[ "tillage", "plow" ], description:"Test description." } );

        let testResult = tillageLogActivity.testExamples();

        expect(testResult.success).toBe(true);
    });

    test("testExamples method is able to detect failure and deliver information.", () => {
        let errorExample_1 = { attributes:{ name: "tillage_error", data:"wrong",timestamp: ( new Date() ).toISOString() }, id: randomUUID(), id: randomUUID() };
        let errorExample_2 = { attributes:{ name: "tillage_error", status:2,timestamp: ( new Date() ).toISOString() }, id: randomUUID() };
        const tillageLogActivity = new builder.SchemaOverlay(
            {
                typeAndBundle:"log--activity",
                name:"tillage", validExamples: [ errorExample_1 ],
                erroredExamples: [ errorExample_2 ]
            });
        tillageLogActivity.strictEnums = true;
        tillageLogActivity.setConstant( { attribute:"name", value:"tillage", description:"Test description." } );
        tillageLogActivity.setEnum( { attribute:"data", valuesArray:[ "tillage", "plow" ], description:"Test description." } );

        let testResult = tillageLogActivity.testExamples();

        // property mandatory in original schema
        let error1 = "must have required property 'status'";
        // property set by set constant
        let error2 = 'must be equal to constant';
        // property set by set enum
        let error3 = 'must be equal to one of the allowed values';

        let failedExamples = testResult.failedExamples.flatMap( d => d.errors );

        // console.log("failed examples");
        // console.log(failedExamples);
        expect(testResult.success).toBe(false);
        expect(failedExamples.map(ex => ex.message)).toStrictEqual( [ error1, error2, error3 ] );
    });

    test("Storage works.", () => {
        let validExample = { attributes:{ name: "tillage", status:"done", data: "plow",timestamp: ( new Date() ).toISOString() }, id: randomUUID() };
        let errorExample_1 = { attributes:{ name: "tillage_error", data: "activity",timestamp: ( new Date() ).toISOString() }, id: randomUUID() };
        let errorExample_2 = { attributes:{ name: "tillage_error", status:2, data: "wrong",timestamp: ( new Date() ).toISOString() }, id: randomUUID() };
        const tillageLogActivity = new builder.SchemaOverlay(
            {
                typeAndBundle:"log--activity",
                name:"tillage",
                validExamples: [validExample],
                erroredExamples: [ errorExample_1, errorExample_2 ],
                storageFolder: `${conventionsFolder}/overlays`

            });
        tillageLogActivity.setConstant( { attribute:"name", value:"tillage", description:"Test description." } );
        tillageLogActivity.setEnum( { attribute:"data", valuesArray:[ "tillage", "plow" ], description:"Test description." } );
        tillageLogActivity.setMainDescription( "Tillage activity, created for testing" );

        let testResult = tillageLogActivity.testExamples();
        let storageStats = tillageLogActivity.store();
        let correctExamples = fs.readdirSync( `${storageStats.path}/examples/correct`, (error, files) => {
            return files;
        } );
        let incorrectExamples  = fs.readdirSync( `${storageStats.path}/examples/incorrect`, (error, files) => {
            return files;
        } );


        expect(storageStats.valid).toBe(true);
        expect(correctExamples.length).toBe(tillageLogActivity.validExamples.length);
        expect(incorrectExamples.length).toBe(tillageLogActivity.erroredExamples.length);
        expect(storageStats.path).toBe(`${__dirname}/unity_test_conventions/overlays/log--activity--tillage`.replace("test", "src/../test"));
    });

    test("A schema overlay is correctly recovered ( read and parsed ) from a JSONschema with all its properties.", () => {
        let tillageLogActivitySrc = JSON.parse( fs.readFileSync(`${__dirname}/unity_test_conventions/overlays/log--activity--tillage/object.json`) );
        let tillageLogActivity = new builder.SchemaOverlay( tillageLogActivitySrc );
        tillageLogActivity.schemaName = tillageLogActivity.schemaName.concat("--recovered");
        let storageTest = tillageLogActivity.testExamples();
        let storageStats = tillageLogActivity.store();

        let originalSchema = JSON.parse( fs.readFileSync(`${__dirname}/unity_test_conventions/overlays/log--activity--tillage/schema.json`) );
        let schemaFromRecovered = JSON.parse( fs.readFileSync(`${__dirname}/unity_test_conventions/overlays/log--activity--tillage--recovered/schema.json`) );

        fs.writeFileSync(`${__dirname}/unity_test_conventions/overlays/log--activity--tillage/recovered_schema.json`, JSON.stringify(tillageLogActivity.schema), console.error);

        expect(tillageLogActivity.overlay.description).toBe("Tillage activity, created for testing");
        expect(storageStats.path).toBe(`${__dirname}/unity_test_conventions/overlays/log--activity--tillage--recovered`.replace("test", "src/../test"));
        expect(schemaFromRecovered).toStrictEqual(originalSchema);
        fs.rmSync(`${__dirname}/unity_test_conventions/overlays/log--activity--tillage`, {recursive:true, force:true});
        fs.rmSync(`${__dirname}/unity_test_conventions/overlays/log--activity--tillage--recovered`, {recursive:true, force:true});
    });

    test("An overlay read from a JSON object keeps its examples.", () => {
        let validExample = { attributes:{ name: "tillage", status:"done", data: "plow",timestamp: ( new Date() ).toISOString() }, id: randomUUID() };
        let errorExample_1 = { attributes:{ name: "tillage_error", data: "activity",timestamp: ( new Date() ).toISOString() }, id: randomUUID() };
        let errorExample_2 = { attributes:{ name: "tillage_error", status:2, data: "wrong",timestamp: ( new Date() ).toISOString() }, id: randomUUID() };
        const tillageLogActivity = new builder.SchemaOverlay(
            {
                typeAndBundle:"log--activity",
                name:"tillage",
                validExamples: [validExample],
                erroredExamples: [ errorExample_1, errorExample_2 ],
                storageFolder: `${conventionsFolder}/overlays`

            });
        tillageLogActivity.setConstant( { attribute:"name", value:"tillage", description:"Test description." } );
        tillageLogActivity.setEnum( { attribute:"data", valuesArray:[ "tillage", "plow" ], description:"Test description." } );
        tillageLogActivity.setMainDescription( "Tillage activity, created for testing" );

        let testResult = tillageLogActivity.testExamples();
        let storageStats = tillageLogActivity.store();

        let recoveredObject = JSON.parse( fs.readFileSync(`${__dirname}/unity_test_conventions/overlays/log--activity--tillage/object.json`) );
        expect(recoveredObject.validExamples.length.length).toBe(tillageLogActivity.validExamples.length.length);
        expect(recoveredObject.erroredExamples.length.length).toBe(tillageLogActivity.erroredExamples.length.length);
    });

} );

describe( "Testing the main convention builder.", () => {

    let tillageLog = new builder.SchemaOverlay({
        typeAndBundle: "log--activity",
        name: "tillage",
        storageFolder: `${conventionsFolder}/overlays`
    });
    tillageLog.setMainDescription("Must be related to a taxonomy_term--log_category named *tillage* and be related to an *asset--land*. Should have quantity--standard--stir, quantity--standard--residue, quantity--standard--tillage_depth. May have other taxonomy_term--log_category. Originally hosted in [link](https://gitlab.com/OpenTEAMAg/ag-data-wallet/openteam-convention/-/blob/main/descriptions/log--activity--tillage.md)");
    tillageLog.setConstant( {
        attribute:"name",
        value:"tillage"
    } );
    let stirQuantity = new builder.SchemaOverlay({
        typeAndBundle: "quantity--standard",
        name: "STIR",
        storageFolder: `${conventionsFolder}/overlays`
    });
    stirQuantity.setMainDescription("Must be labelled as *stir* and it's measure type is *ratio*. See documentation ADD LINK to get the standard specification for this ratio.");
    stirQuantity.setConstant( {
        attribute:"label",
        value:"STIR",
    } );
    stirQuantity.setConstant( {
        attribute:"measure",
        value:"ration",
    } );
    let depthQuantity = new builder.SchemaOverlay({
        typeAndBundle: "quantity--standard",
        name: "tillage_depth",
        storageFolder: `${conventionsFolder}/overlays`
    });
    depthQuantity.setMainDescription("Must be labelled as *depth* and it's measure type is *length*.");
    depthQuantity.setConstant( {
        attribute:"label",
        value:"depth",
    } );
    depthQuantity.setConstant( {
        attribute:"measure",
        value:"length",
    } );

    let logUUID = randomUUID();
    let stirUUID = randomUUID();
    let depthUUID = randomUUID();

    let example = {
        // each entity will be an attribute in the convention, with a reference to a known entity type.
        // we need more precission, for example constants.
        tillage_log: {
            attributes: {
                status:'done',
                name: "tillage",
                timestamp: ( new Date() ).toISOString()
            },
            relationships: {
                quantity: { data: [
                    {
                        type: "quantity--standard",
                        id: stirUUID
                    },
                    {
                        type: "quantity--standard",
                        id: depthUUID
                    }
                ] },
                location: { data: [
                    { type: "asset--land",
                      id: randomUUID()
                    }
                ] }
            },
            id: logUUID
        },
        stir_quantity: {
            attributes: {label: "STIR"},
            id: stirUUID
        },
        depth_quantity: {
            attributes: {
                label:"depth"
            },
            id: depthUUID
        }
    };
    let example2 = {
        // in this example, the optional relationship is missing
        tillage_log: {
            attributes: {
                status:'done',
                name: "tillage",
                timestamp: ( new Date() ).toISOString()
            },
            relationships: {
                quantity: { data: [
                    {
                        type: "quantity--standard",
                        id: stirUUID
                    }
                ] },
                location: { data: [
                    { type: "asset--land",
                        id: randomUUID()
                    }
                ] }
            },
            id: logUUID
        },
        stir_quantity: {
            attributes: {label: "STIR"},
            id: stirUUID
        },
    };
    let example3 = {
        // each entity will be an attribute in the convention, with a reference to a known entity type.
        // we need more precission, for example constants.
        tillage_log: {
            attributes: {
                status:'done',
                name: "tillage",
                timestamp: ( new Date() ).toISOString()
            },
            relationships: {
                quantity: { data: [
                    {
                        type: "quantity--standard",
                        id: depthUUID
                    }
                ] },
                location: { data: [
                    { type: "asset--land",
                        id: randomUUID()
                    }
                ] }
            },
            id: logUUID
        },
        depth_quantity: {
            attributes: {
                label:"depth"
            },
            id: depthUUID
        }
    };
    let exampleError = {
        // each entity will be an attribute in the convention, with a reference to a known entity type.
        // we need more precission, for example constants.
        activity_log: {
            id: logUUID,
            attributes: {
                status:'done',
                name: "tillage log",
                timestamp: ( new Date() ).toISOString()
            },
            relationships: {
                quantity: { data: [ {
                    type: "quantity--standard",
                    id: '695e1d38-a1d6-4ed7-8d9d-c0b818a2cbfe'
                }, {
                    type: "quantity--standard",
                    id: 'b71f7bb3-da43-40b1-8dc3-965e5f912763'
                }
                                  ] }
            },
            relationships: {
                quantity: { data: [  ] }
            }
        },
        stir_quantity: {
            id: logUUID,
            attributes: {label: "stir"}
        },
    };

    let tillageConvention = new builder.ConventionSchema({
        title: "tillage_event",
        schemaName: "log--activity--tillage_log_test",
        version:"0.0.1",
        repoURL:"www.gitlabrepo.com/version/farmos_conventions/",
        description:"A tillage log encompasses all information we can gather about a tillage operation.",
        validExamples: [ example, example2, example3 ],
        erroredExamples: [exampleError],
        storageFolder: `${conventionsFolder}`
    });

    tillageConvention.addAttribute( { schemaOverlayObject:tillageLog, attributeName: "tillage_log", required: true } );
    tillageConvention.addAttribute( { schemaOverlayObject:stirQuantity, attributeName: "stir_quantity", required: false } );
    tillageConvention.addAttribute( { schemaOverlayObject:depthQuantity, attributeName: "depth_quantity", required: false } );
    tillageConvention.addRelationship( { containerEntity:"tillage_log" , relationName:"quantity" , mentionedEntity:"stir_quantity" , required: true } );
    tillageConvention.addRelationship( { containerEntity:"tillage_log" , relationName:"quantity" , mentionedEntity:"depth_quantity" , required: false } );
    // console.log("schema");
    // console.log(tillageConvention.schema.properties.tillage_log.properties.relationships.properties.quantity);

    // console.log(tillageConvention.schema.properties.tillage_log.properties.relationships.properties.quantity.properties.data);


    test("A convention gets built.", () => {
        expect(tillageConvention.schema.properties.tillage_log.properties.relationships.properties.quantity.properties.data.contains.anyOf.length ).toBe(2);
        let testResult = tillageConvention.testExamples();
        let failedExamples = testResult.failedExamples.flatMap( d => d.errors );

        // console.log(`${ testResult.failedExamples.length } examples have failed`);
        // console.log("schema");
        // console.log( tillageConvention.schema.properties.tillage_log.properties.relationships.properties.quantity );
        // console.log("contains");
        // // console.log( tillageConvention.schema.properties.tillage_log.properties.relationships.properties.quantity.properties.data.contains.oneOf.map( d => d.properties ).map( d => d.id ) );

        // console.log("failed examples");
        // console.log(failedExamples);
        expect(testResult.success).toBe(true);
    });

    test("A convention gets stored.", () => {
        let storeStats = tillageConvention.store();
        // console.log(storeStats);
        let correctExamples = fs.readdirSync( `${storeStats.path}/examples/correct`, (error, files) => {
            return files;
        } );
        let incorrectExamples  = fs.readdirSync( `${storeStats.path}/examples/incorrect`, (error, files) => {
            return files;
        } );
        // console.log(correctExamples);
        // console.log(incorrectExamples);
        fs.rmSync(`${__dirname}/unity_test_conventions/log--activity--tillage_log_test`, {recursive:true, force:true});
        expect(storeStats.valid).toBe(true);
        expect(correctExamples.length).toBe(tillageConvention.validExamples.length);
        expect(incorrectExamples.length).toBe(tillageConvention.erroredExamples.length);
        expect(storeStats.path).toBe(`${__dirname}/unity_test_conventions/conventions/log--activity--tillage_log_test`.replace("test", "src/../test"));
    });

    test("Wrong relationship fields throw an informative error.", () => {
        expect( () => tillageConvention.addRelationship({ containerEntity:"tillage_log" , relationName:"amount" , mentionedEntity:"stir_quantity" , required: false }) ).toThrow("The provided relationship field, 'amount', doesn't seem to be a valid field in the schema for 'log--activity'. Available fields are: file, image, location, asset, category, quantity, owner.");
        expect( () => tillageConvention.addRelationship({ containerEntity:"tillage" , relationName:"quantity" , mentionedEntity:"stir_quantity" , required: false }) ).toThrow("Container entity 'tillage' has not been added as an attribute, please add a SchemaOverlay describing it before using it in a relationship.");
        expect( () => tillageConvention.addRelationship({ containerEntity:"tillage_log" , relationName:"quantity" , mentionedEntity:"stir" , required: false }) ).toThrow("Mentioned entity 'stir' has not been added as an attribute, please add a SchemaOverlay describing it before using it in a relationship.");
    });

    test("Relationship arrays do not need to have a fixed order.", () => {
        let crossedIDsExample = structuredClone(example);
        crossedIDsExample.tillage_log.relationships.quantity.data[0].id = example.tillage_log.relationships.quantity.data[1].id;
        crossedIDsExample.tillage_log.relationships.quantity.data[1].id = example.tillage_log.relationships.quantity.data[0].id;
        let validation = tillageConvention.validate(example);
        let validationCrossed = tillageConvention.validate(crossedIDsExample);
        expect(validation).toBe(true);
        // order does not affect the result
        expect(validationCrossed).toBe(true);
    });

    test("Relationships detect wrong ids.", () => {
        let crossedIDsExample = structuredClone(example);
        crossedIDsExample.tillage_log.relationships.quantity.data[0].id = randomUUID();
        crossedIDsExample.tillage_log.relationships.quantity.data[1].id = randomUUID();
        let validation = tillageConvention.validate(example);
        let validationError = tillageConvention.validate(crossedIDsExample);
        expect(validation).toBe(true);
        // A wrong id is detected.
        
        expect(validationError).toBe(false);
        expect(tillageConvention.validate.errors.map(d => d.message )).toContain('must match a schema in anyOf');
    });

    test("Relationship arrays allow the presence of extra items if the fixed ones are present.", () => {
        let extraRelationshipsExample = structuredClone(example);
        extraRelationshipsExample.tillage_log.relationships.quantity.data[2] = { type:"quanity--material", id:randomUUID() };
        let validation = tillageConvention.validate(example);
        let validationOther = tillageConvention.validate(extraRelationshipsExample);
        expect(validation).toBe(true);
        // order does not affect the result
        expect(validationOther).toBe(true);
    });

    test("Relationship arrays do not allow missing items, even if the number of items is still the right one.", () => {
        let missingRelationshipExample = structuredClone(example);
        delete missingRelationshipExample.tillage_log.relationships.quantity.data[0];
        delete missingRelationshipExample.tillage_log.relationships.quantity.data[1];
        missingRelationshipExample.tillage_log.relationships.quantity.data[0] = { type:"quanity--material", id:randomUUID() };
        let validation = tillageConvention.validate(example);
        let validationError = tillageConvention.validate(missingRelationshipExample);
        expect(validation).toBe(true);
        // order does not affect the result
        expect(validationError).toBe(false);
        expect(tillageConvention.validate.errors.map(d => d.message )).toContain("must match a schema in anyOf");
    });

    test("Adequate 'required' tags avoid an empty relationships field to be accepted as valid when a relationship is required.", () => {
        let missingRelationshipExample = structuredClone(example);
        delete missingRelationshipExample.tillage_log.relationships.quantity;
        let validationError = tillageConvention.validate(missingRelationshipExample);

        expect(validationError).toBe(false);
        expect(tillageConvention.validate.errors.map(d => d.message )).toContain(`must have required property 'quantity'`);
    });

    test("Naming a base schema via a string in  the addAttribute method when building a convention results in a valid object", () => {

        let newExample = {};
        let newError = {};
        Object.assign(newExample, example);
        Object.assign(newError, example);
        newExample.seeding_log = {
            attributes: {
                status:"done",
                timestamp: ( new Date() ).toISOString()
            },
            id: randomUUID()
        };
        newError.seeding_log = {
            attributes: {
                status:1,
                timestamp: ( new Date() ).toISOString()
            },
            id: randomUUID()
        };

        let newConvention = new builder.ConventionSchema({
            title: "tillage_event",
            schemaName: "log--activity--tillage_log_test",
            version:"0.0.1",
            repoURL:"www.gitlabrepo.com/version/farmos_conventions/",
            description:"A tillage log encompasses all information we can gather about a tillage operation.",
            validExamples: [ newExample ],
            erroredExamples: [ newError ],
            storageFolder: `${conventionsFolder}`
        });

        newConvention.addAttribute( { schemaOverlayObject:tillageLog, attributeName: "tillage_log", required: true } );
        newConvention.addAttribute( { schemaOverlayObject:stirQuantity, attributeName: "stir_quantity", required: false } );
        newConvention.addAttribute( { schemaOverlayObject:depthQuantity, attributeName: "depth_quantity", required: false } );
        newConvention.addRelationship( { containerEntity:"tillage_log" , relationName:"quantity" , mentionedEntity:"stir_quantity" , required: false } );
        newConvention.addRelationship( { containerEntity:"tillage_log" , relationName:"quantity" , mentionedEntity:"depth_quantity" , required: false } );
        newConvention.addAttribute( { schemaOverlayObject:"log--seeding", attributeName: "seeding_log", required: true } );

        let test = newConvention.testExamples();
        let storageOperation = newConvention.store();

        expect(test.success).toBe(true);
        expect(test.erroredExamples[0].errors[0].instancePath).toBe('/seeding_log/attributes/status');
        expect(test.erroredExamples[0].errors[0].keyword).toBe('type');
    });

    test("Naming a preexisting overlay via a string in  the addAttribute method when building a convention results in a valid object", () => {

            let newConvention = new builder.ConventionSchema({
                title: "tillage_event",
                schemaName: "log--activity--tillage_log_test",
                version:"0.0.1",
                repoURL:"www.gitlabrepo.com/version/farmos_conventions/",
                description:"A tillage log encompasses all information we can gather about a tillage operation.",
                validExamples: [ example ],
                erroredExamples: [exampleError],
                storageFolder: `${conventionsFolder}`
            });

            let depthExample = {
                attributes: {
                    label:"depth",
                measure:"length"
                },
                id: randomUUID()
            };
            let depthError = {
                attributes:{ label:2 },
                id: randomUUID()
            };

            let newDepthQuantity = new builder.SchemaOverlay({
                typeAndBundle: "quantity--standard",
                name: "tillage_depth",
                storageFolder: `${conventionsFolder}/overlays`,
                validExamples: [depthExample],
                erroredExamples:[depthError]
            });
            newDepthQuantity.setMainDescription("Must be labelled as *depth* and it's measure type is *length*.");
            newDepthQuantity.setConstant( {
                attribute:"label",
                value:"depth",
            } );
            newDepthQuantity.setConstant( {
                attribute:"measure",
                value:"length",
            } );

            let overlayTest = newDepthQuantity.testExamples();
            newDepthQuantity.store();

            newConvention.addAttribute( { schemaOverlayObject:tillageLog, attributeName: "tillage_log", required: true } );
            newConvention.addAttribute( { schemaOverlayObject:stirQuantity, attributeName: "stir_quantity", required: false } );
            newConvention.addAttribute( { schemaOverlayObject:"quantity--standard--tillage_depth", attributeName: "depth_quantity", required: false } );
            newConvention.addRelationship( { containerEntity:"tillage_log" , relationName:"quantity" , mentionedEntity:"stir_quantity" , required: false } );
            newConvention.addRelationship( { containerEntity:"tillage_log" , relationName:"quantity" , mentionedEntity:"depth_quantity" , required: false } );

        fs.rmSync(`${__dirname}/unity_test_conventions/overlays/quantity--standard--tillage_depth`, {recursive:true, force:true});
            let test = newConvention.testExamples();
            let storageOperation = newConvention.store();

            expect(test.success).toBe(true);
        });

    test("Naming a FarmOS type bundle when creating an attribute creates a trivial overlay, the unmodified entity.", () => {
        let validExample = {
                plant_asset: {
                    attributes: {
                        status:"active",
                        name: "plant"
                    },
                    id: randomUUID()
                }
        };
        let errorExample = {
            plant_asset: {
                attributes: {
                    name: "plant",
                    timestamp: ( new Date() ).toISOString()
                },
                id: randomUUID()
            }
        };

        let newConvention = new builder.ConventionSchema({
            title: "tillage_event",
            schemaName: "log--activity--test_trivial",
            version:"0.0.1",
            repoURL:"www.gitlabrepo.com/version/farmos_conventions/",
            description:"A tillage log encompasses all information we can gather about a tillage operation.",
            validExamples: [ validExample ],
            erroredExamples: [errorExample],
            storageFolder: `${conventionsFolder}`
        });

        // adding a trivial overlay, just a FarmOS entity.
        newConvention.addAttribute( { schemaOverlayObject:"asset--plant", attributeName: "plant_asset", required: true } );

        let test = newConvention.testExamples();
        let storageOperation = newConvention.store();
        ;

        expect(test.success).toBe(true);
        // no overlay file should have been created
        expect(fs.readdirSync(`${newConvention.storagePath()}/overlays`).findIndex(folder =>  folder == "asset--plant--trivial")).toBe(-1);
        fs.rmSync(`${__dirname}/unity_test_conventions/conventions/log--activity--test_trivial/`, {recursive:true});
    });

    test("Recovering from a JSON object results in a valid ConventionSchema", () => {
        tillageConvention.store();
        let storedSchema = JSON.parse( fs.readFileSync( `${__dirname}/unity_test_conventions/conventions/log--activity--tillage_log_test/schema.json` ) );
        let conventionSrc = JSON.parse( fs.readFileSync( `${__dirname}/unity_test_conventions/conventions/log--activity--tillage_log_test/object.json` ) );
        let recoveredConvention = new builder.ConventionSchema( conventionSrc );
        recoveredConvention.schemaName = recoveredConvention.schemaName.concat("--recovered");
        let storageOperation = recoveredConvention.store();
        let schemaFromRecovered = JSON.parse( fs.readFileSync( `${__dirname}/unity_test_conventions/conventions/log--activity--tillage_log_test--recovered/schema.json` ) );

        // as schema name attributes are different, we need to erase the '$id' before comparing
        delete schemaFromRecovered.$id;
        delete storedSchema.$id;
        expect(schemaFromRecovered).toEqual(storedSchema);
    });
} );
