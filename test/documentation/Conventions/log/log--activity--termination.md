# Diverse Vegetable Seed Treatment - v0.0.1

## termination_log

* **Type: log--activity** 
* **Required: true** 
* Add termination event if an termination date is included in the planting information.


 **Overlay Requirements**: 


 * Comment: The status should always be set to done to inherit the area.
* status: fixed to done.

* is_termination: fixed to true.
### log_category

* **Type: taxonomy_term--log_category** 
* **Required: true** 
* The associated log implies planting termination.

