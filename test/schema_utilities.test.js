const {apiComposeTaxonomiesTransformation,
       exampleImporter,
       fixRelationshipDataField,
       findMissingEntitiesInExample,
       organizeEntitiesArrayIntoConvention,
       organizeEntitiesArrayByRelationships,
       trimNonRequiredFields,
       reduceSchemaToRequiredFields
      } = require("../src/schema_utilities.js");
const fs = require("fs");
const builder = require("../src/convention_builder.js");
const { randomUUID } = require('crypto');

describe( "Fixing basic schemas", () => {
    test("'data' field, removed by FarmOS.js, is properly added back to all relationships while storinng example schemas.", () => {

        let schema = {
            type: "object",
            properties: {
                relationships: {
                    type:"object",
                    properties: { "location": {
                        "type": "array",
                        "title": "Location",
                        "items": {
                            "type": "object",
                            "required": [
                                "id",
                                "type"
                            ],
                            "properties": {
                                "id": {
                                    "type": "string",
                                    "title": "Resource ID",
                                    "format": "uuid",
                                    "maxLength": 128
                                },
                                "type": {
                                    "type": "string"
                                }
                            }
                        }
                    } }
                }
            }
        }
        ;
        fixRelationshipDataField(schema);
        expect(schema.properties.relationships.properties.location.type).toBe('object');
        expect(schema.properties.relationships.properties.location.properties.data.type).toBe('array');
    });
} );

describe("Example importing functionality.", () => {

    test("An entity with implied taxonomies gets decoupled into many schema compliant entities, and these are correctly mentioned into the relationships.",  () => {

        // diverse vegetatle api compose
        const apiComposeExample = JSON.parse( fs.readFileSync(`${__dirname}/dataExamples/api_compose_data_example.json`) );
        let transformationOutput = apiComposeTaxonomiesTransformation( apiComposeExample.find( d => d.entity.type == "log--seeding" ).entity );
        let createdTaxonomyTermLogCat = transformationOutput.newEntities.find( d => d.type == "taxonomy_term--log_category" );
        // new entities have the right structure in and outside "attributes"
        expect( Object.keys( createdTaxonomyTermLogCat ) ).toStrictEqual(["id", "type", "attributes"]);
        expect( Object.keys( createdTaxonomyTermLogCat.attributes ) ).toStrictEqual(["name"]);
        // the relationship now has the proper structure
        expect( Object.keys( transformationOutput.mainEntity.relationships.category.data[0] ) ).toStrictEqual( ["type","id"] );
        // the entity correctly establishes the relationship
        expect( transformationOutput.mainEntity.relationships.category.data[0].id ).toBe( createdTaxonomyTermLogCat.id );
        expect( transformationOutput.mainEntity.relationships.category.data[0].type ).toBe( "taxonomy_term--log_category" );
    });

    test("An entity with two implied taxonomies gets two newEntities",  () => {

        // diverse vegetable api compose
        const apiComposeExample = JSON.parse( fs.readFileSync(`${__dirname}/dataExamples/api_compose_data_example.json`) );
        let transformationOutput = apiComposeTaxonomiesTransformation( apiComposeExample.find( d => d.entity.type == "asset--plant" ).entity );

        expect(transformationOutput.newEntities.length).toBe(2);
        });

    test("Dates are properly reparsed when using examples importer.",  () => {
        // diverse vegetable api compose
        let example = exampleImporter( "api_compose_data_example.json", basePath = `${__dirname}/dataExamples` );
        let expectedTimestamp = '2023-05-18T04:00:00.000Z';
        let readTimestamp = example.find( entity => entity.type == "log--seeding" ).attributes.timestamp;

        expect(readTimestamp).toBe(expectedTimestamp);
    });

    test("Main user facing function for this feature, exampleImporter, is able to retrieve a file and process it.", () => {
        let example = exampleImporter( "api_compose_data_example.json", basePath = `${__dirname}/dataExamples` );
        expect(example.filter( d => d.type.includes("taxonomy")).length ).toBe(6);
    });

    test("Missing examples detector works", () => {
        let example = [
            {
                type: 'quantity--standard',
                id: 'c5daa14a-6e08-47ce-9935-86252e5260a3',
                attributes: { value: { value: { decimal: 1 }, label: 'active_ingredient_percent' }, label: 'active_ingredient_percent' },
                relationships: { units: {
                    data: [
                        {
                            type: 'taxonomy_term--unit',
                            id: '4ed95592-0ec8-47f5-9fe8-998567d79957'
                        }
                    ]
} }
            },
            {
                id: '4ed95592-0ec8-47f5-9fe8-998567d79957',
                type: 'taxonomy_term--unit',
                attributes: { name: '%' }
            },
            {
                type: 'log--input',
                attributes: {
                    name: 'insecticide 2 4 d 1 cubic ft',
                    status: 'done',
                    notes: [Object],
                    is_termination: true,
                    timestamp: '2023-05-31T04:00:00.000Z'
                },
                relationships: {
                    asset: {
                        data: [
                            {
                                type: 'asset--plant',
                                id: 'cba27c69-9a31-40b7-8f40-3ef690fdb9c6'
                            }
                        ]
},
                    category: {
                        data: [
                            {
                                type: 'taxonomy_term--log_category',
                                id: 'fef5183c-9f6e-4708-b980-87fa81e659d8'
                            },
                            {
                                type: 'taxonomy_term--log_category',
                                id: 'ec4c4741-d080-45d0-ad79-728171614882'
                            }
                        ]
},
                    quantity: {
                        data: [
                            {
                                type: 'quantity--standard',
                                id: '4eb67df7-6903-4423-ad66-5f406f6cf71d'
                            },
                            {
                                type: 'quantity--material',
                                id: 'c709a558-8ad7-452c-ad01-8e56db34571f'
                            },
                            {
                                type: 'quantity--standard',
                                id: 'c5daa14a-6e08-47ce-9935-86252e5260a3'
                            }
                        ]
}
                },
                id: '246f2455-3462-450b-8b2f-aa3ffed86c39'
            },
        ];
        let search = findMissingEntitiesInExample(example);
        let includedOrMissing = [ ... search.included , ... search.mentionedNotIncluded.map( d => d.id ) ];
        let onlyInMentioned = includedOrMissing.filter( d => !search.mentioned.includes(d) );
        let onlyInIncludedOrMissing = search.mentioned.filter( d => !includedOrMissing.includes(d) );

        expect( search.mentioned.includes(... includedOrMissing) ).toBe( true );
        expect( search.mentioned.length ).toBeLessThanOrEqual( search.included.length + search.mentionedNotIncluded.length );
    });
});

describe("Structuring loose entity arrays into convention objects.", () => {
    test("Non relationships reliant version recovers a convention from a properly structured array", () => {
        // obtain the preexisting convention
        let tillageConventionSrc = JSON.parse( fs.readFileSync( `${__dirname}/dataExamples/tillage_log_convention_object.json` ) );
        let tillageConvention = new builder.ConventionSchema( tillageConventionSrc );
        // reading the example
        let arrayExample = exampleImporter(filename = "tillageLogSurvey_mechanized_complete.json", basePath = `${__dirname}/dataExamples/`);

        // apply the structuring function
        let searchResultTillage = organizeEntitiesArrayIntoConvention( {
            entitiesArray: arrayExample,
            conventionObject:tillageConvention
        } );
        let allStatuses = Object.keys( searchResultTillage.structured_convention ).map( key => searchResultTillage.structured_convention[key].status );
        expect(allStatuses.filter(d=>d).length).toBe(1);
    });
    test("Relationships reliant version recovers a convention from a properly structured array", () => {
        // obtain the preexisting convention
        let tillageConventionSrc = JSON.parse( fs.readFileSync( `${__dirname}/dataExamples/tillage_log_convention_object.json` ) );
        let tillageConvention = new builder.ConventionSchema( tillageConventionSrc );
        // reading the example
        let arrayExample = exampleImporter(filename = "tillageLogSurvey_mechanized_complete.json", basePath = `${__dirname}/dataExamples/`);

        // apply the structuring function
        let searchResultTillage = organizeEntitiesArrayByRelationships( {
            entitiesArray: arrayExample,
            conventionObject:tillageConvention
        } );
        // expect(allStatuses.filter(d=>d).length).toBe(1);
    });
});

describe("Generating 'required only' version of a schema.",() => {
    test("trimNonRequiredFields function in charge of pruning an attribute works", () => {
        let firstExample = {
            firstLevel: {
                secondLevel : {
                    properties: {
                        a:1,
                        b:2,
                        c:3
                    },
                    required: ["a","b"]
                }
            }
        };
        let firstTrimmed = trimNonRequiredFields(firstExample, "firstLevel.secondLevel");
        expect(Object.keys(firstTrimmed.firstLevel.secondLevel.properties)).toStrictEqual(["a","b"]);
    });
    test("We can get a required only schema from a convention, using reduceSchemaToRequiredFields.", () => {
        // obtain the preexisting convention
        let tillageConventionSrc = JSON.parse( fs.readFileSync( `${__dirname}/dataExamples/tillage_log_convention_object.json` ) );
        let tillageConvention = new builder.ConventionSchema( tillageConventionSrc );
        // reduce it
        let reducedSchema = reduceSchemaToRequiredFields(tillageConvention.schema);
        expect(Object.keys(reducedSchema.properties)).toEqual(reducedSchema.required);
        expect(Object.keys(reducedSchema.properties.plant_asset.properties.attributes.properties)).toEqual(reducedSchema.properties.plant_asset.properties.attributes.required);
        // console.log("required");
        // console.log(tillageConvention.schema.properties.plant_asset.properties.relationships.required);
        // console.log("kept");
        // console.log(reducedSchema.properties.plant_asset.properties.relationships.properties);

        expect(Object.keys(reducedSchema.properties.plant_asset.properties.relationships.properties)).toEqual(reducedSchema.properties.plant_asset.properties.relationships.required);
    });
    test("Relationships field gets erased when it doesn't contain required fields.", () => {
        let schema = JSON.parse( fs.readFileSync( `${__dirname}/dataExamples/plantingSchemaForReductionTest.json` ) );
        let reducedSchema = reduceSchemaToRequiredFields(schema);
        // console.log(schema.properties["taxonomy_term--plant_type--species"].properties.relationships);
        // console.log(reducedSchema.properties["taxonomy_term--plant_type--species"].properties.relationships);
        expect(reducedSchema.properties["taxonomy_term--plant_type--species"].properties.relationships).toBe(undefined);
    });
});
